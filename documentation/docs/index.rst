..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2018 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

.. Plugin Squash TA for IntelliJ-idea documentation master file, created by
   sphinx-quickstart on Fri Mar  1 15:17:40 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


##############################
Squash TF IntelliJ IDEA plugin
##############################

.. toctree::
   :maxdepth: 2
   :hidden:


   Squash TF Doc Homepage <https://squash-tf.readthedocs.io>
   Usage <usage.rst>
   Development Tools <development-tools.rst>
   Download <https://squash-tf.readthedocs.io/en/latest/download.html#squash-intellij-idea-plugin>

*******
Summary
*******

This IntelliJ IDEA plugin helps us to write **S** quash **K** eyword **F** ramework, aka SKF, scripts. It brings us :

* autocompletion (instructions / macros)
* syntax highlighting (instructions / macros)
* syntax validation (instructions / macros)

----

.. admonition:: Known limitations
   :class: admonition warning

   | Currently the Squash TF IntelliJ plugin has some limitations:

   * The last declared PHASE/SECTION line in a Test script MUST not be empty to be recognised. If you don't have anything to insert in it yet, just simply complete that line by pressing "Enter".
   * All plugin's features such as syntax validation, color highlighting, autocompletion... will no longer work properly after the first parsing error (if any) encountered in the file.
   * Sometime the IntelliJ's color highlighting doesn't update after a syntax error correction, in that case all you need to do is move back to the beginning of the current line and press "Enter" to refresh the color highlighting.
   * When you open the code completion popup for a macro and type some characters, the plugin gets all the macro containing those characters and keeps them to proposal. However, the filtration result contains all macro in which the searched characters existed not only in one block but also being separated by other characters.
