..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2018 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

##############################
Squash TF IntelliJ IDEA plugin
##############################

.. toctree::

*******************
Plugin Installation
*******************

Requirements
============

* IntelliJ Community IDE (version :guilabel:`2018.3.2` or later)
* JAVA JDK 1.8+

.. note::
    The plugin can work without installing Maven on your machine thanks to the embedded version.
    However it is preferable to indicate the path of your own Maven version (3.5.0 recommended).
    

Install plugin from disk
========================

Download Squash TF plugin for Intellij IDEA `here <https://squash-tf.readthedocs.io/en/latest/download.html#squash-intellij-idea-plugin>`_

To install follow the default "`install plugin from disk <https://www.jetbrains.com/help/idea/managing-plugins.html#install_plugin_from_disk>`_" instructions from IntelliJ :

.. |cogwheel_icon| image:: _static/icons/gearPlain.png
                   :alt: cogwheel icon
                   :scale: 70 %

#. In the ``Settings/Preferences`` dialog (:guilabel:`Ctrl+Alt+S`), select ``Plugins``.
#. In the ``Plugins`` dialog, click the |cogwheel_icon| and then click ``Install Plugin from Disk``.
#. Select the plugin archive file and click ``OK``.
#. Click ``OK`` to apply the changes and restart the IDE if prompted.

|

***********
User Manual
***********

:download:`IntelliJ-Squash-plugin-user-manual-v1.0.8.docx <_static/doc/IntelliJ-Squash-plugin-user-manual-v1.0.8.docx>`
