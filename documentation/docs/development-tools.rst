..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2018 - 2020 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

.. role:: blue
.. role:: green
.. role:: group-id
.. role:: red

.. raw:: html

    <style> .red {color:red} </style>
    <style> .blue {color:blue} </style>
    <style> .green {color:green} </style>
    <style> .group-id {color:green; font-style:italic} </style>
    <style> .advice {color:green; font-style:italic} </style>

##############################
Squash TF IntelliJ IDEA plugin
##############################

.. toctree::

***************************************
Guide for Plugin Functional Development
***************************************

Introduction
============

1. Purpose
----------

This document is a Technical Design Document for use by **IntelliJ-Squash-plugin** project.
It provides not only a global view on the infra-structure between the project modules, but also the technical/functional analysis onto each feature of the plugin project.

2. Intended audience
--------------------

* Supervisors, to analyse the design and implementation of the plugin project
* Squash TF team members
* Future developers, testers who will work with the plugin

3. Scope
--------

This document describes the structure design and the technical analysis of **IntelliJ-Squash-plugin** project.

4. Acronyms
-----------

+-----------------+--------------------------------------+
|*Acronym*        |*Definition*                          |
+=================+======================================+
|`BNF`            |`Backus-Naur Form`                    |
+-----------------+--------------------------------------+
|`DSL`            |`Domain Specific Language`            |
+-----------------+--------------------------------------+
|`IDE`            |`Integrated Development Environment`  |
+-----------------+--------------------------------------+
|`ISP`            |`IntelliJ-Squash-plugin`              |
+-----------------+--------------------------------------+
|`SKF`            |`Squash Keyword Framework`            |
+-----------------+--------------------------------------+

|

System overview
===============

The **ISP** project is a member of the :group-id:`Squash TF galaxia` project group which is open-source but belongs to @Henix company.

While the whole group purpose is to provide to grand public a toolbox for functional testing automation and is dedicated to the industrialization of automated test execution,
the goal of the **ISP** itself is to allow users (i.e. developers, testers...) to compose, execute or even test :green:`SKF DSL` via :blue:`IntelliJ Community IDE`.

This plugin project consists of 5 modules:

    * **IntelliJ-Idea Platform:** creates all dependencies in the IntelliJ-idea binaries to be used as provided dependencies for the build and packaging of the Intellij reactor.
    * **IntelliJ-Idea Platform Bundle:** allows the packaging of the IntelliJ-Idea platform dependencies for CI builds and development environment.
    * **Squash Keyword Framework Parser for IntelliJ-Idea:** parse the :green:`SKF DSL elements` into :blue:`IntelliJ PsiElements` for plugin feature development.
    * **Intellij-jflex-adapter:** creates an adapter for the version :guilabel:`1.7.0-2` of :blue:`flex` created by :blue:`Jetbrains` for :blue:`IntelliJ Community IDE`, in order to use it with the jflex plugin.
    * **Squash Keyword Framework Plugin for IntelliJ-Idea:** provides all kinds of features for :green:`SKF DSL` in :blue:`IntelliJ Community IDE` such as: autocompletion, syntax annotation/coloration...

|

Technical requirement
=====================

As **ISP** is a Maven project dedicated to create an IntelliJ IDE plugin, it is required in local:

    * JAVA JDK 1.8+
    * MAVEN (3.5.0 recommended)

    .. warning::
                The Maven path MUST contain no spaces to avoid installation problems.

    * `IntelliJ Community IDE <https://www.jetbrains.com/idea/download/other.html>`_ (version from :guilabel:`2018.3.2` to :guilabel:`2019.1.x`)
    * Git for cloning the `IntelliJ project code sources <https://github.com/JetBrains/intellij-community/releases/tag/idea%2F183.4886.37/>`_ (version :guilabel:`2018.3.2` :guilabel:`Build #IC-183.4886.37`)

PS: The document for setting an environment for the **ISP** development can be obtain :download:`here <_static/doc/Squash-TF-IntelliJ-plugin-Dev-Env-Setting-Instructions-v1.0.2.docx>`

|

Plugin component descriptions
=====================================

1. IntelliJ-Idea Platform
-------------------------

To be updated...

2. IntelliJ-Idea Platform Bundle
--------------------------------

To be updated...

3. Squash Keyword Framework Parser for IntelliJ-Idea
----------------------------------------------------

This is the core of the :green:`SKF DSL` parsing process in :blue:`IntelliJ Community IDE`.
In fact, it helps the IDE not only to identify whether or not a file belongs to a DSL (ex: .ta, .macro)
but as well to check if that file content complies with all those DSL syntax conventions.

This module consists of 2 principal packages:

    * **src/main/bnf:** contains all BNF files of the **ISP**.

        As known by its name, each BNF file is a formal notation for encoding grammars intended for human consumption.
        More detailed information about BNF can be found `here. <http://matt.might.net/articles/grammars-bnf-ebnf/>`_

        There are two BNF files of :green:`SKF DSLs` used in the **ISP**.

            * Squash Test BNF File: for files with ``.ta`` extension

                .. image:: _static/images/SquashTestBNF.png
                   :scale: 50 %
                   :alt: Squash Test BNF File

            * Squash Macro BNF File: for files with ``.macro`` extension

                .. image:: _static/images/SquashMacroBNF.png
                   :scale: 50 %
                   :alt: Squash Macro BNF File

        The elements in :green:`green color` is of type **simple**. Their value is defined by a Lexer file that will be discussed in the next chapters of this document.

        The elements in :blue:`blue color` is of type **complex**. Their value is composed of more than one **simple** or other **complex** elements.

        Each element here will then be generated into a specific JAVA class called **IntelliJ PsiElement** class which is later used for the **ISP** features development.

        .. warning::
            As a file violates one or more BNF specifications, the *first-found-troublemaker* element will be annotated as :red:`ERROR`.

            This :red:`ERROR` will disable any plugin functionality for the rest of the file.

            Moreover, it also blocks all possible development interventions as no more **IntelliJ PsiElement** could be identified.

        .. Hint::
            It is therefore strongly advised to have a discussion between the plugin developers and product owner before inserting or removing a syntax convention at this PARSING level.

    * **src/main/java:**

        This package contains JAVA classes that helps :blue:`IntelliJ Community IDE` to identify a file's language by its extension.

        It also creates Element-type and Token-type ofr a DSL, as well as implement methods in order to obtain the value of each PsiElement.

4. Intellij-jflex-adapter
-------------------------

To be updated...

5. Squash Keyword Framework Plugin for IntelliJ-Idea
----------------------------------------------------

This module whose type is **IntelliJ Plugin** is the base package of the **ISP**.

Its purpose is to develop all :green:`SKF DSL` features for :blue:`IntelliJ Community IDE`.

    * **sources/META-INF:** contains the :green:`plugin.xml` file.

        This XML file declares the :blue:`service`, the :blue:`role`, as well as the :blue:`scale` (or :blue:`life-cycle`) of JAVA classes created in the following **src/main/java** package.

            .. image:: _static/images/plugin-xml.png
                       :scale: 50 %
                       :alt: META-INF/plugin.xml

        The most important part of this file is the *service declaration and implementation* that indicates which JAVA class takes care of which plugin feature of the **ISP**.

            .. image:: _static/images/plugin-xml-main-section.png
                       :scale: 50 %
                       :alt: META-INF/plugin.xml

        .. note::
            Syntax convention for this :green:`plugin.xml` is modified by :blue:`IntelliJ` from version ``2019.2.x``.
            More information about this can be found `here <http://www.jetbrains.org/intellij/sdk/docs/tutorials/custom_language_support/language_and_filetype.html#b-register-file-type-20192-or-later>`_.

    * **src/main/java:** consists of all JAVA classes used for the plugin feature development.

        There are **9** main sections in this package:

        5.1. Project model

        This is to defines a model for the :green:`SKF` project organization in :blue:`IntelliJ Community IDE` as well tools to use it.

        To be specific, it checks whenever the current project is a valid :green:`SKF DSL` project.
        On the other hand, it helps :blue:`IntelliJ Community IDE` to get the virtual files of all TA File/Macro files in the working project for further development investigations/interactions.

        |

        5.2. Framework connector

        This module's goal is to connect the **ISP** with the :green:`SKF DSL` framework (:green:`squash-ta-new-engine` project) to obtain all the engine components such as built-in macros, converters or command...

        |

        5.3. Notification

        This service controls the notification mechanism in the **ISP** (the notif. message content, warning level, showing time...).

        |

        5.4. Language

        All JAVA classes for name, extension, icons, lexer (see ``4.5.5 section``) and parser of each :green:`SKF DSL` are defined in this section.

        |

        5.5. DSL Lexer

        Until the project re-structuring is completed, these lexers temporarily locate in the **file/lexer/** and **macro/lexer/** folders.

        :blue:`IntelliJ Community IDE` uses :green:`jflex` technology to create the lexer for each custom language.
        In fact, a lexer defines the valid value(s) for every element created by the parser of that DSL.
        If the current parsed element of a file conforms to the corresponding lexer convention, it will be assigned to an appropriate PsiElement in the **ISP**.

        There are 2 principal steps in every lexer file (.flex):

            * Declaration of variables and phases

                .. image:: _static/images/squash-test-flex-p1.png
                       :scale: 50 %
                       :alt: Declaration of variables and phases in Squash Test lexer

            * Utilisation of variables and phases to identify the parsed element

                .. image:: _static/images/squash-test-flex-p2.png
                       :scale: 50 %
                       :alt: Utilisation of variables and phases in Squash Test lexer

        More detailed information about :green:`jflex` can be found at this `link <https://jflex.de/manual.html>`_.

        .. warning::
            As in case of parsing process, if a file violates one or more FLEX specifications, the *first-found-troublemaker* element will be annotated as :red:`ERROR`.

            This :red:`ERROR` will disable any plugin functionality for the rest of the file.

            Moreover, it also blocks all possible development interventions as no more **IntelliJ PsiElement** could be identified.

        .. Hint::
            It is therefore strongly advised to have a discussion between the plugin developers and product owner before inserting or removing a syntax convention at this LEXING level.

        |

        5.6. Validation

        This is one of the 3 main features of the **ISP** added into :blue:`IntelliJ Community IDE`: defining general validation logic for :green:`SKF DSLs`.

        As you may already know, the first step of validating a file content is provided through each DSL parser and lexer.
        If passed, that file is then considered of that DSL and then the parsing process in **ISP** will store each captured item in an appropriate PsiElement object.

        This is the second validation step which is to check if this *DSL-conformed-file* is in the correct directory, if a :green:`SKF DSL` basic instruction line is 1 of 6 valid templates, if a macro line is well defined or if a metadata input syntax is correct...

        .. Note::
            The advantage of this validation step is that developer can choose when, where and how to call an :red:`error`, :blue:`warn` or :green:`info` message for each case of violation.

        In addition to the syntax validation, this section also provides the **macro signature/line tracing** ability.
        If a macro signature or a macro line of a file is found in the framework component list or in the project ``shortcuts`` folder (or its subfolders),
        a marker will be created at the beginning of that line and eventually provides the navigation to the defining macro file if the latest is in the ``shortcuts`` folder (or its subfolders).

        |

        5.7. Highlighting

        This is second features of the **ISP** added into :blue:`IntelliJ Community IDE`: providing generic highlight tools for :green:`SKF DSLs`.

        When a file is found in such a DSL by passing that language parsing process, each DSL item of this file will be associated with a corresponding PsiElement element.
        Those elements are then grouped by type.

        This functionality is to assign each element group a specific color for the user visualization.

        Besides, it creates also a ``color setting page`` for each :green:`SKF DSL` in :blue:`IntelliJ Community IDE`.

            .. image:: _static/images/color-setting-page-example.png
                       :scale: 50 %
                       :alt: Squash Test File color setting page

        |

        5.8. Autocompletion

        This is third features of the **ISP** added into :blue:`IntelliJ Community IDE`: implementing general completion logic for :green:`SKF DSLs`.

        This functionality proposes to user the most appropriate :green:`SKF DSL` elements based on the current file content and the user cursor position.
        Each :green:`SKF DSL` (Squash Test and Squash Macro) has a specific autocompletion mechanism.

            .. note::
                The common point of these two mechanisms is that the autocompletion result is at least a :green:`SKF DSL` element and at most to complete the *asking-for-completion* line.

            5.8.1. Squash Test autocompletion

            There are three different kinds of completions in a Squash Test file:

                * Phase/section completion

                    When user requests a completion by typing :guilabel:`Ctrl+Space`, the plugin will firstly check the existence of each phase/section (i.e. METADATA, SETUP, TEST, TEARDOWN) in the current file.
                    If this file is not an Ecosystem file (``setup.ta`` or ``teardown.ta``), the appropriate missing phase(s) will be proposed.

                    .. warning::
                        The TEST phase must exist in file content or it is the only result to be proposed for completion.

                * Line completion

                    When a phase is already defined and the user cursor is on an empty line, a line completion will be invoked.

                    In fact, user can choose either 1 of 6 available :green:`SKF DSL` instruction templates or simply their HEAD KEY to insert an ``basic instruction``.

                    On the other hand, he/she can get the ``#`` symbol to start a ``macro line``.

                * Element completion

                    When an autocompletion is asked on a non-empty line, the most appropriate :green:`SKF DSL` elements will be offered if that line is of type ``basic instruction``.

                    In case of ``macro line``, the :green:`element completion` functionality is not yet available for this **ISP** version.

            5.8.2. Squash Macro autocompletion

            Similarly, there are also three different kinds of completions in a Squash Macro file:

                * Macro signature completion

                    When user requests a completion for an empty ``.macro`` , a model of :green:`Squash macro signature` will be offered.
                    User can then modify the Fixed parts or/and their Parameter to define his/her own macro signature.

                    Before auto-completion:

                        .. image:: _static/images/macro-signature-model-completion.png
                           :scale: 75 %
                           :alt: Squash Macro signature model completion

                    After auto-completion:

                        .. image:: _static/images/macro-signature-model-completion-result.png
                           :scale: 72 %
                           :alt: Squash Macro signature model completion result

                * Line completion

                    :red:`Under reconstruction`

                * Element completion

                    :red:`Under reconstruction`

        |

        5.9. Tools

        This is a collection of utils (methods) used for developing the 3 features of the **ISP**.
