/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.projectmodel;

import com.intellij.openapi.project.Project;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class SquashMacroFileLocationProjectServiceTest {

    public static final String PROJECT_PATH_4_TESTS = "C:/Users/Tester/IdeaProjects/squash-tf-project";
    public static final String CAS_DANS_SOUSDIR_MACROS = "C:/Users/Tester/IdeaProjects/squash-tf-project/src/squashTA/shortcuts/subfolder/test.macro";
    public static final String CAS_DANS_DIR_MACRO_DIRECT = "C:/Users/Tester/IdeaProjects/squash-tf-project/src/squashTA/shortcuts/test.macro";
    public static final String CAS_DEHORS = "C:/Users/Tester/IdeaProjects/squash-tf-project/src/squashTA/test.macro";

    private Project testProject;
    private SquashMacroFileLocationProjectServiceImpl testee;

    @Before
    public void testeeSetup(){
        testProject= Mockito.mock(Project.class);
        Mockito.when(testProject.getBasePath()).thenReturn(PROJECT_PATH_4_TESTS);
        testee=new SquashMacroFileLocationProjectServiceImpl(testProject);
    }

    @Test
    public void definitionsInMacroFolderSubdirectoryShouldBeSeenIn() {

        Assert.assertTrue(testee.isInShortCutsFolder(CAS_DANS_SOUSDIR_MACROS));
    }

    @Test
    public void definitionsDirectlyInMacroFolderShouldBeSeenIn() {

        Assert.assertTrue(testee.isInShortCutsFolder(CAS_DANS_DIR_MACRO_DIRECT));

    }

    @Test
    public void definitionsOutsideMacroFolderShouldBeSeenOut() {

        Assert.assertFalse(testee.isInShortCutsFolder(CAS_DEHORS));
    }

}
