/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.tools;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CollectionToolkitTest {
    @Test
    public void compareKeySetTest() {
        String key1 = "ASSERT XML";
        String key2 = "IS VALID USING SCHEMA";

        List<String> expectedKeySet = new ArrayList<>();
        expectedKeySet.add(key1);
        expectedKeySet.add(key2);

        String key3 = "Assert XML ";
        String key4 = " Is valid Using SCHEMA";

        List<String> actualKeySet = new ArrayList<>();
        actualKeySet.add(key3);
        actualKeySet.add(key4);

        Assert.assertEquals(2, CollectionToolkit.compareKeySet(actualKeySet, expectedKeySet));

        List<String> actualKeySet2 = new ArrayList<>();
        actualKeySet2.add(key1);
        actualKeySet2.add("IS VALID ");
        Assert.assertEquals(3, CollectionToolkit.compareKeySet(actualKeySet2, expectedKeySet));
    }

}
