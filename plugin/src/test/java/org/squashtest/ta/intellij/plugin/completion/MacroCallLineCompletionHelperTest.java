/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.completion;

import com.intellij.psi.PsiFile;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.squashtest.ta.galaxia.enginelink.components.*;
import org.squashtest.ta.galaxia.metaexecution.enginelink.EngineLinkException;
import org.squashtest.ta.intellij.plugin.fwconnector.IdeaFrameworkConnector;
import org.squashtest.ta.intellij.plugin.macro.psi.SquashMacroMacroTitle;
import org.squashtest.ta.intellij.plugin.macro.psi.SquashMacroMacroTitleContent;
import org.squashtest.ta.intellij.plugin.macro.psi.SquashMacroMacroTitleProperty;
import org.squashtest.ta.intellij.plugin.projectmodel.SquashMacroFileLocationProjectService;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeoutException;

@RunWith(MockitoJUnitRunner.class)
public class MacroCallLineCompletionHelperTest {
    private MacroCallLineCompletionHelper testee;
    @Mock
    private SquashMacroFileLocationProjectService mockLocator;

    @Mock
    private IdeaFrameworkConnector mockFwConnector;

    @Before
    public void setTesteeUp() throws TimeoutException, EngineLinkException {
        testee=new MacroCallLineCompletionHelper(mockLocator,mockFwConnector);
        SquashDSLComponentRegistry mockRegistry = Mockito.mock(SquashDSLComponentRegistry.class);
        Mockito.when(mockFwConnector.getSquashDSLComponentRegistry()).thenReturn(mockRegistry);
        Mockito.when(mockRegistry.getMacros()).thenReturn(Arrays.asList(
                getLogAtLevel(),
                getDistractor()
        ));
        SquashMacroMacroTitle internal=getInternal();
        Mockito.when(mockLocator.getMacroTitlesInProjectShortcutsFolder()).thenReturn(Arrays.asList(
                internal
        ));
    }

    @NotNull
    protected SquashDSLMacro getLogAtLevel() {
        return new SquashDSLMacro("log-at-level", Arrays.asList(
                new SquashDSLMacroFixedPart("LOG "),
                new SquashDSLMacroParam(new SquashDSLMacroParamDefinition("content")),
                new SquashDSLMacroFixedPart(" AT "),
                new SquashDSLMacroParam(new SquashDSLMacroParamDefinition("level"))
        ));
    }

    @NotNull
    protected SquashDSLMacro getDistractor() {
        return new SquashDSLMacro("distractor", Arrays.asList(
                new SquashDSLMacroFixedPart("BUH "),
                new SquashDSLMacroParam(new SquashDSLMacroParamDefinition("content"))
        ));
    }

    @NotNull
    protected SquashDSLMacro getInitialParmMacro() {
        return new SquashDSLMacro("initial-parm-macro", Arrays.asList(
                new SquashDSLMacroParam(new SquashDSLMacroParamDefinition("initial")),
                new SquashDSLMacroFixedPart(" BUH "),
                new SquashDSLMacroParam(new SquashDSLMacroParamDefinition("content")),
                new SquashDSLMacroFixedPart(" AT "),
                new SquashDSLMacroParam(new SquashDSLMacroParamDefinition("level"))
        ));
    }

    @NotNull
    protected SquashMacroMacroTitle getInternal() {
        SquashMacroMacroTitle internal=Mockito.mock(SquashMacroMacroTitle.class);
        SquashMacroMacroTitleContent macroDef = Mockito.mock(SquashMacroMacroTitleContent.class);
        Mockito.when(internal.getMacroTitleContent()).thenReturn(macroDef);

        SquashMacroMacroTitleProperty fixed = Mockito.mock(SquashMacroMacroTitleProperty.class);
        Mockito.when(fixed.getTitleKey()).thenReturn("LOGGING ONLY COOL ");
        Mockito.when(fixed.getTitleParam()).thenReturn("");
        SquashMacroMacroTitleProperty param = Mockito.mock(SquashMacroMacroTitleProperty.class);
        Mockito.when(param.getTitleKey()).thenReturn("");
        Mockito.when(param.getTitleParam()).thenReturn("stuff");
        Mockito.when(macroDef.getMacroTitlePropertyList()).thenReturn(Arrays.asList(
                fixed,
                param
        ));
        PsiFile file = Mockito.mock(PsiFile.class);
        Mockito.when(file.getName()).thenReturn("daInternal.macro");
        Mockito.when(internal.getContainingFile()).thenReturn(file);
        return internal;
    }

    @Test
    public void thereIsOneThatFits() throws EngineLinkException {
        String primer="LOG myFoot";
        List<SquashDSLMacro> actual = testee.macroLineCompletion(primer);
        Assert.assertEquals("There should be one and only one matching",1, actual.size());
        Assert.assertEquals("Mauvaise macro sélectionnée",getLogAtLevel().getName(), actual.get(0).getName());
    }

    @Test
    public void thereWorksWithFinalSpaceToo() throws EngineLinkException {
        String primer="LOG myFoot ";
        List<SquashDSLMacro> actual = testee.macroLineCompletion(primer);
        Assert.assertEquals("There should be one and only one matching",1, actual.size());
        Assert.assertEquals("Mauvaise macro sélectionnée",getLogAtLevel().getName(), actual.get(0).getName());
    }

    @Test
    public void twoMatchesWithOneLetter() throws EngineLinkException {
        String primer="L";
        List<SquashDSLMacro> actual = testee.macroLineCompletion(primer);
        Assert.assertEquals("There should be one and only one matching",2, actual.size());
    }

    @Test
    public void findsInternalMacrosToo() throws EngineLinkException {
        String primer="LOGGING ONLY";
        List<SquashDSLMacro> actual = testee.macroLineCompletion(primer);
        Assert.assertEquals("There should be one and only one matching",1, actual.size());
        Assert.assertEquals("Mauvaise macro sélectionnée","daInternal.macro", actual.get(0).getName());
    }

    @Test
    public void noneIfNoneMatches() throws EngineLinkException {
        String primer="THIS LINE MATCHES NOTHING";
        int actualSize = testee.macroLineCompletion(primer).size();
        Assert.assertEquals("There should be one and only one matching",0, actualSize);
    }

    private MacroCallLineCompletionHelper getTesteeUpForInitialParmTest() throws TimeoutException, EngineLinkException {

        SquashMacroFileLocationProjectService specialMockLocator=Mockito.mock(SquashMacroFileLocationProjectService.class);
        IdeaFrameworkConnector specialMockFwConnector=Mockito.mock(IdeaFrameworkConnector.class);

        MacroCallLineCompletionHelper specialTestee=new MacroCallLineCompletionHelper(specialMockLocator,specialMockFwConnector);
        SquashDSLComponentRegistry mockRegistry = Mockito.mock(SquashDSLComponentRegistry.class);
        Mockito.when(specialMockFwConnector.getSquashDSLComponentRegistry()).thenReturn(mockRegistry);
        Mockito.when(mockRegistry.getMacros()).thenReturn(Arrays.asList(
                getLogAtLevel(),
                getDistractor(),
                getInitialParmMacro()
        ));
        SquashMacroMacroTitle internal=getInternal();
        Mockito.when(specialMockLocator.getMacroTitlesInProjectShortcutsFolder()).thenReturn(Arrays.asList(
                internal
        ));
        return specialTestee;
    }

    @Test
    public void initialParmWouldMakeTheDarnMacroMatchAnyPrimer() throws TimeoutException, EngineLinkException {
        String primer="THIS LINE MATCHES NOTHING";
        List<SquashDSLMacro> actual = getTesteeUpForInitialParmTest().macroLineCompletion(primer);
        Assert.assertEquals("There should be one and only one matching",1, actual.size());
        Assert.assertEquals("Mauvaise macro sélectionnée","initial-parm-macro", actual.get(0).getName());
    }

    @Test
    public void allForEmpty()  throws EngineLinkException {
        List<SquashDSLMacro> actualSelection = testee.macroLineCompletion("");
        Assert.assertEquals("All macros should be selected if primer is empty", 3, actualSelection.size());
        actualSelection.stream().anyMatch(squashDSLMacro -> {return squashDSLMacro.getName().equals("log-at-level");});
        actualSelection.stream().anyMatch(squashDSLMacro -> {return squashDSLMacro.getName().equals("distractor");});
        actualSelection.stream().anyMatch(squashDSLMacro -> {return squashDSLMacro.getName().equals("daInternal.macro");});
    }

    //completion build tests

    @NotNull
    private SquashDSLMacro getBigMacro() {
        return new SquashDSLMacro("log-at-level-in-console", Arrays.asList(
                new SquashDSLMacroFixedPart("LOG "),
                new SquashDSLMacroParam(new SquashDSLMacroParamDefinition("content")),
                new SquashDSLMacroFixedPart(" AT "),
                new SquashDSLMacroParam(new SquashDSLMacroParamDefinition("level")),
                new SquashDSLMacroFixedPart(" IN CONSOLE")
        ));
    }



    @Test
    public void primerOnlyPartOfFirstFixedPart(){
        Set<String> actualList=testee.buildCompletionProposalsForMacro("BU",getDistractor());
        Assert.assertEquals(expectedSet("H {content}"),actualList);
    }

    @NotNull
    private HashSet<String> expectedSet(String... expectedStrings) {
        return new HashSet<>(Arrays.asList(expectedStrings));
    }

    @Test
    public void firstPartContainsAllFixed(){
        Set<String> actualSet=testee.buildCompletionProposalsForMacro("BUH ",getDistractor());
        Assert.assertEquals(expectedSet("{content}"),actualSet);
    }


    @Test
    public void needToAddSpaceAfterFixedForParm(){
        Set<String> actualSet=testee.buildCompletionProposalsForMacro("BUH",getDistractor());
        Assert.assertEquals(expectedSet(" {content}"),actualSet);
    }

    @Test
    public void ifFirstTokenAndRoom4ParmShouldSuggestNextFixed(){
        Set<String> actualSet=testee.buildCompletionProposalsForMacro("LOG toto",getLogAtLevel());
        Assert.assertEquals(expectedSet(" AT {level}"),actualSet);
    }

    @Test
    public void supportTheCaseWhereItEndsWithAFixed(){
        SquashDSLMacro bigMacro = getBigMacro();
        Set<String> actualSet=testee.buildCompletionProposalsForMacro("LOG toto",bigMacro);
        Assert.assertEquals(expectedSet(" AT {level} IN CONSOLE"),actualSet);
    }

    @Test
    public void generateSeveralProposalsWhenSeveralFixedMayMatch(){
        SquashDSLMacro bigMacro=getBigMacro();
        Set<String> actualSet=testee.buildCompletionProposalsForMacro("LOG toto AT debug",bigMacro);
        Assert.assertEquals(expectedSet(" IN CONSOLE"," AT {level} IN CONSOLE"),actualSet);
    }

    @Test
    public void partialEndInFixedPartWithSeveralHypotheses(){
        SquashDSLMacro bigMacro=getBigMacro();
        Set<String> actualSet=testee.buildCompletionProposalsForMacro("LOG toto AT debug IN",bigMacro);

        Assert.assertEquals(expectedSet(
                " CONSOLE", // hypothesis that the final IN is the beginning of the 'IN CONSOLE' fixed part
                " IN CONSOLE",               // hypothesis that the final IN is part of the previous {level} parameter value
                " AT {level} IN CONSOLE"     // hypothesis that 'toto AT debug IN' is nothing but the value for parm {content}
        ),actualSet);
    }

    @Test
    public void supportInitialParmMacros(){
        SquashDSLMacro bigMacro=getInitialParmMacro();
        Set<String> actualSet=testee.buildCompletionProposalsForMacro("Let's groove but not BUH AT the man",bigMacro);

        Assert.assertEquals(expectedSet(
                " BUH {content} AT {level}", // This is the case where the {initial} parm matches everything
                " AT {level}"   // and here is the case where it only matches untill 'not', and we get to suggest the next elements
        ),actualSet);
    }

}
