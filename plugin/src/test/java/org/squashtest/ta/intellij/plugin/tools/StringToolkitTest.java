/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.tools;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class StringToolkitTest {
    @Test
    public void getIthIndexesTest() {
        String testee = "//This is a comment line.\nSETUP :     \nLOAD abc AS xyz\n\n";
        String SETUP="SETUP :";

        LineSearchingResult firstSetUpPhase = StringToolkit.getIthIndexes(testee, SETUP, "[ \t]*\n?",1);
        Assert.assertEquals(26, firstSetUpPhase.getStartIndex());
        Assert.assertEquals(39, firstSetUpPhase.getEndIndex());
        Assert.assertEquals(SETUP, firstSetUpPhase.getContent().trim());

        LineSearchingResult secondSetUpPhase = StringToolkit.getIthIndexes(testee, SETUP, "[ \t]*\n?",2);
        Assert.assertFalse("Should not have found setup phase.", secondSetUpPhase.wasFound());
    }

    @Test
    public void getAllIndexesTest() {
        String testee = "//This is a comment line.\nSETUP :     \nLOAD abc AS xyz\n\n";
        String SETUP="SETUP :";

        List<LineSearchingResult> resultList = StringToolkit.getAllIndexes(testee, SETUP, "[ \t]*\n?");
        Assert.assertEquals(1, resultList.size());
        LineSearchingResult result = resultList.get(0);
        Assert.assertEquals(26, result.getStartIndex());
        Assert.assertEquals(39, result.getEndIndex());
        Assert.assertEquals(SETUP, result.getContent().trim());
    }

    @Test
    public void getFirstCompleteOccurrence(){
        String content = "# LOG toto AT {level}";
        LineSearchingResult result= StringToolkit.findCompleteOccurrenceOrPartialEndingOccurrence(content,"AT ", 0);
        Assert.assertTrue(result.wasFound());
        Assert.assertEquals(11,result.getStartIndex());
        Assert.assertEquals(14,result.getEndIndex());
    }

    @Test
    public void getFirstOccurrenceAfterUsed(){
        String content = "# LOG toto AT {level} FOR USE AT {site}";
        LineSearchingResult result= StringToolkit.findCompleteOccurrenceOrPartialEndingOccurrence(content,"AT ", 13);
        Assert.assertTrue(result.wasFound());
        Assert.assertEquals(30,result.getStartIndex());
        Assert.assertEquals(33,result.getEndIndex());
    }

    @Test
    public void getPartialFinalOccurrence(){
        String content = "# LOG toto AT {level} CLOW";
        LineSearchingResult result= StringToolkit.findCompleteOccurrenceOrPartialEndingOccurrence(content,"CLOWN ", 0);
        Assert.assertTrue(result.wasFound());
        Assert.assertEquals(22,result.getStartIndex());
        Assert.assertEquals(26,result.getEndIndex());
    }

    @Test
    public void mustLookForMissingEndOccurrenceOfLongerStringAndNotFind(){
        String content = "ASSERT {result}";
        LineSearchingResult result=StringToolkit.findCompleteOccurrenceOrPartialEndingOccurrence(content," IS FAILURE WITH EXIT CODE",0);
        Assert.assertFalse(result.wasFound());
    }

    @Test
    public void mustLookForRealEndOccurrenceOfLongerStringAndFind(){
        String content = "ASSERT {result} IS FAIL";
        LineSearchingResult result=StringToolkit.findCompleteOccurrenceOrPartialEndingOccurrence(content," IS FAILURE WITH EXIT CODE",0);
        Assert.assertTrue(result.wasFound());
        Assert.assertEquals(" IS FAIL",result.getContent());
    }

    @Test
    public void partialNotAtEndIsNotAMatch(){
        String content = "ASSERT IS FAIL FALSE+";
        LineSearchingResult result=StringToolkit.findCompleteOccurrenceOrPartialEndingOccurrence(content," IS FAILURE WITH EXIT CODE",0);
        Assert.assertFalse(result.wasFound());
    }
}
