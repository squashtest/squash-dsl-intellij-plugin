/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.projectmodel;

import com.intellij.openapi.project.Project;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class SquashTestFileLocationServiceTest {

    public static final String CAS_TRUE_ECOSYSTEM_DEFAUT = "C:/Users/Tester/IdeaProjects/squash-tf-project/src/squashTA/tests/test.ta";
    public static final String CAS_TRUE_ECOSYSTEME_NON_DEFAUT = "C:/Users/Tester/IdeaProjects/squash-tf-project/src/squashTA/tests/junit/junit-test.ta";
    public static final String CAS_FALSE = "C:/Users/Tester/IdeaProjects/squash-tf-project/src/squashTA/test.ta";
    public static final String PROJECT_PATH_4_TESTS = "C:/Users/Tester/IdeaProjects/squash-tf-project";


    private Project testProject;
    private SquashTestFileLocationService testee;

    @Before
    public void testeeSetup(){
        testProject= Mockito.mock(Project.class);
        Mockito.when(testProject.getBasePath()).thenReturn(PROJECT_PATH_4_TESTS);
        testee=new SquashTestFileLocationService(testProject);
    }

    @Test
    public void testsInDefaultEcosystemShouldBeSeenAsIn() {

        Assert.assertTrue(testee.isInTestsFolder(CAS_TRUE_ECOSYSTEM_DEFAUT));
    }

    @Test
    public void testsInAnyNonDefaultEcosystemShouldBeSeenAsIn() {

        Assert.assertTrue(testee.isInTestsFolder(CAS_TRUE_ECOSYSTEME_NON_DEFAUT));
    }

    @Test
    public void testsOutsideTestTreeShouldBeSeenAsOut() {

        Assert.assertFalse(testee.isInTestsFolder(CAS_FALSE));
    }
}
