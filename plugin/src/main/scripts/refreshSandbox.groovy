/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
import org.apache.commons.io.FileUtils

import java.nio.file.CopyOption
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.StandardCopyOption

// To activate this, you'll need to tell Idea to build with -Diead.sandbox.root=<sandbox root path>
def sandboxroot=System.properties.'idea.cfg.root'

def sandboxrootDir = new File(sandboxroot)

def pluginDeployment = new File(sandboxrootDir,"/system/plugins-sandbox/plugins/${project.artifactId}")

//Here we try to clean a previous deployment, with all that error checking a rotten designer failed to do...
if(pluginDeployment.deleteDir()){
    println pluginDeployment.getAbsolutePath()+" cleared, proceeding with debug deploy"
}else if(pluginDeployment.exists()){
    throw new IOException("Failed to delete "+pluginDeployment.getAbsolutePath()+" aborting debug deploy.")
}else if(sandboxrootDir.isDirectory()) {
    //Yep, first time there's nothing to clean
    println pluginDeployment.getAbsolutePath()+" did not exist, proceeding with debug deploy."
}else{
    throw new IOException("IntelliJ-idea Sandbox directory "+sandboxrootDir.getAbsolutePath()+" does not exist")
}

//There, we deploy our development plugin version in the sandbox for debug !
def debugPackage= new File("${project.basedir}/target/${project.artifactId}")

FileUtils.copyDirectory(debugPackage,pluginDeployment)
