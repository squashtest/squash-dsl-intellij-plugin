/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.notification;

import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

/**
 * Manages data from notifications to throttle notifications (avoid mass-notifying the same error, owing to some parts
 * of our code which may trigger the same error repeatedly in a short time) reliably, while NOT leaving records forever
 * as this would create memory leaks in some circumstances.
 */
class NotificationThrottle implements Runnable{
    public static final Logger LOGGER = LoggerFactory.getLogger(NotificationThrottle.class);

    /** Memorizing last publication timestamp by notification content. */
    private Map<String, Date> notificationLastTime=new HashMap<>();
    /** Period between two publications of the same message (milliseconds) */
    private long publishingPeriod;

    private Executor ex=Executors.newCachedThreadPool(new ExpiryDaemonThreadFactory());

    /** Each deamon checks that none other has been launched, and die otherwise. */
    private Thread expiryDeamon;

    static interface WallClock{
        Date getCurrentTime();
    }
    private static class SystemWallClock implements WallClock{
        @Override
        public Date getCurrentTime() {
            return new Date();
        }
    }

    private final WallClock clock;

    /**
     * This constructor allows throttle testing.
     * @param period
     * @param clock
     */
    NotificationThrottle(long period, WallClock clock){
        this.publishingPeriod=period;
        this.clock=clock;
    }

    /**
     * Full initialization constructor.
     * @param period length of the minimal period between two identical messages (milliseconds)
     */
    public NotificationThrottle(long period){
        this(period,new SystemWallClock());
    }

    public synchronized boolean isInThrottlingPeriod(String messageSignature) {
        Date now = clock.getCurrentTime();
        if (notificationLastTime.containsKey(messageSignature)) {
            Date lastNotification = notificationLastTime.get(messageSignature);
            if (now.getTime() - lastNotification.getTime() < publishingPeriod) {
                return true;
            }
        }
        notificationLastTime.put(messageSignature, now);
        launchExpiryDeamon();
        return false;
    }

    @Override
    public void run() {
        LOGGER.debug("Starting throttle cleanup deamon for throttle {}.",this);
        registerAsLastDeamon();
        while (isLastDaemon() && hasRecords()){
            try {
                Thread.sleep(publishingPeriod);
            } catch (InterruptedException e) {
                //NOSONAR : yes, we log this exception strangely, because intelliJ messes with logging and esnds the user alarming messages when exceptions are logged regardless of the loglevel.
                LOGGER.debug("The throttle record wiping demaon has been interrupted in its sleep. How rude XD"+e);
            }

            for(String messageKey: getMessageKeySet()){
                Date currentTime = clock.getCurrentTime();
                removeRecordIfExpired(messageKey, currentTime);
            }
        }
        LOGGER.debug("Throttle cleanup deamon has finished its work. Let it Rest In Pieces ;)");
    }

    @NotNull
    protected synchronized HashSet<String> getMessageKeySet() {
        return new HashSet<>(notificationLastTime.keySet());
    }

    protected synchronized void removeRecordIfExpired(String messageKey, Date currentTime) {
            if (notificationLastTime.containsKey(messageKey) && currentTime.getTime() > notificationLastTime.get(messageKey).getTime() + publishingPeriod) {
                Date date = notificationLastTime.remove(messageKey);
                LOGGER.debug("Message {} was last logged at {}, as periode is {} ms, it is now ({}) expired.", messageKey, date, publishingPeriod, currentTime);
            }
    }

    /**
     * Check if notification time records exist (to know if we need to sweep)
     */
    protected synchronized boolean hasRecords(){
        return !notificationLastTime.isEmpty();
    }

    /**
     * Check if another expiry daemon task has been created.
     */
    protected boolean isLastDaemon(){
        return this.expiryDeamon==Thread.currentThread();
    }

    /**
     * State that the current threads is executing the last created expiry daemon thread.
     */
    protected synchronized void registerAsLastDeamon(){
        registerLastDeamon(Thread.currentThread());
    }

    /**
     * State that another thread is executing the last created expiry daemon thread.
     * @param deamonThread the thread for the new daemon task.
     */
    protected synchronized void registerLastDeamon(Thread deamonThread){
        this.expiryDeamon=deamonThread;
    }

    /**
     * Launch a new expiry deamon thread (so that each time we record a date for throttling, we known we have a task to wipe it).
     */
    protected void launchExpiryDeamon(){
         ex.execute(this);
    }

    /**
     * Thread factory to set expiry daemon threads up.
     */
    private static class ExpiryDaemonThreadFactory implements ThreadFactory {
        @Override
        public Thread newThread(@NotNull Runnable runnable) {
            Thread newWorker = new Thread(runnable);
            newWorker.setName("Throttle worker "+newWorker.toString());
            newWorker.setDaemon(true);
            return newWorker;
        }
    }
}
