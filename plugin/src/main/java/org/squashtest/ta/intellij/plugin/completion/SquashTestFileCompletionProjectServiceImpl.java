/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.completion;

import com.intellij.codeInsight.completion.CompletionParameters;
import com.intellij.codeInsight.completion.CompletionResultSet;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.galaxia.metaexecution.enginelink.EngineLinkException;
import org.squashtest.ta.intellij.plugin.tools.LineSearchingResult;
import org.squashtest.ta.intellij.plugin.tools.StringToolkit;

import java.util.HashMap;
import java.util.Map;

public class SquashTestFileCompletionProjectServiceImpl extends SquashCompletionProjectServiceImpl implements SquashTestFileCompletionProjectService {
    private static final String METADATA_PHASE = "METADATA :";
    private static final String SETUP_PHASE = "SETUP :";
    private static final String TEST_PHASE = "TEST :";
    private static final String TEARDOWN_PHASE = "TEARDOWN :";

    private static final String END_LINE_REGEX = "[ \t]*\n?";

    private static final Logger LOGGER = LoggerFactory.getLogger(SquashTestFileCompletionContributor.class);

    private final DSLInstructionLineCompletionHelper dslInstructionLineCompletionHelper;

    private String beforeACBackToEndLine;

    private String afterACUpToEndLine;

    public SquashTestFileCompletionProjectServiceImpl(MacroCallLineCompletionHelper macroCallLineCompletionHelper, DSLInstructionLineCompletionHelper dslInstructionLineCompletionHelper) {
        super(macroCallLineCompletionHelper);
        this.dslInstructionLineCompletionHelper=dslInstructionLineCompletionHelper;
    }

    @Override
    public void addCompletion(CompletionParameters parameters, CompletionResultSet result) {

        PsiElement position = parameters.getPosition();
        int autoCompletePos = position.getNode().getStartOffset();

        PsiFile taTestFile = parameters.getOriginalFile();
        String filePath = taTestFile.getVirtualFile().getPath();
        String docContent = parameters.getEditor().getDocument().getText();

        beforeACBackToEndLine = dslInstructionLineCompletionHelper.getInlineTextBeforeACPosition(docContent, autoCompletePos);
        afterACUpToEndLine = dslInstructionLineCompletionHelper.getInlineTextAfterACPosition(docContent, autoCompletePos);

        try {
            if (filePath.endsWith("setup.ta") || filePath.endsWith("teardown.ta")) {
                macroOrCommandLineCompletion(docContent, position, result);
            }else{
                LineSearchingResult firstMetadataSection = StringToolkit.getIthIndexes(docContent, METADATA_PHASE, END_LINE_REGEX,1);

                LineSearchingResult firstSetUpPhase = StringToolkit.getIthIndexes(docContent, SETUP_PHASE, END_LINE_REGEX,1);
                LineSearchingResult firstTestPhase = StringToolkit.getIthIndexes(docContent, TEST_PHASE, END_LINE_REGEX, 1);
                LineSearchingResult firstTearDownPhase = StringToolkit.getIthIndexes(docContent, TEARDOWN_PHASE, END_LINE_REGEX, 1);

                phaseCompletion(docContent, position, firstMetadataSection, firstSetUpPhase, firstTestPhase, firstTearDownPhase, result);
            }
        } catch (EngineLinkException e) {
            LOGGER.error("AUTO-COMPLETION: "+filePath +": We failed to retrieve the component registry handle: "+e);
        }
    }

    private void phaseCompletion(String docContent, PsiElement position, LineSearchingResult firstMetadataSection, LineSearchingResult firstSetupPhase,
                                 LineSearchingResult firstTestPhase, LineSearchingResult firstTeardownPhase, CompletionResultSet result) throws EngineLinkException {
        int autoCompletePos = position.getNode().getStartOffset();

        LOGGER.debug("Autocompletion starting at position : " + autoCompletePos);
        LOGGER.debug("Phase/section checking in current TA test file...");

        String phaseControlString = new SquashTestFilePhaseControl(firstMetadataSection, firstSetupPhase, firstTestPhase, firstTeardownPhase).findPhases();
        Map<String, LineSearchingResult> phaseProperties;

        switch (phaseControlString) {
            case "EMPTY":
                addPhaseLookupToCompletionResult(result, METADATA_PHASE);
                addPhaseLookupToCompletionResult(result, SETUP_PHASE);
                addPhaseLookupToCompletionResult(result, TEST_PHASE);
                addPhaseLookupToCompletionResult(result, TEARDOWN_PHASE);
                break;
            case "EMPTY_METADATA":
                metadataSectionAC(firstMetadataSection, result, autoCompletePos);
                break;
            case "EMPTY_TEARDOWN":
                teardownPhaseAC(docContent, position, firstTeardownPhase, result, autoCompletePos);
                break;
            case "EMPTY_TEST":
                testPhaseAC(docContent, position, firstTestPhase, result, autoCompletePos);
                break;
            case "EMPTY_SETUP":
                setupPhaseAC(docContent, position, firstSetupPhase, result, autoCompletePos);
                break;
            case "EMPTY_TEST_TEARDOWN":
                testTeardownPhasesAC(docContent, position, firstTestPhase, firstTeardownPhase, result, autoCompletePos);
                break;
            case "EMPTY_SETUP_TEARDOWN":
                setupTeardownPhasesAC(docContent, position, firstSetupPhase, firstTeardownPhase, result, autoCompletePos);
                break;
            case "EMPTY_METADATA_TEARDOWN":
                metadataTeardownPhasesAC(docContent, position, firstMetadataSection, firstTeardownPhase, result, autoCompletePos);
                break;
            case "EMPTY_SETUP_TEST":
                setupTestPhasesAC(docContent, position, firstSetupPhase, firstTestPhase, result, autoCompletePos);
                break;
            case "EMPTY_METADATA_TEST":
                metadataTestPhasesAC(docContent, position, firstMetadataSection, firstTestPhase, result, autoCompletePos);
                break;
            case "EMPTY_METADATA_SETUP":
                metadataSetupPhasesAC(docContent, position, firstSetupPhase, result, autoCompletePos);
                break;
            case "EMPTY_SETUP_TEST_TEARDOWN":
                phaseProperties = getPhaseSearchingResult(null, firstSetupPhase, firstTestPhase, firstTeardownPhase);
                setupTestTeardownPhasesAC(docContent, position, result, autoCompletePos, phaseProperties);
                break;
            case "EMPTY_METADATA_TEST_TEARDOWN":
                phaseProperties = getPhaseSearchingResult(firstMetadataSection, null, firstTestPhase, firstTeardownPhase);
                metadataTestTeardownPhasesAC(docContent, position, result, autoCompletePos, phaseProperties);
                break;
            case "EMPTY_METADATA_SETUP_TEARDOWN":
                phaseProperties = getPhaseSearchingResult(firstMetadataSection, firstSetupPhase, null, firstTeardownPhase);
                metadataSetupTeardownPhasesAC(docContent, position, result, autoCompletePos, phaseProperties);
                break;
            case "EMPTY_METADATA_SETUP_TEST":
                phaseProperties = getPhaseSearchingResult(firstMetadataSection, firstSetupPhase, firstTestPhase, null);
                metadataSetupTestPhasesAC(docContent, position, result, autoCompletePos, phaseProperties);
                break;
            case "EMPTY_METADATA_SETUP_TEST_TEARDOWN":
                phaseProperties = getPhaseSearchingResult(firstMetadataSection, firstSetupPhase, firstTestPhase, firstTeardownPhase);
                metadataSetupTestTeardownPhasesAC(docContent, position, result, autoCompletePos, phaseProperties);
                break;
            default:
                break;
        }
    }

    private void metadataSetupTestTeardownPhasesAC(String docContent, PsiElement position, CompletionResultSet result, int autoCompletePos, Map<String, LineSearchingResult> phaseProperties) throws EngineLinkException {
        LineSearchingResult firstSetupPhase = phaseProperties.get(SETUP_PHASE);
        LineSearchingResult firstTestPhase = phaseProperties.get(TEST_PHASE);
        LineSearchingResult firstTeardownPhase = phaseProperties.get(TEARDOWN_PHASE);

        if (firstSetupPhase.before(autoCompletePos) && firstTestPhase.after(autoCompletePos)) {
            //in setup phase
            macroOrCommandLineCompletion(docContent, position, result);
            return;
        }
        if (firstTestPhase.before(autoCompletePos) && firstTeardownPhase.after(autoCompletePos)){
            //in test phase
            macroOrCommandLineCompletion(docContent, position, result);
            return;
        }
        if (firstTeardownPhase.before(autoCompletePos)){
            //in teardown phase
            macroOrCommandLineCompletion(docContent, position, result);
        }
    }

    private void metadataSetupTeardownPhasesAC(String docContent, PsiElement position, CompletionResultSet result, int autoCompletePos, Map<String, LineSearchingResult> phaseProperties) throws EngineLinkException {
        LineSearchingResult firstSetupPhase = phaseProperties.get(SETUP_PHASE);
        LineSearchingResult firstTeardownPhase = phaseProperties.get(TEARDOWN_PHASE);

        if (firstTeardownPhase.before(autoCompletePos)){
            //in teardown phase
            macroOrCommandLineCompletion(docContent, position, result);
            return;
        }

        if (firstSetupPhase.before(autoCompletePos) && firstTeardownPhase.after(autoCompletePos)){
            //in setup phase
            macroOrCommandLineCompletion(docContent, position, result);
            addPhaseLookupToCompletionResult(result, TEST_PHASE);
        }
    }

    private void metadataTestPhasesAC(String docContent, PsiElement position, LineSearchingResult firstMetadataSection, LineSearchingResult firstTestPhase, CompletionResultSet result, int autoCompletePos) throws EngineLinkException {
        if (firstMetadataSection.before(autoCompletePos) && firstTestPhase.after(autoCompletePos)){
            addPhaseLookupToCompletionResult(result, SETUP_PHASE);
            return;
        }
        if (firstTestPhase.before(autoCompletePos)){
            macroOrCommandLineCompletion(docContent, position, result);
            addPhaseLookupToCompletionResult(result, TEARDOWN_PHASE);
        }
    }

    private void metadataTeardownPhasesAC(String docContent, PsiElement position, LineSearchingResult firstMetadataSection, LineSearchingResult firstTeardownPhase, CompletionResultSet result, int autoCompletePos) throws EngineLinkException {
        if (firstMetadataSection.before(autoCompletePos) && firstTeardownPhase.after(autoCompletePos)){
            addPhaseLookupToCompletionResult(result, SETUP_PHASE);
            addPhaseLookupToCompletionResult(result, TEST_PHASE);
            return;
        }
        if (firstTeardownPhase.before(autoCompletePos)){
            macroOrCommandLineCompletion(docContent, position, result);
        }
    }

    private void metadataSetupPhasesAC(String docContent, PsiElement position, LineSearchingResult firstSetupPhase, CompletionResultSet result, int autoCompletePos) throws EngineLinkException {
        if (firstSetupPhase.before(autoCompletePos)){
            addPhaseLookupToCompletionResult(result, TEST_PHASE);
            addPhaseLookupToCompletionResult(result, TEARDOWN_PHASE);
            macroOrCommandLineCompletion(docContent, position, result);
        }
    }

    private void metadataSectionAC(LineSearchingResult firstMetadataSection, CompletionResultSet result, int autoCompletePos) {
        if (firstMetadataSection.before(autoCompletePos)) {
            addPhaseLookupToCompletionResult(result, SETUP_PHASE);
            addPhaseLookupToCompletionResult(result, TEST_PHASE);
            addPhaseLookupToCompletionResult(result, TEARDOWN_PHASE);
        }
    }

    @NotNull
    private Map<String, LineSearchingResult> getPhaseSearchingResult(LineSearchingResult firstMetadataSection, LineSearchingResult firstSetupPhase, LineSearchingResult firstTestPhase, LineSearchingResult firstTeardownPhase) {
        Map<String, LineSearchingResult> phaseProperties = new HashMap<>();
        phaseProperties.put(METADATA_PHASE, firstMetadataSection);
        phaseProperties.put(SETUP_PHASE, firstSetupPhase);
        phaseProperties.put(TEST_PHASE, firstTestPhase);
        phaseProperties.put(TEARDOWN_PHASE, firstTeardownPhase);
        return phaseProperties;
    }

    private void teardownPhaseAC (String docContent, PsiElement position, LineSearchingResult firstTeardownPhase, CompletionResultSet result, int autoCompletePos) throws EngineLinkException {
        if (firstTeardownPhase.after(autoCompletePos)) {
            addPhaseLookupToCompletionResult(result, METADATA_PHASE);
            addPhaseLookupToCompletionResult(result, SETUP_PHASE);
            addPhaseLookupToCompletionResult(result, TEST_PHASE);
            return;
        }
        if (firstTeardownPhase.before(autoCompletePos)) {
            macroOrCommandLineCompletion(docContent, position, result);
        }
    }

    /**
     * TODO : you might (icy threatening smile bordering on murderous schizo-paranoïd seizure) want to break this down OR rewrite it with more structure
     */
    private void setupTestTeardownPhasesAC(String docContent, PsiElement position, CompletionResultSet result, int autoCompletePos, Map<String, LineSearchingResult> phaseProperties) throws EngineLinkException {
        LineSearchingResult firstSetupPhase = phaseProperties.get(SETUP_PHASE);
        LineSearchingResult firstTestPhase = phaseProperties.get(TEST_PHASE);
        LineSearchingResult firstTeardownPhase = phaseProperties.get(TEARDOWN_PHASE);

        //TODO : I have no 'Aspirine du Rhône' stocks, so I guess we'll have to rewrite this :D
        //Quan: I did rewrite it somehow :D
        if (firstSetupPhase.after(autoCompletePos)) {
            //before setup phase
            addPhaseLookupToCompletionResult(result, METADATA_PHASE);
            return;
        }
        if (firstSetupPhase.before(autoCompletePos) && firstTestPhase.after(autoCompletePos)) {
            //in setup phase
            macroOrCommandLineCompletion(docContent, position, result);
            return;
        }
        if (firstTestPhase.before(autoCompletePos) && firstTeardownPhase.after(autoCompletePos)){
            //in test phase
            macroOrCommandLineCompletion(docContent, position, result);
            return;
        }
        if (firstTeardownPhase.before(autoCompletePos)) {
            //in teardown phase
            macroOrCommandLineCompletion(docContent, position, result);
        }
    }

    private void metadataTestTeardownPhasesAC(String docContent, PsiElement position, CompletionResultSet result, int autoCompletePos, Map<String, LineSearchingResult> phaseProperties) throws EngineLinkException {
        LineSearchingResult firstMetadataSection = phaseProperties.get(METADATA_PHASE);
        LineSearchingResult firstTestPhase = phaseProperties.get(TEST_PHASE);
        LineSearchingResult firstTeardownPhase = phaseProperties.get(TEARDOWN_PHASE);

        if (firstMetadataSection.before(autoCompletePos) && firstTestPhase.after(autoCompletePos)) {
            //in metadata phase
            addPhaseLookupToCompletionResult(result, SETUP_PHASE);
            return;
        }

        if (firstTestPhase.before(autoCompletePos) && firstTeardownPhase.after(autoCompletePos)){
            //in test phase
            macroOrCommandLineCompletion(docContent, position, result);
            return;
        }
        if (firstTeardownPhase.before(autoCompletePos)) {
            //in teardown phase
            macroOrCommandLineCompletion(docContent, position, result);
        }
    }

    private void metadataSetupTestPhasesAC(String docContent, PsiElement position, CompletionResultSet result, int autoCompletePos, Map<String, LineSearchingResult> phaseProperties) throws EngineLinkException {
        LineSearchingResult firstSetupPhase = phaseProperties.get(SETUP_PHASE);
        LineSearchingResult firstTestPhase = phaseProperties.get(TEST_PHASE);

        if (firstSetupPhase.before(autoCompletePos) && firstTestPhase.after(autoCompletePos)) {
            //in setup phase
            macroOrCommandLineCompletion(docContent, position, result);
            return;
        }
        if (firstTestPhase.before(autoCompletePos)){
            //in test phase
            addPhaseLookupToCompletionResult(result, TEARDOWN_PHASE);
            macroOrCommandLineCompletion(docContent, position, result);
        }
    }

    private void setupTestPhasesAC(String docContent, PsiElement position, LineSearchingResult firstSetupPhase, LineSearchingResult firstTestPhase, CompletionResultSet result, int autoCompletePos) throws EngineLinkException {
        if (firstSetupPhase.after(autoCompletePos)) {
            //before setup phase
            addPhaseLookupToCompletionResult(result, METADATA_PHASE);
            return;
        }
        if (firstSetupPhase.before(autoCompletePos) && (firstTestPhase.after(autoCompletePos))) {
            //in setup phase
            macroOrCommandLineCompletion(docContent, position, result);
            return;
        }
        if ( firstTestPhase.before(autoCompletePos)) {
            //in test phase
            macroOrCommandLineCompletion(docContent, position, result);
            addPhaseLookupToCompletionResult(result, TEARDOWN_PHASE);
        }
    }

    private void setupTeardownPhasesAC(String docContent, PsiElement position, LineSearchingResult firstSetupPhase, LineSearchingResult firstTeardownPhase, CompletionResultSet result, int autoCompletePos) throws EngineLinkException {
        if (firstSetupPhase.before(autoCompletePos) && firstTeardownPhase.after(autoCompletePos)){
            macroOrCommandLineCompletion(docContent, position, result);
            addPhaseLookupToCompletionResult(result, TEST_PHASE);
            return;
        }
        if (firstTeardownPhase.before(autoCompletePos)){
            macroOrCommandLineCompletion(docContent, position, result);
            return;
        }
        if (firstSetupPhase.after(autoCompletePos)){
            addPhaseLookupToCompletionResult(result, METADATA_PHASE);
        }
    }

    private void testTeardownPhasesAC(String docContent, PsiElement position, LineSearchingResult firstTestPhase, LineSearchingResult firstTeardownPhase, CompletionResultSet result, int autoCompletePos) throws EngineLinkException {
        if (firstTestPhase.after(autoCompletePos )) {
            //before test phase
            addPhaseLookupToCompletionResult(result, METADATA_PHASE);
            addPhaseLookupToCompletionResult(result, SETUP_PHASE);
            return;
        }
        if (firstTestPhase.before(autoCompletePos ) && firstTeardownPhase.after(autoCompletePos)) {
            //in test phase
            macroOrCommandLineCompletion(docContent, position, result);
            return;
        }
        if (firstTeardownPhase.before(autoCompletePos)) {
            //in teardown phase
            macroOrCommandLineCompletion(docContent, position, result);
        }
    }

    private void setupPhaseAC(String docContent, PsiElement position, LineSearchingResult firstSetupPhase, CompletionResultSet result, int autoCompletePos) throws EngineLinkException {
        if (firstSetupPhase.after(autoCompletePos)) {
            addPhaseLookupToCompletionResult(result, METADATA_PHASE);
            return;
        }
        if (firstSetupPhase.before(autoCompletePos)) {
            macroOrCommandLineCompletion(docContent, position, result);
            addPhaseLookupToCompletionResult(result, TEST_PHASE);
            addPhaseLookupToCompletionResult(result, TEARDOWN_PHASE);
        }
    }

    private void testPhaseAC(String docContent, PsiElement position, LineSearchingResult firstTestPhase, CompletionResultSet result, int autoCompletePos) throws EngineLinkException {
        if (firstTestPhase.after(autoCompletePos)) {
            //before test phase
            addPhaseLookupToCompletionResult(result, METADATA_PHASE);
            addPhaseLookupToCompletionResult(result, SETUP_PHASE);
            return;
        }
        if (firstTestPhase.before(autoCompletePos)) {
            //in test phase
            macroOrCommandLineCompletion(docContent, position, result);
            addPhaseLookupToCompletionResult(result, TEARDOWN_PHASE);
        }
    }

    private void addPhaseLookupToCompletionResult(CompletionResultSet result, String str) {
        LOGGER.debug("{} autocompletion proposed.",str);
        if (beforeACBackToEndLine.trim().isEmpty()
            && str.startsWith(afterACUpToEndLine.trim().toUpperCase())){
            result.addElement(LookupElementBuilder.create(str).bold().withCaseSensitivity(false));
        }
    }

    private void macroOrCommandLineCompletion(String fileContent, PsiElement position, CompletionResultSet result) throws EngineLinkException {
        ASTNode currPos = position.getNode();
        String currPosType = currPos.getElementType().toString();
        PsiElement currPosParent = currPos.getPsi().getParent();
        String currPosParentType = currPosParent.getNode().getElementType().toString();

        treatElementByItsType(fileContent, position, result, currPosType, currPosParent, currPosParentType);
    }

    private void treatElementByItsType(String fileContent, PsiElement position, CompletionResultSet result, String currPosType, PsiElement currPosParent, String currPosParentType) throws EngineLinkException {
        switch (currPosType) {
            case "SquashTestTokenType.METADATA_KEY":
            case "SquashTestTokenType.AC_POSITION":
            case "SquashTestTokenType.CMD_KEY" :
                acPositionAC(fileContent, position, result, currPosParentType);
                break;
            case "SquashTestTokenType.CRLF":
            case "WHITE_SPACE":
                dslInstructionLineCompletionHelper.commandLineKeyCompletion(result, position, fileContent);
                break;
            case "SquashTestTokenType.VALUE":
                valueAC(fileContent, position, result, currPosParentType);
                break;
            case "SquashTestTokenType.CONVERTER":
                converterAC(result);
                break;
            case "SquashTestTokenType.EXECUTE_CMD":
                executeAC(result);
                break;
            case "SquashTestTokenType.ASSERTION_VALIDATOR":
                assertionAC(result);
                break;
            case "SquashTestTokenType.MACRO_LINE_CONTENT":
                macroLineContentAC(result, currPosParent);
                break;
            default:
                break;
        }
    }

    private void assertionAC(CompletionResultSet result) throws EngineLinkException {
        LOGGER.debug("Starting Squash Command line Command autocompletion");
        dslInstructionLineCompletionHelper.cmdLineAssertionCompletion(result);
    }

    private void executeAC(CompletionResultSet result) throws EngineLinkException {
        LOGGER.debug("Starting Squash Command line Command autocompletion");
        dslInstructionLineCompletionHelper.cmdLineExecuteCommandCompletion(result);
    }

    private void converterAC(CompletionResultSet result) throws EngineLinkException {
        LOGGER.debug("Starting Squash Command line Converter autocompletion");
        dslInstructionLineCompletionHelper.converterInstructionLineCompletion(result);
    }

    private void valueAC(String fileContent, PsiElement position, CompletionResultSet result, String currPosParentType) {
        if ("CMD_HEAD_PROPERTY_VALUE".equals(currPosParentType)) {
            LOGGER.debug("Squash Command line Head Property Value autocompletion");
            dslInstructionLineCompletionHelper.cmdLineHeadPropValueCompletion(result, position, fileContent);
        }

        if ("CMD_PROPERTY_VALUE".equals(currPosParentType) || "VALUE_ELEMENT".equals(currPosParentType)) {
            LOGGER.debug("Squash Command line ONE template autocompletion");
            dslInstructionLineCompletionHelper.cmdLinePropValueCompletion(result, position, fileContent);
        }
    }

    private void acPositionAC(String fileContent, PsiElement position, CompletionResultSet result, String currPosParentType) {
        if ("SECTION_LINE".equals(currPosParentType) || "CMD_HEAD_PROPERTY_KEY".equals(currPosParentType)) {
            String lineContentBeforeAC = beforeACBackToEndLine.trim();
            String lineContentAfterAC = afterACUpToEndLine.trim();
            if ("#".equals(lineContentAfterAC)) {
                LOGGER.debug("Insert a space after the # symbol");
                result.addElement(LookupElementBuilder.create(" "));
            } else if (lineContentAfterAC.isEmpty()) {
                if (lineContentBeforeAC.isEmpty()){
                    LOGGER.debug("Squash instruction line ALL templates autocompletion");
                    dslInstructionLineCompletionHelper.allCommandLineTemplatesCompletion(result);
                }
            } else {
                LOGGER.debug("Squash instruction line ONE template autocompletion");
                dslInstructionLineCompletionHelper.proposeInstructionHeadKeyWithItTemplate(lineContentAfterAC, result);
            }
        } else if ("CMD_PROPERTY_KEY".equals(currPosParentType)) {
            LOGGER.debug("Squash instruction line key word autocompletion");
            dslInstructionLineCompletionHelper.commandLineKeyCompletion(result, position, fileContent);
        }
    }
}
