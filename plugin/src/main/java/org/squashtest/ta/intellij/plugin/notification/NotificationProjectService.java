/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.notification;

import com.intellij.notification.NotificationType;

/**
 * Interface for the notification system.
 */
public interface NotificationProjectService{
    /**
     * Emits a notification that is related to an IDE project.
     * @param type roughly, message criticity.
     * @param title notification title (gets seen first)
     * @param subtitle subtitle that gets seen when the notification pops.
     * @param content the notification contents, more details that are displayed when the user opens the event or the event log.
     */
    void notifyProject(NotificationType type, String title, String subtitle, String content);

    /**
     * Emits an application level notification (not for a specific project).
     * @param type roughly, message criticity.
     * @param title notification title (gets seen first)
     * @param subtitle subtitle that gets seen when the notification pops.
     * @param content the notification contents, more details that are displayed when the user opens the event or the event log.
     */
    void notifyGlobal(NotificationType type, String title, String subtitle, String content);
}
