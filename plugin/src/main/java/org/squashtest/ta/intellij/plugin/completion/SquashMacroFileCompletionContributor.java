/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.completion;


import com.intellij.codeInsight.completion.CompletionContributor;
import com.intellij.codeInsight.completion.CompletionParameters;
import com.intellij.codeInsight.completion.CompletionProvider;
import com.intellij.codeInsight.completion.CompletionType;
import com.intellij.codeInsight.completion.CompletionResultSet;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.psi.PsiElement;
import com.intellij.util.ProcessingContext;
import org.jetbrains.annotations.NotNull;
import org.squashtest.ta.intellij.plugin.macro.general.SquashMacroLanguage;
import org.squashtest.ta.intellij.plugin.ProjectAwareRegistry;

import static com.intellij.patterns.PlatformPatterns.psiElement;

public class SquashMacroFileCompletionContributor extends CompletionContributor {

    private final ProjectAwareRegistry registry;

    public SquashMacroFileCompletionContributor(){
        this.registry=ServiceManager.getService(ProjectAwareRegistry.class);
        extend(CompletionType.BASIC,
                psiElement().withLanguage(SquashMacroLanguage.TA_MACRO_INSTANCE),
                new CompletionProvider<CompletionParameters>() {
                    @Override
                    public void addCompletions(@NotNull CompletionParameters parameters,
                                               @NotNull ProcessingContext context,
                                               @NotNull CompletionResultSet resultSet) {
                        addCompletion(parameters, resultSet);
                    }
                });
    }

    private void addCompletion(CompletionParameters parameters, CompletionResultSet result) {
        //the current cursor position - does bear project context
        PsiElement position = parameters.getPosition();
        registry.getService(SquashMacroFileCompletionProjectService.class,position).addCompletion(parameters,result);
    }
}
