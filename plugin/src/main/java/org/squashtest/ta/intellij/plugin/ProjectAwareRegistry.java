/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin;

import com.intellij.notification.Notification;
import com.intellij.notification.NotificationType;
import com.intellij.notification.Notifications;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiElement;
import org.jetbrains.annotations.NotNull;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.galaxia.metaexecution.enginelink.EngineLinkException;
import org.squashtest.ta.intellij.plugin.notification.NotificationProjectService;
import org.squashtest.ta.intellij.plugin.projectmodel.InvalidProjectException;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * This class allows application level components/services method calls to boot into a project context.
 */
public class ProjectAwareRegistry {

    public ProjectAwareRegistry(){
        LoggerFactory.getLogger(ProjectAwareRegistry.class).info("Squash TF Plugin component registry created.");
    }

    /**
     * Get project level components explicitely. <strong>Please keep this to the smallest possible number,</strong>
     * to keep service code clean - and testable.
     *
     * @param serviceInterface
     * @param element
     * @param <CType>
     * @return
     */
    public <CType> CType getService(@NotNull Class<CType> serviceInterface, PsiElement element){
        final Project project = element.getProject();
        try {
            CType service = ServiceManager.getService(project, serviceInterface);
            if(service==null){
                throw new IllegalArgumentException("Not a registered service interface "+serviceInterface.getName());
            }
            return service;
        }catch(Exception e){
            notifyException(project, e);
            //NOSONAR : we should be logging this ite as an exception, here, but IntelliJ will throw a tantrum if we do, so let's not...
            LoggerFactory.getLogger(ProjectAwareRegistry.class).debug("Error intercepted while building service.\n"+e);
            return buildNoopProxy(serviceInterface);
        }
    }

    @NotNull
    private <CType> CType buildNoopProxy(Class<CType> componentClass) {
        return (CType) Proxy.newProxyInstance(getClass().getClassLoader(), new Class[]{componentClass}, new InvocationHandler() {
            @Override
            public Object invoke(Object o, Method method, Object[] objects) throws Throwable {
                if(method.getReturnType().equals(Void.TYPE)) {
                    return null;
                }else if(Number.class.isAssignableFrom(method.getReturnType())){
                    return 0;
                }else{
                    throw new RuntimeException("Unsupported service method return type for wrong project type placeholder."+method.toGenericString());
                }
            }
        });
    }

    private void notifyException(Project project, Exception ite) {
        NotificationProjectService notificationService=ServiceManager.getService(project,NotificationProjectService.class);
        Throwable cause = ite.getCause();
        if(cause==null){
            notificationService.notifyProject(NotificationType.ERROR,"Error in Squash DSL service initialization.",ite.getMessage(), "No more details.");
        }else{
            try{
                throw cause;
            }catch(EngineLinkException e) {
                String content;
                if(e.getCause() == e || e.getCause()==null){
                    content="No more details.";
                }else{
                    content = e.getCause().getMessage();
                }
                notificationService.notifyProject(NotificationType.WARNING,"Not a valid Squash DSL project.", e.getMessage(), content);
            }catch(Error e){
                throw e;
            }catch(Throwable t){
                notificationService.notifyProject(NotificationType.ERROR,"Error in Squash DSL service initialization.",cause.getMessage(), cause.getCause() != cause ? cause.getCause().getMessage() : "No more details.");
            }
        }
    }

}
