/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.projectmodel;

import com.intellij.openapi.project.Project;
import org.jetbrains.annotations.NotNull;
import org.squashtest.ta.intellij.plugin.language.SquashTestFile;
import org.squashtest.ta.intellij.plugin.language.SquashTestFileType;

public class SquashTestFileLocationService extends SquashVirtualFileLocationService<SquashTestFileType, SquashTestFile> {

    public SquashTestFileLocationService(Project p){

        super(p,SquashTestFileType.INSTANCE,SquashTestFile.class);
    }

    private static final String TA_TEST_FOLDER = "/src/squashTA/tests";

    /**
     * This method is to check if the working TA test file is in the project 'tests' folder
     * @param currentFilePath the current TA test file path
     * @return true if the current TA test file path is as expected
     */
    public boolean isInTestsFolder(String currentFilePath) {
        return isInExpectedFolder(currentFilePath);
    }

    @NotNull
    @Override
    protected String expectedFolder() {
        return TA_TEST_FOLDER;
    }

    @Override
    protected boolean allowSubfolders() {
        return true;
    }
}
