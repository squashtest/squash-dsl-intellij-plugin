/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.validation;

import com.intellij.codeInspection.ProblemHighlightType;
import com.intellij.lang.ASTNode;
import com.intellij.lang.annotation.AnnotationHolder;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLComponentRegistry;
import org.squashtest.ta.intellij.plugin.fwconnector.IdeaFrameworkConnector;
import org.squashtest.ta.intellij.plugin.highlight.MacroHighlightsHelper;
import org.squashtest.ta.intellij.plugin.macro.psi.SquashMacroCmdHeadProperty;
import org.squashtest.ta.intellij.plugin.macro.psi.SquashMacroCmdHeadPropertyValue;
import org.squashtest.ta.intellij.plugin.macro.psi.SquashMacroCmdProperty;
import org.squashtest.ta.intellij.plugin.macro.psi.SquashMacroCmdPropertyKey;
import org.squashtest.ta.intellij.plugin.macro.psi.SquashMacroCmdPropertyValue;
import org.squashtest.ta.intellij.plugin.macro.psi.SquashMacroCommandLine;
import org.squashtest.ta.intellij.plugin.macro.psi.SquashMacroMacroTitleContent;
import org.squashtest.ta.intellij.plugin.macro.psi.SquashMacroUsingClause;
import org.squashtest.ta.intellij.plugin.macro.psi.SquashMacroValueElement;
import org.squashtest.ta.intellij.plugin.macro.psi.SquashMacroValueList;
import org.squashtest.ta.intellij.plugin.notification.NotificationProjectService;
import org.squashtest.ta.intellij.plugin.projectmodel.SquashMacroFileLocationProjectService;
import org.squashtest.ta.intellij.plugin.projectmodel.SquashMacroTitleControlResult;

import java.util.List;

public class SquashMacroAnnotationProjectServiceImpl extends BaseAnnotationProjectServiceImpl implements SquashMacroAnnotationProjectService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SquashMacroAnnotationProjectServiceImpl.class);
    
    private final MacroCallValidationHelper macroCallValidationHelper;
    private final MacroHighlightsHelper macroHighlightsHelper;
    private final MacroDefinitionValidationHelper macroDefinitionValidationHelper;
    private final SquashMacroFileLocationProjectService macroFileLocator;

    private final DSLInstructionLineValidationHelper dslInstructionLineValidationHelper = new DSLInstructionLineValidationHelper();

    public SquashMacroAnnotationProjectServiceImpl(
            NotificationProjectService notifier,
            SquashMacroFileLocationProjectService macroFileLocator,
            MacroCallValidationHelper macroCallValidationHelper,
            MacroDefinitionValidationHelper macroDefinitionValidationHelper,
            MacroHighlightsHelper macroHighlightsHelper,
            IdeaFrameworkConnector connector

    ) {
        super(notifier,connector);
        this.macroFileLocator = macroFileLocator;
        this.macroCallValidationHelper=macroCallValidationHelper;
        this.macroDefinitionValidationHelper= macroDefinitionValidationHelper;
        this.macroHighlightsHelper= macroHighlightsHelper;
    }

    @Override
    protected void performAnnotation(@NotNull PsiElement element, @NotNull AnnotationHolder holder, SquashDSLComponentRegistry macroJsonFileContent) {
        PsiFile taMacroFile = element.getContainingFile();
        String elementType =  element.getNode().getElementType().toString();

        switch (elementType){
            case "MACRO_TITLE_CONTENT":
                //check the macro signature
                macroTitleControl(holder, element, macroJsonFileContent);
                break;
            case "SquashMacroTokenType.MACRO_LINE_CONTENT":
                //macro line control
                macroLineControl(holder, element, macroJsonFileContent);
                break;
            case "COMMAND_LINE":
                //command line control
                commandLineControl(holder, element, macroJsonFileContent);
                break;
            case "FILE":
                //check if this current file is located in 'shortcuts' folder or one of its subfolders
                macroFileControl(holder, taMacroFile);
                break;
            default:
                break;
        }
    }

    private void macroFileControl(@NotNull AnnotationHolder holder, PsiFile taMacroFile) {
       if (!macroFileLocator.isInShortCutsFolder(taMacroFile.getVirtualFile().getPath())) {
                        LOGGER.warn("The Squash Macro File {} is NOT in the 'project/src/squashTA/shortcuts' folder or one of its subfolders !",taMacroFile.getVirtualFile().getPath());
                        holder.createErrorAnnotation(taMacroFile,
                                "This Squash Macro File should be placed in the 'project/src/squashTA/shortcuts' folder or one of its subfolders.");
       }
    }

    private void commandLineControl(@NotNull AnnotationHolder holder, PsiElement cmdLine, SquashDSLComponentRegistry macroJsonFileContent)   {
        SquashMacroCommandLine commandLine = (SquashMacroCommandLine) cmdLine;
        if (commandLine != null) {
            //check instruction template syntax
            validateInstructionTemplate(holder, commandLine);

            //check values
            validateValueOfAllKeys(holder, commandLine, macroJsonFileContent);

            //check USING value list
            validateUsingValueList(holder, commandLine);
        }
    }

    private void validateUsingValueList(AnnotationHolder holder, SquashMacroCommandLine commandLine) {
        List<SquashMacroUsingClause> squashMacroUsingClauses = commandLine.getUsingClauseList();
        for (SquashMacroUsingClause usingClause : squashMacroUsingClauses) {
            String usingKey = usingClause.getUsingKey().getUsing();
            if (usingKey != null) {
                SquashMacroValueList squashMacroValueList = usingClause.getValueList();
                if (squashMacroValueList != null) {
                    ASTNode valueListNode = squashMacroValueList.getNode();
                    dslInstructionLineValidationHelper.controlEmptyValue(holder, valueListNode);

                    List<SquashMacroValueElement> squashMacroUsingValueElements = squashMacroValueList.getValueElementList();
                    controlUsingValues(holder, squashMacroUsingValueElements);
                }
            }
        }

    }

    private void controlUsingValues(AnnotationHolder holder, List<SquashMacroValueElement> squashMacroUsingValueElements) {
        for (SquashMacroValueElement squashMacroUsingValueElement : squashMacroUsingValueElements) {
            ASTNode eleNode = squashMacroUsingValueElement.getFirstChild().getNode();
            String eleNodeType = eleNode.getElementType().toString();
            if ("SquashMacroTokenType.AC_POSITION".equals(eleNodeType)){
                holder.createErrorAnnotation(eleNode, "Value must contain ONLY '.', '-', '_', '/', \\ or digital characters.");
            }
        }
    }

    private void validateValueOfAllKeys(AnnotationHolder holder, SquashMacroCommandLine commandLine, SquashDSLComponentRegistry macroJsonFileContent) {
        //check in head clause
        SquashMacroCmdHeadProperty cmdHeadProperty = commandLine.getCmdHeadProperty();
        String headKey = cmdHeadProperty.getCmdHeadPropertyKey().getCMDHeadKey();
        if (headKey != null) {
            SquashMacroCmdHeadPropertyValue headValue = cmdHeadProperty.getCmdHeadPropertyValue();
            if (headValue != null) {
                ASTNode headValueNode = headValue.getNode();
                //check in this instruction if there are values of type empty "{}" or "{{}}"
                String newHeadValue = headValueNode.getText().trim();
                if ("{}".equals(newHeadValue) || "{{}}".equals(newHeadValue)) {
                    holder.createWeakWarningAnnotation(headValueNode, "Value inside curly brackets must NOT be empty.");
                }
                String undefinedBuiltInValue = headValue.getUndefinedElement();
                dslInstructionLineValidationHelper.validateValueOfOneKey(holder, headValueNode, headKey, undefinedBuiltInValue, macroJsonFileContent);
            }
        }

        //check in other clauses
        List<SquashMacroCmdProperty> macroCmdPropertyList = commandLine.getCmdPropertyList();
        for (SquashMacroCmdProperty property : macroCmdPropertyList) {
            SquashMacroCmdPropertyKey cmdPropertyKey = property.getCmdPropertyKey();
            String key = cmdPropertyKey.getCMDKey();
            if (key != null){
                validateFoundKey(property, holder, key, macroJsonFileContent);
            }
        }
    }

    private void validateFoundKey(SquashMacroCmdProperty property, AnnotationHolder holder, String key, SquashDSLComponentRegistry macroJsonFileContent) {
        SquashMacroCmdPropertyValue cmdPropertyValue = property.getCmdPropertyValue();
        if (cmdPropertyValue != null) {
            ASTNode cmdPropertyValueNode = cmdPropertyValue.getNode();
            //check in this instruction if there are values of type empty "{}" or "{{}}"
            String newValue = cmdPropertyValue.getText().trim();
            if ("{}".equals(newValue) || "{{}}".equals(newValue)) {
                holder.createWeakWarningAnnotation(cmdPropertyValueNode, "Value inside curly brackets must NOT be empty.");
            }
            String undefinedBuiltInValue = cmdPropertyValue.getUndefinedElement();
            dslInstructionLineValidationHelper.validateValueOfOneKey(holder, cmdPropertyValueNode, key, undefinedBuiltInValue, macroJsonFileContent);
        }
    }

    private void validateInstructionTemplate(AnnotationHolder holder, SquashMacroCommandLine commandLine) {
        List<String> instructionKeyList = dslInstructionLineValidationHelper.getKeyListFromInstructionTextInMacroFile(commandLine);
        if (dslInstructionLineValidationHelper.hasTemplateError(instructionKeyList)){
            ASTNode instructionLineNode = commandLine.getNode();
            dslInstructionLineValidationHelper.displayInstructionTemplateListInQuickFix(holder, instructionLineNode);
        }

    }



    private void macroLineControl(@NotNull AnnotationHolder holder, PsiElement macroLineContent, SquashDSLComponentRegistry macroJsonFileContent)  {
        String macroLineContentText = macroLineContent.getText();
        SquashMacroCallControlResult macroControlResult = macroCallValidationHelper.checkMacroLineContent(macroLineContentText, macroJsonFileContent);
        String message = (macroControlResult != null)? macroCallValidationHelper.getMacroLineMsg(macroControlResult): "";

        //macro color annotation
        switch (message){
            case "Macro line definition not found":
                holder.createErrorAnnotation(macroLineContent, "No match found with any existing Macro").setHighlightType(ProblemHighlightType.ERROR);
                break;
            case "Macro line multi-definition conflict":
                holder.createWarningAnnotation(macroLineContent, "Conflict : Multiple Macros match").setHighlightType(ProblemHighlightType.WEAK_WARNING);
                break;
            case "":
                break;
            default:
                macroHighlightsHelper.makeColorToNewValue(macroLineContent, message, holder, "macro");
                break;
        }
    }

    private void macroTitleControl(@NotNull AnnotationHolder holder, PsiElement ele, SquashDSLComponentRegistry macroJsonFileContent)  {
        SquashMacroMacroTitleContent macroTitleContent = (SquashMacroMacroTitleContent) ele;
        SquashMacroTitleControlResult macroTitleCheckResult = macroDefinitionValidationHelper.isMacroTitleExisted(macroTitleContent, macroJsonFileContent);
        if (macroTitleCheckResult.isInCustom() || macroTitleCheckResult.isInBuiltIn()) {
            //check if the Squash Macro title is already defined
            LOGGER.error("The signature of Squash Macro File ({}) is already used.",ele.getContainingFile().getVirtualFile().getPath());
            holder.createErrorAnnotation(macroTitleContent, "This Macro signature already exists");
        }
    }

}
