/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.completion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLMacroFixedPart;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLMacroParam;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLMacroSignatureVisitor;
import org.squashtest.ta.intellij.plugin.tools.LineSearchingResult;

/**
 * This visitor implements the process of checking that signature parts match a given primer string (the start of a macro call).
 * @author edegenetais
 */
public class PrimerMatcherSquashDSLMacroSignatureVisitor implements SquashDSLMacroSignatureVisitor<LineSearchingResult> {
    private static final Logger LOGGER = LoggerFactory.getLogger(PrimerMatcherSquashDSLMacroSignatureVisitor.class);

    private final String macroCallPrimer;
    private LineSearchingResult lastResult;

    PrimerMatcherSquashDSLMacroSignatureVisitor(String macroCallPrimer) {
        this.macroCallPrimer = macroCallPrimer;
    }

    public LineSearchingResult visit(SquashDSLMacroFixedPart fixed){

        if(lastResult==null){
            int lastResultEnd=0;
            tryMatch(fixed, macroCallPrimer, lastResultEnd);
        }else if(lastResult.wasFound() && lastResult.getEndIndex()< macroCallPrimer.length()){
            String toMatch= macroCallPrimer.substring(lastResult.getEndIndex());
            int lastResultEnd=lastResult.getEndIndex();
            tryMatch(fixed,toMatch,lastResultEnd);
        }else{
            lastResult=LineSearchingResult.LINE_SEARCH_MISS;
        }
        return lastResult;
    }

    private void tryMatch(SquashDSLMacroFixedPart fixed, String toMatch, int lastResultEnd) {
        LOGGER.debug("Trying to match {} against the {} remaining primer.",fixed.getContent(),toMatch);
        String fixedContent = fixed.getContent();
        int fixedContentLength = fixedContent.length();

        if(toMatch.length() > fixedContentLength && toMatch.startsWith(fixedContent)){
            lastResult=new LineSearchingResult.LineSearchHit(0,lastResultEnd+fixedContentLength, macroCallPrimer.substring(0,lastResultEnd+fixedContentLength));
        }else if(fixedContent.startsWith(toMatch) || fixedContent.toUpperCase().startsWith(toMatch)){
            lastResult=new LineSearchingResult.LineSearchHit(0, macroCallPrimer.length(), macroCallPrimer);
        }else{
            lastResult = LineSearchingResult.LINE_SEARCH_MISS;
        }
        LOGGER.debug("Result is {}",lastResult);
    }

    public LineSearchingResult visit(SquashDSLMacroParam parm){//we, in fact, don't care about the parm reference here, knowing that it is a parm is enough
        LOGGER.debug("Using {} parameter to match remaining primer part.",parm.getDefinition().getName());
        if(lastResult==null){
            lastResult = new LineSearchingResult.LineSearchHit(0, macroCallPrimer.length(), macroCallPrimer);
        }else if(lastResult.wasFound() && macroCallPrimer.charAt(lastResult.getEndIndex()-1) == ' ') {
            lastResult = new LineSearchingResult.LineSearchHit(0, macroCallPrimer.length(), macroCallPrimer);
        } else{
            lastResult = LineSearchingResult.LINE_SEARCH_MISS;
        }
        return lastResult;
    }
}