/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.tools;

import java.util.List;

/**
 * This class offers various collection operations.
 * TODO : this should be reviewed against JDK API, and also rewritten for clarity to simplify the calling code.
 */
public class CollectionToolkit {

    /**
     * This method is to compare if every string at a specific position of the two (size-equal) comparing lists is equal to each other
     * TODO : this compares ORDERED keysets for equality (ignoring case and initial/terminal whitespace). See if it does not already exists in JDK API.
     * @param actualKeySet the actual string list
     * @param expectedKeySet the expected string list
     * @return the size of two list if true and size+1 if false // TODO uh? seriously ? Couldn't it return the square root of my birthsday's numeric ordinal in the Maya calendar if false, and the weight of my left thumb if equals ?
     *                                                          // That would make thing WAY clearer on the callsite side }:-]
     */
    public static int compareKeySet(List<String> actualKeySet, List<String> expectedKeySet) {
        int keySetLength = actualKeySet.size();

        int k=0;
        while (k<keySetLength) {
            if (!StringToolkit.compareTwoTrimmedStringIgnoredCases(actualKeySet.get(k), expectedKeySet.get(k))) {
		k = keySetLength;
	    }
            ++k;
        }
        return k;
    }
}
