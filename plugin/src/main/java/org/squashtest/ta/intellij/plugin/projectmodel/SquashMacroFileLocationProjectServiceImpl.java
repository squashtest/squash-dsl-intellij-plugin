/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.projectmodel;

import com.intellij.openapi.project.Project;
import com.intellij.psi.util.PsiTreeUtil;
import org.jetbrains.annotations.NotNull;
import org.squashtest.ta.intellij.plugin.language.SquashMacroFile;
import org.squashtest.ta.intellij.plugin.language.SquashMacroFileType;
import org.squashtest.ta.intellij.plugin.macro.psi.SquashMacroMacroTitle;

import java.util.ArrayList;
import java.util.List;

public class SquashMacroFileLocationProjectServiceImpl extends SquashVirtualFileLocationService<SquashMacroFileType, SquashMacroFile> implements SquashMacroFileLocationProjectService {
    private static final String TA_SHORTCUT_FOLDER = "/src/squashTA/shortcuts";

    public SquashMacroFileLocationProjectServiceImpl(Project project) {
        super(project, SquashMacroFileType.INSTANCE, SquashMacroFile.class);
    }

    /**
     * This method is to get all non-empty-macro-titles in the project 'shortcuts' folder  or one of its subfolders
     * @return a InProjectMacroTitle list
     */
    @Override
    public List<SquashMacroMacroTitle> getMacroTitlesInProjectShortcutsFolder() {
       List<SquashMacroMacroTitle> result = new ArrayList<>();
        for (SquashMacroFile taMacro : getProjectSquashFiles()) {
            //take only macros located in "shortcuts" folder (NOT in its sub folder either)
            if (taMacro != null && isInShortCutsFolder(taMacro.getVirtualFile().getPath())) {
                SquashMacroMacroTitle macroTitle = PsiTreeUtil.getChildOfType(taMacro, SquashMacroMacroTitle.class);
                if (macroTitle != null) {
                    result.add(macroTitle);
                }
            }
        }
        return result;
    }

    /**
     * This method is to check if the working TA macro definition file is in the project 'src/squashTA/shortcuts' folder  or one of its subfolders
     * @param currentFilePath the current TA test file path
     * @return true if the current TA test file path is as expected
     */
    @Override
    public boolean isInShortCutsFolder(String currentFilePath) {
        return isInExpectedFolder(currentFilePath);
    }

    @Override
    @NotNull
    protected String expectedFolder() {
        return TA_SHORTCUT_FOLDER;
    }

    @Override
    protected boolean allowSubfolders() {
        return true;
    }
}
