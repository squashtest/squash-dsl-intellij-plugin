/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.highlight;

import com.intellij.lang.annotation.AnnotationHolder;
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.PsiElement;
import org.squashtest.ta.intellij.plugin.macro.psi.SquashMacroMacroTitle;
import org.squashtest.ta.intellij.plugin.macro.psi.SquashMacroMacroTitleContent;
import org.squashtest.ta.intellij.plugin.projectmodel.MacroDefinitionOperations;
import org.squashtest.ta.intellij.plugin.projectmodel.SquashMacroFileLocationProjectService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MacroHighlightsHelper {

    private final MacroDefinitionOperations macroDefinitionOperations = new MacroDefinitionOperations();
    private SquashMacroFileLocationProjectService macroFileLocationService;

    public MacroHighlightsHelper(SquashMacroFileLocationProjectService macroFileLocationSrv){
        this.macroFileLocationService=macroFileLocationSrv;
    }

    public <MACRO_LINE extends PsiElement> void makeColorToNewValue(MACRO_LINE macroLine, String message, AnnotationHolder holder, String extension) {
        int firstOffset = macroLine.getTextRange().getStartOffset();
        String upperCaseSentence = macroLine.getText().toUpperCase();

        String[] newValueArray = message.split("\n");
        for (String s: newValueArray){
            if (!"".equals(s)){
                String upperCaseWord = s.toUpperCase();
                int contentFirstOffset = firstOffset + upperCaseSentence.indexOf(upperCaseWord);
                int contentLastOffset = contentFirstOffset + upperCaseWord.length();
                TextRange range = new TextRange(contentFirstOffset,contentLastOffset);
                if ("ta".equals(extension)){
                    holder.createInfoAnnotation(range, "Macro Keyword").setTextAttributes(SquashTestSyntaxHighlighter.MACRO_KEY);
                } else if ("macro".equals(extension)){
                    holder.createInfoAnnotation(range, "Macro Keyword").setTextAttributes(SquashMacroSyntaxHighlighter.MACRO_KEY);
                }
            }
        }
    }

    /**
     * This method is to get a the macro signature in the project 'shortcuts' folder with a searching signature content
     * @param titleContent the searching signature content
     * @return the list of custom macro signatures that match the searching input
     */
    public List<SquashMacroMacroTitle> getCustomMacroTitlesByTitleContent(SquashMacroMacroTitleContent titleContent) {
        List<SquashMacroMacroTitle> result = new ArrayList<>();

        PsiElement currentFile = titleContent.getContainingFile();
        List<String> expectedKeySet = macroDefinitionOperations.getMacroKeysFromTitleContent(titleContent);
        Map<Integer, String> expectedParamSet = macroDefinitionOperations.getMacroParamsFromTitleContent(titleContent);

        List<SquashMacroMacroTitle> projectCustomMacroTitles = macroFileLocationService.getMacroTitlesInProjectShortcutsFolder() ;

        for (SquashMacroMacroTitle title : projectCustomMacroTitles) {
            //remove the current file from Project Macro Title list
            if (!title.getContainingFile().equals(currentFile)){
                SquashMacroMacroTitleContent actualTitleContent = title.getMacroTitleContent();
                List<String> keySet = macroDefinitionOperations.getMacroKeysFromTitleContent(actualTitleContent);
                Map<Integer, String> paramSet = macroDefinitionOperations.getMacroParamsFromTitleContent(actualTitleContent);
                macroDefinitionOperations.compareSets(result, expectedKeySet, expectedParamSet, title, keySet, paramSet);
            }
        }
        return result;
    }
}
