/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.highlight;

import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighter;
import com.intellij.openapi.options.colors.AttributesDescriptor;
import com.intellij.openapi.options.colors.ColorDescriptor;
import com.intellij.openapi.options.colors.ColorSettingsPage;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.squashtest.ta.intellij.plugin.language.SquashMacroIcons;

import javax.swing.Icon;
import java.util.Map;

public class SquashMacroColorSettingsPage implements ColorSettingsPage {
    private static final AttributesDescriptor[] DESCRIPTORS = new AttributesDescriptor[]{
            new AttributesDescriptor("Macro Symbol", SquashMacroSyntaxHighlighter.MACRO_SYMBOL),
            new AttributesDescriptor("Instruction Key", SquashMacroSyntaxHighlighter.CMD_KEY),
            new AttributesDescriptor("Instruction Built-in Value", SquashMacroSyntaxHighlighter.BUILT_IN),
            new AttributesDescriptor("Instruction Value", SquashMacroSyntaxHighlighter.VALUE),
            new AttributesDescriptor("Instruction Inline Declared Value", SquashMacroSyntaxHighlighter.INLINE_VALUE),
            new AttributesDescriptor("Macro Signature Key", SquashMacroSyntaxHighlighter.MACRO_SIGNATURE_KEY),
            new AttributesDescriptor("Macro Signature Param", SquashMacroSyntaxHighlighter.MACRO_SIGNATURE_PARAM),
            new AttributesDescriptor("Macro Line", SquashMacroSyntaxHighlighter.MACRO_MACRO_LINE),
            new AttributesDescriptor("Macro Key", SquashMacroSyntaxHighlighter.MACRO_KEY),
            new AttributesDescriptor("Comment Line", SquashMacroSyntaxHighlighter.COMMENT)
    };

    @Nullable
    @Override
    public Icon getIcon() {
        return SquashMacroIcons.FILE;
    }

    @NotNull
    @Override
    public SyntaxHighlighter getHighlighter() {
        return new SquashMacroSyntaxHighlighter();
    }

    @NotNull
    @Override
    public String getDemoText() {
        return  "# MACRO_KEY1 {param1} MACRO_KEY2 {param_2} MACRO_KEY3 {param_3} MACRO_KEY4 {param_4}\n" +
                "=>\n" +
                "\n" +
                "DEFINE $(raw_data) AS expectedResult\n" +
                "LOAD {param1} FROM {param2} AS nameInTheContext\n"+
                "\n" +
                "CONVERT nameInTheContext TO xml(xslt) AS convertedResource\n"+
                "EXECUTE execute WITH convertedResource ON {param_3} USING {param4} AS result\n" +
                "\n" +
                "//This is another comment line.\n" +
                "VERIFY result IS similaire WITH expectedResult\n" +
                "# ASSERT XML {result} IS $(VALID) USING SCHEMA {{xsd_path}}\n";
    }

    @Nullable
    @Override
    public Map<String, TextAttributesKey> getAdditionalHighlightingTagToDescriptorMap() {
        return null;
    }

    @NotNull
    @Override
    public AttributesDescriptor[] getAttributeDescriptors() {
        return DESCRIPTORS;
    }

    @NotNull
    @Override
    public ColorDescriptor[] getColorDescriptors() {
        return  ColorDescriptor.EMPTY_ARRAY;
    }

    @NotNull
    @Override
    public String getDisplayName() {
        return "Squash Macro File";
    }
}
