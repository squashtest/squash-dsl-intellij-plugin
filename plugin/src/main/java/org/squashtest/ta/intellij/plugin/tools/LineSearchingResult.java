/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.tools;

/**
 * This class defines one found location of a given target pattern in a larger string.
 */
public abstract class LineSearchingResult {

    /** @return the occurrence start index if found. Throws {@link UnsupportedOperationException} if not found. */
    public abstract int getStartIndex();

    /** @return the occurrence end index if found. Throws {@link UnsupportedOperationException} if not found. */
    public abstract int getEndIndex();

    /**
     * @return the effective matched content if found. Throws {@link UnsupportedOperationException} if not found.
     */
    public abstract String getContent();

    /**
     * @return <code>true</code> for a match, <code>false</code> for a search miss.
     */
    public abstract boolean wasFound();

    /**
     * @return <code>true</code> for a search miss, <code>false</code> for a match.
     */
    public boolean notFound() {
        return !wasFound();
    }

    /** <code>true</code> if this occurrence is after the given position */
    public abstract boolean after(int position);
    /** <code>true</code> if this occurrence is before the given position */
    public abstract boolean before(int position);

    public abstract boolean startsBefore(int position);

    public static final LineSearchingResult LINE_SEARCH_MISS = new LineSearchingResult(){
        @Override
        public boolean wasFound() {
            return false;
        }

        @Override
        public int getStartIndex() {
            throw new UnsupportedOperationException("Search misses have no startIndex");
        }

        @Override
        public int getEndIndex() {
            throw new UnsupportedOperationException("Search misses have no endIndex");
        }

        @Override
        public String getContent() {
            throw new UnsupportedOperationException("Search misses have no hit content.");
        }

        /** A search miss has no position, so it is never after any given position (makes no sense). */
        @Override
        public boolean after(int position) {
            return false;
        }
        /** A search miss has no position, so it is never before any given position (makes no sense). */
        @Override
        public boolean before(int position) {
            return false;
        }

        @Override
        public boolean startsBefore(int position) {return false;}
    };

    public static class LineSearchHit extends LineSearchingResult{

        private int startIndex;
        private int endIndex;
        private String content;

        public LineSearchHit(int startIndex, int endIndex, String content) {
            this.startIndex = startIndex;
            this.endIndex = endIndex;
            this.content = content;
        }

        /** Where the occurrence begins in the original string. */
        @Override
        public int getStartIndex() {
            return startIndex;
        }

        /** Where the occurrence ended in the original string */
        @Override
        public int getEndIndex() {
            return endIndex;
        }

        /** Exact value of the occurrence (as it may have been defined as a regex or other variable search target definition) */
        @Override
        public String getContent() {
            return content;
        }

        @Override
        public boolean wasFound() {
            return true;
        }

        @Override
        public boolean after(int position) {
            return position <= startIndex;
        }

        @Override
        public boolean before(int position) {
            return position >= endIndex;
        }

        @Override
        public boolean startsBefore(int position) {
            return position>=startIndex;
        }
    }
}
