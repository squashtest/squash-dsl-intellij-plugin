/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.completion;

public class SquashMacroFileContentControl {
    private static final String SYMBOL = "_SYMBOL";
    private static final String SEPARATOR = "_SEPARATOR";

    private int macroSymbolIndex;
    private int separatorIndex;

    public SquashMacroFileContentControl(int macroSymbolIndex, int separatorIndex) {
        this.macroSymbolIndex = macroSymbolIndex;
        this.separatorIndex = separatorIndex;
    }

    public String checkContent() {
        int symbolIndex = this.macroSymbolIndex;
        int sepIndex = this.separatorIndex;

        StringBuilder result = new StringBuilder("EMPTY");
        if (symbolIndex!=-1) {
            result.append(SYMBOL);
        }
        if (sepIndex!=-1 && sepIndex<symbolIndex) {
            result.append(SEPARATOR);
        }
        return result.toString();
    }
}
