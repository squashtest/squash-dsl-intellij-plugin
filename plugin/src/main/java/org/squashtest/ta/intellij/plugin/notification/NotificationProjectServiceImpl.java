/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.notification;

import com.intellij.notification.Notification;
import com.intellij.notification.NotificationType;
import com.intellij.notification.Notifications;
import com.intellij.openapi.project.Project;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementation of the notification service.
 */
public class NotificationProjectServiceImpl implements NotificationProjectService {
    private static final String SQUASH_TF_GROUP_DISPLAY_ID = "Squash TF plugin";
    public static final int NOTIFICATION_MINIMAL_PERIOD_MS = 5000;
    public static final Logger LOGGER = LoggerFactory.getLogger(NotificationProjectServiceImpl.class);

    private Project project;
    private NotificationThrottle t=new NotificationThrottle(NOTIFICATION_MINIMAL_PERIOD_MS);

    /**
     * Full initialization constructor.
     * @param project
     */
    public NotificationProjectServiceImpl(Project project){
        this.project = project;
    }

    @Override
    public void notifyGlobal(NotificationType type, String title, String subtitle, String content) {
        String notificationData=type.name()+"|"+title+"|"+subtitle+"|"+content;

        if(t.isInThrottlingPeriod(notificationData)) {
            LOGGER.debug("Message {} gets throttled. Already notified in the last "+NOTIFICATION_MINIMAL_PERIOD_MS+" ms",notificationData);
        }else{
            LOGGER.debug("Notifying message {}.",notificationData);
            Notification notification = createNotification(type, title, subtitle, content);
            Notifications.Bus.notify(notification);
        }
    }

    /**
     * Factory method for notification objects.
     * @param type roughly, message criticity.
     * @param title notification title (gets seen first).
     * @param subtitle subtitle, is shown when the notification, pops.
     * @param content detailed contents, seen when the user opens the event (or looks at the event log).
     * @return notification instance with the requested properties.
     */
    @NotNull
    protected Notification createNotification(NotificationType type, String title, String subtitle, String content) {
        return new Notification(SQUASH_TF_GROUP_DISPLAY_ID, null,title,subtitle,content,type, null);
    }
    @Override
    public void notifyProject(NotificationType type, String title, String subtitle, String content) {
        String notificationData=type.name()+"|"+title+"|"+subtitle+"|"+content+project.getBasePath();
        if(t.isInThrottlingPeriod(notificationData)) {
            LOGGER.debug("Message {} gets throttled. Already notified in the last "+NOTIFICATION_MINIMAL_PERIOD_MS+" ms",notificationData);
        }else {
            LOGGER.debug("Notifying message {}.", notificationData);
            Notification notification = createNotification(type, title, subtitle, content);
            Notifications.Bus.notify(notification, project);
        }
    }

}
