/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.validation;

import org.squashtest.ta.galaxia.enginelink.components.SquashDSLMacro;
import org.squashtest.ta.intellij.plugin.macro.psi.SquashMacroMacroTitle;

import java.util.List;

public class SquashMacroCallControlResult {
    private boolean emptyMacroLine;
    private int matchNumber;
    private List<SquashMacroMacroTitle> customMacroTitles;
    private SquashDSLMacro latestMacroLineTemplate;
    private float bestMatchSpecificity;

    public boolean isEmptyMacroLine() {
        return emptyMacroLine;
    }

    public void setEmptyMacroLine(boolean emptyMacroLine) {
        this.emptyMacroLine = emptyMacroLine;
    }

    public int getMatchNumber() {
        return matchNumber;
    }

    public void setMatchNumber(int matchNumber) {
        this.matchNumber = matchNumber;
    }

    public List<SquashMacroMacroTitle> getCustomMacroTitles() {
        return customMacroTitles;
    }

    public void setCustomMacroTitles(List<SquashMacroMacroTitle> customMacroTitles) {
        this.customMacroTitles = customMacroTitles;
    }

    public SquashDSLMacro getLatestMacroLineTemplate() {
        return latestMacroLineTemplate;
    }

    public void setLatestMacroLineTemplate(SquashDSLMacro latestMacroLineTemplate) {
        this.latestMacroLineTemplate = latestMacroLineTemplate;
    }

    public float getBestMatchSpecificity() {
        return bestMatchSpecificity;
    }

    public void setBestMatchSpecificity(float bestMatchSpecificity) {
        this.bestMatchSpecificity = bestMatchSpecificity;
    }

    @Override
    public String toString() {
        return "SquashMacroCallControlResult{" +
                "emptyMacroLine=" + emptyMacroLine +
                ", matchNumber=" + matchNumber +
                ", customMacroTitles=" + customMacroTitles +
                ", latestMacroLineTemplate=" + latestMacroLineTemplate +
                ", bestMatchSpecificity=" + bestMatchSpecificity +
                '}';
    }
}
