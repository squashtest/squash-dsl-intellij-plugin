/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.validation;

import com.intellij.lang.annotation.AnnotationHolder;
import com.intellij.lang.annotation.AnnotationSession;
import com.intellij.notification.NotificationType;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.galaxia.metaexecution.enginelink.EngineLinkException;
import org.squashtest.ta.intellij.plugin.notification.NotificationProjectService;

import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.TimeoutException;

import org.squashtest.ta.galaxia.enginelink.components.SquashDSLComponentRegistry;
import org.squashtest.ta.intellij.plugin.fwconnector.IdeaFrameworkConnector;

public abstract class BaseAnnotationProjectServiceImpl {

    private static final Logger LOGGER= LoggerFactory.getLogger(BaseAnnotationProjectServiceImpl.class);

    //TODO : this must have been defined a couple of times here and there. We might want to set up something to share these messages among user objects. Did you say message bundles ?
    private static final String JSON_LOADING_MSG = "Retrieving of Squash component registry (JSON file) is in progress. Please wait until it is completely loaded!";

    private NotificationProjectService notifier;
    private Map<AnnotationSession,Boolean> errorMap=new WeakHashMap<>();
    private Map<AnnotationSession,Boolean> loadingMap=new WeakHashMap<>();
    protected final IdeaFrameworkConnector connector;

    public BaseAnnotationProjectServiceImpl(NotificationProjectService notifier,IdeaFrameworkConnector connector) {
        this.notifier = notifier;
        this.connector = connector;
    }

    public void annotate(@NotNull final PsiElement element, @NotNull AnnotationHolder holder) {

        PsiFile taTestFile = element.getContainingFile();

        //get all built-in content
        try {

            SquashDSLComponentRegistry macroJsonFileContent = connector.getSquashDSLComponentRegistry();

            performAnnotation(element, holder, macroJsonFileContent);

        } catch (EngineLinkException e) {
            notifier.notifyProject(NotificationType.ERROR, "Failed to load SKF component data.", "", e.getMessage());
            AnnotationSession session = holder.getCurrentAnnotationSession();
            if (this.errorMap.getOrDefault(session, false)) {
                LOGGER.debug("Omitting component data extraction error annotation for file {} because it has already been annotated.", taTestFile.getVirtualFile().getPath());
            } else {
                this.errorMap.put(session, true);
                LOGGER.debug("Annotating file for limited DSL support due to failed registry retrieval.");
                holder.createWarningAnnotation(taTestFile, "We failed to retrieve the component registry handle : no validation or completion for component names and macro calls.");
            }
            LOGGER.error(e +"ANNOTATION for {} : We failed to retrieve the component registry handle.", taTestFile.getVirtualFile().getPath());
        } catch(TimeoutException e){
            notifier.notifyProject(NotificationType.INFORMATION, "Loading component registry data.", "Data extraction ongoing.", "The Squash TF plugin is still busy extracting component registry data for your project, please wait untill the registry is ready. When registry availability is notified, you may close and reopen already annotated files to get accurate validation and autocompletion support.");
            AnnotationSession session = holder.getCurrentAnnotationSession();
            if (this.loadingMap.getOrDefault(session, false)) {
                LOGGER.debug("Omitting component data extraction error annotation for file {} because it has already been annotated.", taTestFile.getVirtualFile().getPath());
            } else {
                this.loadingMap.put(session, true);
                LOGGER.debug("Annotating file for limited DSL support due to failed registry retrieval.");
                holder.createWarningAnnotation(taTestFile, JSON_LOADING_MSG);
            }
        }
    }

    /**
     * Concrete service implementations define their own element annotation logioc by implementing this method.
     * @param element the element to annotate.
     * @param holder the current annotation holder.
     * @param macroJsonFileContent the current component registry instance to get component information from.
     */
    protected abstract void performAnnotation(@NotNull PsiElement element, @NotNull AnnotationHolder holder, SquashDSLComponentRegistry macroJsonFileContent);
}
