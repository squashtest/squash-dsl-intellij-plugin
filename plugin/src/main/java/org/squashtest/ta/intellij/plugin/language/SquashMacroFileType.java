/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.language;

import com.intellij.openapi.fileTypes.LanguageFileType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.squashtest.ta.intellij.plugin.macro.general.SquashMacroLanguage;

import javax.swing.Icon;

public class SquashMacroFileType extends LanguageFileType {

    public static final SquashMacroFileType INSTANCE = new SquashMacroFileType();

    private SquashMacroFileType() {
        super(SquashMacroLanguage.TA_MACRO_INSTANCE);
    }

    @NotNull
    @Override
    public String getName() {
        return "Squash Macro File";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Squash TF macro file";
    }

    @NotNull
    @Override
    public String getDefaultExtension() {
        return "macro";
    }

    @Nullable
    @Override
    public Icon getIcon() {
        return SquashMacroIcons.FILE;
    }
}
