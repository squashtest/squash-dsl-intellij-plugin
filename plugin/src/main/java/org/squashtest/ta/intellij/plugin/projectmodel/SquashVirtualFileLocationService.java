/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.projectmodel;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiManager;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.util.indexing.FileBasedIndex;
import com.intellij.util.indexing.ID;
import org.jetbrains.annotations.NotNull;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

abstract class SquashVirtualFileLocationService<FileType, File extends PsiElement> {

    private final Project project;

    private final FileType realType;

    private final Class<File> fileClazz;

    protected SquashVirtualFileLocationService(Project project, FileType realType, Class<File> fileClazz) {
        this.project = project;
        this.realType = realType;
        this.fileClazz = fileClazz;
    }

    public String getProjectBasePath(){
        return project.getBasePath();
    }

    public List<File> getProjectSquashFiles() {
        List<File> result = new ArrayList<>();

        //get the virtual files of all TA File/Macro files in the working project
        Collection<VirtualFile> virtualFiles = getVirtualFiles();

        for ( VirtualFile virtualFile: virtualFiles ) {
            final PsiFile file = PsiManager.getInstance(project).findFile(virtualFile);
            try {
                File realFile = fileClazz.cast(file);
                //get the non empty TA Macro files
                if(realFile != null) {
                    result.add(realFile);
                }
            } catch (ClassCastException cce) {
                Class<?> psiFileClazz = (file !=null)? file.getClass() :  null;
                throw new IllegalArgumentException("Error while trying to cast "+ psiFileClazz+" into "+fileClazz, cce);
            }
        }
        return Collections.unmodifiableList(result);
    }

    File getCurrentSquashFile(PsiFile taMacroElement) {
        //get the virtual files of the TA working file
        Collection<VirtualFile> virtualFiles = getCurrentVirtualFile(taMacroElement);
        if (!virtualFiles.isEmpty()) {
            final PsiFile file = PsiManager.getInstance(project).findFile(virtualFiles.iterator().next());
            try {
                return fileClazz.cast(file);
            } catch (ClassCastException cce) {
                Class<?> psiFileClazz = (file !=null)? file.getClass() :  null;
                throw new IllegalArgumentException("Error while trying to cast "+ psiFileClazz+" into "+fileClazz, cce);
            }
        }
        return null;

    }

    //get the virtual files of all TA File/Macro files in the working project
    private Collection<VirtualFile> getVirtualFiles() {
        Collection<VirtualFile> virtualFiles = FileBasedIndex.getInstance().getContainingFiles(ID.create("filetypes"),
                realType, GlobalSearchScope.allScope(project));
        return Collections.unmodifiableCollection(virtualFiles);
    }

    //get the virtual files of the current TA File/Macro file
    private Collection<VirtualFile> getCurrentVirtualFile(PsiFile taMacroElement){
        Collection<VirtualFile> virtualFiles = FileBasedIndex.getInstance().getContainingFiles(ID.create("filetypes"),
                realType, GlobalSearchScope.fileScope(taMacroElement));
        return Collections.unmodifiableCollection(virtualFiles);
    }

    protected boolean isInExpectedFolder(String currentFilePath) {
        Path actualFolderPath = Paths.get(currentFilePath).getParent();
        Path expectedFolderPath = Paths.get(project.getBasePath() + expectedFolder());
        if(allowSubfolders()){
            return actualFolderPath.startsWith(expectedFolderPath);
        }else {
            return actualFolderPath.equals(expectedFolderPath);
        }
    }

    @NotNull
    protected abstract String expectedFolder();

    /** Some project elements may be organized in subfolders, other may not. Each specific service will define this accordingly. */
    protected abstract boolean allowSubfolders();
}
