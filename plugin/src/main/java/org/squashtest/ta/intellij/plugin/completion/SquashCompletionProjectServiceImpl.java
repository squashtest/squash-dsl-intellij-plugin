/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.completion;

import com.intellij.codeInsight.completion.CompletionResultSet;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.psi.PsiElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLMacro;
import org.squashtest.ta.galaxia.metaexecution.enginelink.EngineLinkException;

import java.util.List;
import java.util.Set;

class SquashCompletionProjectServiceImpl {

    private final Logger LOGGER= LoggerFactory.getLogger(SquashCompletionProjectServiceImpl.class);

    private final MacroCallLineCompletionHelper macroCallLineCompletionHelper;

    SquashCompletionProjectServiceImpl(MacroCallLineCompletionHelper macroCallLineCompletionHelper) {
        this.macroCallLineCompletionHelper=macroCallLineCompletionHelper;
    }

    void macroLineContentAC(CompletionResultSet result, PsiElement macroLine) throws EngineLinkException {
        LOGGER.info("Starting Squash Macro line autocompletion");

        /* remove the "IntellijIdeaRulezzz" in the string
         * TODO : Stray hack from hell detected ! We need to make dead sure we can't do this another way. AND that this won't ever collide with user code (Good Lord !).
         */
        String text = macroLine.getText();
        String element = "IntellijIdeaRulezzz";
        String newMacroName = (text.contains(element)) ? text.substring(2, text.indexOf(element)) : text;

        List<SquashDSLMacro> proposals=macroCallLineCompletionHelper.macroLineCompletion(newMacroName.toUpperCase());

        String amorce;
        if(newMacroName.endsWith(" ")){
            amorce="";
        }else{
            amorce=newMacroName;
        }

        for(SquashDSLMacro proposedMacro:proposals) {
            Set<String> proposalsForMacro = macroCallLineCompletionHelper.buildCompletionProposalsForMacro(newMacroName.toUpperCase(), proposedMacro);
            LOGGER.debug("Proposing {} completions : {}",proposalsForMacro.size(),proposalsForMacro);
            for(String proposal:proposalsForMacro) {

                String lookupString = amorce + proposal + " ";
                LOGGER.debug("Proposing '{}'", lookupString);
                if (proposedMacro.isCustomMacro()) {
                    result.addElement(LookupElementBuilder.create(lookupString).withCaseSensitivity(false));
                } else {
                    result.addElement(LookupElementBuilder.create(lookupString).withCaseSensitivity(false).bold());
                }
            }
        }
    }
}
