/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.completion;

public class SquashPropertyListTemplate {
    private String name;
    private String headProperty;
    private SquashPropertyTemplate[] properties;

    public SquashPropertyListTemplate(String name, String headProperty, SquashPropertyTemplate[] properties) {
        this.name = name;
        this.headProperty = headProperty;
        this.properties = properties;
    }

    public String getName() {
        return name;
    }

    public String getHeadProperty() {
        return headProperty;
    }

    public SquashPropertyTemplate[] getProperties() {
        return properties;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(headProperty);
        for (SquashPropertyTemplate prop : properties) {
            builder.append(" ").append(prop.toString());
        }
        return builder.toString();
    }

}
