/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.completion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLMacro;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLMacroSignature;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLMacroSignatureVisitor;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLComponentRegistry;
import org.squashtest.ta.galaxia.metaexecution.enginelink.EngineLinkException;
import org.squashtest.ta.intellij.plugin.fwconnector.IdeaFrameworkConnector;
import org.squashtest.ta.intellij.plugin.macro.psi.SquashMacroMacroTitle;
import org.squashtest.ta.intellij.plugin.projectmodel.MacroDefinitionOperations;
import org.squashtest.ta.intellij.plugin.projectmodel.SquashMacroFileLocationProjectService;
import org.squashtest.ta.intellij.plugin.tools.LineSearchingResult;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeoutException;

public class MacroCallLineCompletionHelper extends CompletionHelper{
    private static final Logger LOGGER = LoggerFactory.getLogger(MacroCallLineCompletionHelper.class);

    private SquashMacroFileLocationProjectService macroFileLocationService;
    private IdeaFrameworkConnector connector;
    private MacroDefinitionOperations macroDefinitionOperations=new MacroDefinitionOperations();

    MacroCallLineCompletionHelper(SquashMacroFileLocationProjectService macroLocationSrv, IdeaFrameworkConnector connector){
        this.macroFileLocationService=macroLocationSrv;
        this.connector = connector;
    }

    /**
     * This method is to provide Macro Autocompletion for TA File and Macro
     * @param macroCallPrimer the line content which is supposed to be the beginning of an incomplete macro call line.
     */
    List<SquashDSLMacro> macroLineCompletion(final String macroCallPrimer) throws EngineLinkException {

        LOGGER.debug("Looking up relevant macro propositions for primer '{}'",macroCallPrimer);

        //macro template suggestions whose content is auto-updated based on current user macroLine value
        List<SquashDSLMacro> proposedMacroList = getAllMacroTemplates();

        if(macroCallPrimer.trim().isEmpty()){    // no primer, we propose every macro for completion
            return  proposedMacroList;
        }else {                                 // now, if we DO have a primer, we'll filter macros to only propose macros which call may start with that primer
            List<SquashDSLMacro> proposals = new ArrayList<>();
            for (SquashDSLMacro candidateMacro : proposedMacroList) {

                LOGGER.debug("Checking '{}' against the '{}' primer.",candidateMacro, macroCallPrimer);

                SquashDSLMacroSignatureVisitor<LineSearchingResult> filterVisitor= new PrimerMatcherSquashDSLMacroSignatureVisitor(macroCallPrimer);

                LineSearchingResult filteringResult=null;
                Iterator<SquashDSLMacroSignature> partSource=candidateMacro.getSignatures().iterator();
                while(partSource.hasNext() && (filteringResult == null || (filteringResult.wasFound() && filteringResult.getEndIndex()< macroCallPrimer.length()))){
                    filteringResult=partSource.next().visit(filterVisitor);
                }
                if(filteringResult != null && filteringResult.wasFound()) {
                    LOGGER.debug("'{}' matches the '{}' primer, proposing it!",candidateMacro, macroCallPrimer);
                    proposals.add(candidateMacro);
                } else{
                    LOGGER.debug("Rejecting '{}' as it doesn't fit the '{}' primer.",candidateMacro, macroCallPrimer);
                }
            }
            LOGGER.debug("{} selected proposals for macro call primer '{}'.",proposals.size(),macroCallPrimer);
            return proposals;
        }
    }

    Set<String> buildCompletionProposalsForMacro(String primer, SquashDSLMacro macro){

        ProposalBuildSquashDSLMacroSignatureVisitor proposalBuilder= new ProposalBuildSquashDSLMacroSignatureVisitor(primer);

        for(SquashDSLMacroSignature signatureElement:macro.getSignatures()){
            signatureElement.visit(proposalBuilder);
        }

        return proposalBuilder.getProposals();
    }

    private List<SquashDSLMacro> getAllMacroTemplates() throws EngineLinkException {
        List<SquashDSLMacro> result = new ArrayList<>();
        try {
            SquashDSLComponentRegistry macroJsonFileContent = connector.getSquashDSLComponentRegistry();

            List<SquashDSLMacro> builtInMacroList = macroJsonFileContent.getMacros();

            result.addAll(builtInMacroList);
            LOGGER.debug("Added {} macros from the project's component registry.",builtInMacroList.size());

        //charge all marcos that is user-defined in the project
        List<SquashMacroMacroTitle> projectMacroList = macroFileLocationService.getMacroTitlesInProjectShortcutsFolder();

        for (SquashMacroMacroTitle projectMacro : projectMacroList) {

            List<SquashDSLMacroSignature> tempDSLMacroSigList = macroDefinitionOperations.createDSLMacroSignatureListFromPsiElementTitle(projectMacro);
            result.add(new SquashDSLMacro(projectMacro.getContainingFile().getName(),tempDSLMacroSigList, true));
        }
        }catch(TimeoutException e){
           //TODO : this is probably more akin to connector management ... let's review this after the first re-packaging phase.
           LOGGER.warn(TIMEOUT_EXCEPTION_MSG,e);
        }
        // this list is to later control if a proposed macro line is built_in or custom one for different display - or not, because we don't give a damn !
        return result;
    }
}
