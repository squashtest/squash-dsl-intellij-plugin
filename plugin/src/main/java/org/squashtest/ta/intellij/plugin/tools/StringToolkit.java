/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.tools;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class offers various String manipulation tools.
 * TODO : this should be reviewed against JDK API, even tough there is no obvious direct replacements there, otherwise I'd have killed ours without mercy.
 *
 * @author edegenetais
 */
public class StringToolkit {

    public static final Logger LOGGER = LoggerFactory.getLogger(StringToolkit.class);

    private StringToolkit(){/* Just to barr subclassing (VERY bad idea with static code) or instanciation (misleading with static code). */}
    static boolean compareTwoTrimmedStringIgnoredCases(String s1, String s2){
        if (s1!=null && s2!=null) {
            return s1.trim().equalsIgnoreCase(s2.trim());
        } else  {
            return (s1==null && s2==null);
        }
    }

    /**
     * This method is to get the position of the i th (1st, 2nd, 3rd, or 4th...) element in a string content
     * @param content the current string content
     * @param string the searching regex pattern
     * @param endIndicator the ending searching regex pattern
     * @param i the i th element to be searched (1st, 2nd, 3rd, or 4th...)
     * @return an LineSearchResult object with occurrence location data if found,
     * or the {@link LineSearchingResult#LINE_SEARCH_MISS} object with wasFound() {@literal =>} false to state that the search failed.
     */
    public static LineSearchingResult getIthIndexes(String content, String string, String endIndicator, int i) {
        List<LineSearchingResult> results = getAllIndexes(content,string, endIndicator);
        int resultsSize = results.size();
        if (resultsSize > 0 && i <= resultsSize) {
            return results.get(i-1);
        }
        return LineSearchingResult.LINE_SEARCH_MISS;
    }

    /**
     * This method is to get the position of all specific elements in a string content
     * @param content the current string content
     * @param targetPattern the searching regex pattern
     * @param endIndicator the ending searching regex pattern
     * @return the element position and its text range list
     */
    static List<LineSearchingResult> getAllIndexes(String content, String targetPattern, String endIndicator) {
        List<LineSearchingResult> results = new ArrayList<>();

        String regex = "(?m)^[ \\t\\x0B]*"+targetPattern+endIndicator;
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(content);
        while (matcher.find()){
            String test = matcher.group();
            LineSearchingResult result = new LineSearchingResult.LineSearchHit(matcher.start(), matcher.end(), test);
            results.add(result);
        }
        return results;
    }

    /**
     * This function finds out if a specified string contains any complete occurrence of a target string,
     * or ends with the target's beginning.
     *
     * @param primer the content string we're searching.
     * @param fixedPart the target string we're looking for.
     * @param beginIndex offset at which we want to begin the search
     * @return the search result, which may be {@link LineSearchingResult#LINE_SEARCH_MISS} if the target was not found.
     *
     */
    public static LineSearchingResult findCompleteOccurrenceOrPartialEndingOccurrence(String primer, String fixedPart, int beginIndex){
        LOGGER.debug("Looking up completeOrFinalOccurrence of '{}' in '{}', starting at {}.",fixedPart,primer,beginIndex);
        int completeMatchIndex=primer.indexOf(fixedPart,beginIndex);
        if(completeMatchIndex>=0){
            return new LineSearchingResult.LineSearchHit(completeMatchIndex,completeMatchIndex+fixedPart.length(),fixedPart);
        }else{
            /*
             * not found, so at least one character must be missing
             * => we begin the lookup for a partial occurrence at (fixedPart.length()-1 chars from the primer end.
             */
            int searchIndex=Math.max(0,primer.length()-(fixedPart.length()-1));
            while(searchIndex<primer.length()){
                int fixedPartIndex=0;
                while(searchIndex+fixedPartIndex<primer.length() && primer.charAt(searchIndex+fixedPartIndex) == Character.toUpperCase(fixedPart.charAt(fixedPartIndex))){
                    LOGGER.debug("Possible occurence at {}, for char {} in target.",searchIndex,fixedPartIndex);
                    fixedPartIndex++;
                }
                if(searchIndex+fixedPartIndex>=primer.length()){
                    LOGGER.debug("Found at {}.",searchIndex);
                    return new LineSearchingResult.LineSearchHit(searchIndex,primer.length(),fixedPart.substring(0,fixedPartIndex));
                }else{
                    LOGGER.debug("Not found, searching from next character.");
                    searchIndex++;
                }
            }
            LOGGER.debug("Missed");
            return LineSearchingResult.LINE_SEARCH_MISS;
        }
    }
}
