/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.lexer;

import com.intellij.lexer.FlexLexer;
import com.intellij.psi.tree.IElementType;
import com.intellij.psi.TokenType;

import org.squashtest.ta.intellij.plugin.macro.psi.SquashMacroTypes;

%%

%{
    private String inlineText;
    private int count = 0;
    private boolean inUsingValueList;
    private boolean inlineInUsing;

    void append(char c){
        inlineText += c;
    }

    private void startInlineState() {
        ++count;
        inlineText += "$(";
    }

    private void getInParenthesis() {
        ++count;
        inlineText += "(";
    }

    private void getOutParenthesis() {
        if (count > 0){
            --count;
            inlineText += ')';
        }
    }
%}

%class SquashMacroLexer
%implements FlexLexer
%unicode
%function advance
%type IElementType
%ignorecase
%eof{  return;
%eof}

WHITE_SPACE=[\ \t\x0B]
END_LINE=[\r\n]
COMMENT="//"[^\r\n]*

SYMBOL="#"
SEPARATOR="=>"

MACRO_TITLE_CHARACTER=[^"{""}"\r\n\f]
MACRO_TITLE_PARAM_CHARACTER=[^"{""}"\ \t\x0B\r\n\f]

MACRO_LINE_CONTENT=[^\r\n\f]

ATOMIC_VALUE=[\w"-"".""/"\\]+
NUMBER_PLACEHOLDER="{%%"[^"}"]+?"}"

//Let's spend sometime to think about unbinding it here
UNIQUE_NUMBER_VALUE={ATOMIC_VALUE}*{NUMBER_PLACEHOLDER}{ATOMIC_VALUE}*

BUILT_IN_VALUE="{"{ATOMIC_VALUE}?"}"
TEMP_SCOPE_VALUE="{{"{ATOMIC_VALUE}?"}}"
CONVERTER_NAME="("[^"("")"]+")"

OPEN_INLINE="$("
OPEN_PARANTHESE="("
CLOSE_PARANTHESE=")"
INLINE_VALUE_CHAR=[^"("")"\r\n\f]

CMD_HEAD_KEY = "LOAD" | "CONVERT" | "ASSERT" | "VERIFY"
EXECUTE = "EXECUTE"
DEFINE = "DEFINE"

CMD_KEY = "AS" | "WITH" | "ON" | "THAN" | "THE" | "FROM"
USING = "USING"
VALUE_SEPARATOR = ","
TO = "TO"
VALIDATOR = "IS" | "HAS" | "DOES"

TITLE_WAITER = ([^"#"\r\n\f][^\r\n\f]*) | ("#"[^\ \t]+[^\r\n\f]*)

AC_POSITION = ([^"$"\ \t\x0B\r\n\f][^\ \t\x0B\r\n\f]*) | ("$"[^"("\ \t\x0B\r\n\f]*)
AC_POSITION_IN_USING = ([^"$"\ \t\x0B\r\n\f","][^\ \t\x0B\r\n\f","]*) | ("$"[^"("\ \t\x0B\\r\n\f","]*)

%state MACRO_TITLE, MACRO_TITLE_PROP, CONTENT, MACRO_LINE, CMD_KEY, CMD_VALUE, DEFINE_VALUE, CONVERSION, EXECUTE_CMD, ASSERTION
%xstate INLINE, USING_VALUE_LIST

%%
<YYINITIAL>{WHITE_SPACE}*{SEPARATOR}{WHITE_SPACE}*                             { yybegin(CONTENT); return SquashMacroTypes.SEPARATOR; }

<YYINITIAL, MACRO_TITLE, MACRO_TITLE_PROP>({WHITE_SPACE}*{END_LINE})+                 { yybegin(YYINITIAL); return SquashMacroTypes.CRLF; }
<YYINITIAL>^{WHITE_SPACE}*{SYMBOL}{WHITE_SPACE}+                                      { yybegin(MACRO_TITLE); return SquashMacroTypes.SYMBOL; }

<YYINITIAL>^{TITLE_WAITER}                                                            { yybegin(MACRO_TITLE_PROP); return SquashMacroTypes.TITLE_WAITER; }

<MACRO_TITLE>"{"{MACRO_TITLE_CHARACTER}*"}"                                           { yybegin(MACRO_TITLE); return SquashMacroTypes.TITLE_FIRST_PARAM; }
<MACRO_TITLE, MACRO_TITLE_PROP>{MACRO_TITLE_CHARACTER}+                               { yybegin(MACRO_TITLE_PROP); return SquashMacroTypes.TITLE_KEY; }
<MACRO_TITLE_PROP>"{"{MACRO_TITLE_PARAM_CHARACTER}*"}"                                { return SquashMacroTypes.TITLE_PARAM; }


<CONTENT, CMD_KEY, CMD_VALUE, DEFINE_VALUE, EXECUTE_CMD, CONVERSION, ASSERTION, MACRO_LINE, INLINE, USING_VALUE_LIST>
({WHITE_SPACE}*{END_LINE})+                                                           { yybegin(CONTENT); return SquashMacroTypes.CRLF; }
<CONTENT>^{WHITE_SPACE}*{COMMENT}                                                     { yybegin(CONTENT); return SquashMacroTypes.COMMENT; }
<CONTENT>^{WHITE_SPACE}*{SYMBOL}{WHITE_SPACE}+                                        { yybegin(MACRO_LINE); return SquashMacroTypes.SYMBOL; }

//a whitespace after any CMD_KEY words to idenity them from a non-defining word before the auto-completion
<CONTENT>^{WHITE_SPACE}*{CMD_HEAD_KEY}{WHITE_SPACE}                                   { yybegin(CMD_VALUE); return SquashMacroTypes.CMD_HEAD_KEY; }
<CONTENT>^{WHITE_SPACE}*{DEFINE}{WHITE_SPACE}                                         { yybegin(DEFINE_VALUE); return SquashMacroTypes.CMD_HEAD_KEY; }
<CONTENT>^{WHITE_SPACE}*{EXECUTE}{WHITE_SPACE}                                        { yybegin(EXECUTE_CMD); return SquashMacroTypes.CMD_HEAD_KEY; }

<MACRO_LINE>{MACRO_LINE_CONTENT}+                                                     { return SquashMacroTypes.MACRO_LINE_CONTENT; }

<CMD_KEY, USING_VALUE_LIST>{WHITE_SPACE}+{CMD_KEY}{WHITE_SPACE}+                      { yybegin(CMD_VALUE); return SquashMacroTypes.CMD_KEY; }
<CMD_KEY, USING_VALUE_LIST>{WHITE_SPACE}+{TO}{WHITE_SPACE}+                           { yybegin(CONVERSION); return SquashMacroTypes.CMD_KEY; }
<CMD_KEY, USING_VALUE_LIST>{WHITE_SPACE}+{VALIDATOR}{WHITE_SPACE}+                    { yybegin(ASSERTION); return SquashMacroTypes.CMD_KEY; }
<CMD_KEY>{WHITE_SPACE}+{USING}{WHITE_SPACE}+                                          { yybegin(USING_VALUE_LIST); inUsingValueList = true; return SquashMacroTypes.USING; }

<CONVERSION>{ATOMIC_VALUE}({WHITE_SPACE}*{CONVERTER_NAME})?                           { yybegin(CMD_KEY); return SquashMacroTypes.CONVERTER; }

<CMD_VALUE, DEFINE_VALUE, EXECUTE_CMD, ASSERTION>{OPEN_INLINE}                        { yybegin(INLINE); inlineText=""; count=0; startInlineState();
                                                                                      return  SquashMacroTypes.INLINE_VALUE_CHAR;}

<USING_VALUE_LIST>{
    {OPEN_INLINE}                                                                     { if (inUsingValueList) {
                                                                                        yybegin(INLINE); inlineText=""; count=0; inlineInUsing=true; startInlineState();
                                                                                        return  SquashMacroTypes.INLINE_VALUE_CHAR;}
                                                                                      }
    {WHITE_SPACE}*{VALUE_SEPARATOR}{WHITE_SPACE}*                                     { inUsingValueList = true; return  SquashMacroTypes.VALUE_SEPARATOR;}
    {ATOMIC_VALUE} | {BUILT_IN_VALUE} | {TEMP_SCOPE_VALUE} | {UNIQUE_NUMBER_VALUE}    { if (inUsingValueList) {
                                                                                        inUsingValueList = false;
                                                                                        return SquashMacroTypes.VALUE; }
                                                                                      }
    {AC_POSITION_IN_USING}                                                            { if (inUsingValueList) {
                                                                                            inUsingValueList = false;
                                                                                            return SquashMacroTypes.AC_POSITION; }
                                                                                      }
    {WHITE_SPACE}+                                                                    { return TokenType.WHITE_SPACE; }
    [^]                                                                               { return TokenType.BAD_CHARACTER; }
}

<INLINE>{
    {CLOSE_PARANTHESE}                                                                { getOutParenthesis(); if (count == 0) {
                                                                                          if (inlineInUsing) {
                                                                                              yybegin(USING_VALUE_LIST);
                                                                                              inUsingValueList = false;
                                                                                              inlineInUsing = false;
                                                                                          } else {
                                                                                              yybegin(CMD_KEY);
                                                                                          }
                                                                                          return SquashMacroTypes.INLINE_VALUE_CHAR;
                                                                                          }
                                                                                      return SquashMacroTypes.INLINE_VALUE_CHAR;}
    {INLINE_VALUE_CHAR}                                                               { return SquashMacroTypes.INLINE_VALUE_CHAR;}
    {OPEN_PARANTHESE} | {OPEN_INLINE}                                                 { getInParenthesis(); return SquashMacroTypes.INLINE_VALUE_CHAR;}
}

<CMD_VALUE>{ATOMIC_VALUE} | {BUILT_IN_VALUE} | {TEMP_SCOPE_VALUE} | {UNIQUE_NUMBER_VALUE}                     { yybegin(CMD_KEY); return SquashMacroTypes.VALUE; }
<EXECUTE_CMD>{ATOMIC_VALUE} | {BUILT_IN_VALUE} | {TEMP_SCOPE_VALUE} | {UNIQUE_NUMBER_VALUE}                   { yybegin(CMD_KEY); return SquashMacroTypes.EXECUTE_CMD; }
<ASSERTION>{ATOMIC_VALUE} | {BUILT_IN_VALUE} | {TEMP_SCOPE_VALUE} | {UNIQUE_NUMBER_VALUE}                     { yybegin(CMD_KEY); return SquashMacroTypes.ASSERTION_VALIDATOR; }

{WHITE_SPACE}+                                                                        { return TokenType.WHITE_SPACE; }

<CONTENT, CMD_KEY>{AC_POSITION}                                                       { yybegin(CMD_VALUE); return SquashMacroTypes.AC_POSITION; }
<CMD_VALUE, DEFINE_VALUE, EXECUTE_CMD, CONVERSION, ASSERTION>{AC_POSITION}            { yybegin(CMD_KEY); return SquashMacroTypes.AC_POSITION; }


//important to handle unidentified element, don't remove it even there's an IDE warning!!!
[^]                                                                                   { return TokenType.BAD_CHARACTER; }
