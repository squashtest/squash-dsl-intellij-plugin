/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.validation;

import org.jetbrains.annotations.NotNull;
import org.squashtest.ta.galaxia.dsltools.DslTools;
import org.squashtest.ta.galaxia.dsltools.MacroValidator;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLComponentRegistry;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLMacro;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLMacroFixedPart;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLMacroSignature;
import org.squashtest.ta.intellij.plugin.macro.psi.SquashMacroMacroTitle;
import org.squashtest.ta.intellij.plugin.projectmodel.MacroDefinitionOperations;
import org.squashtest.ta.intellij.plugin.projectmodel.Source;
import org.squashtest.ta.intellij.plugin.projectmodel.SquashMacroFileLocationProjectService;

import java.util.*;

public class MacroCallValidationHelper {

    private final MacroDefinitionOperations macroDefinitionOperations = new MacroDefinitionOperations();

    private SquashMacroFileLocationProjectService macroFileLocator;

    public MacroCallValidationHelper(SquashMacroFileLocationProjectService macroFileLocator){
        this.macroFileLocator =macroFileLocator;
    }

    @NotNull
    String getMacroLineMsg(SquashMacroCallControlResult macroControlResult) {
        String message;
        int matchNumber = macroControlResult.getMatchNumber();
        List<SquashMacroMacroTitle> macroTitles = macroControlResult.getCustomMacroTitles();
        int inCustom = (macroTitles.isEmpty())? 0: macroTitles.size();
        int inBuiltIn = matchNumber - inCustom;

        if (inBuiltIn <= 0 && inCustom <= 0) {
            message = "Macro line definition not found";
        } else if (inBuiltIn > 1 || inCustom > 1 || (inBuiltIn == 1 && inCustom == 1)) {
            message = "Macro line multi-definition conflict";
        } else {
            StringBuilder builder = new StringBuilder();
            SquashDSLMacro resultMacroLine = macroControlResult.getLatestMacroLineTemplate();
            for (SquashDSLMacroSignature signature :resultMacroLine.getSignatures()){
                if (signature instanceof SquashDSLMacroFixedPart) builder.append(((SquashDSLMacroFixedPart) signature).getContent()).append("\n");
            }
            message = builder.toString();
        }
        return message;
    }

    /**
      * This method is to check if a Macro line content is defined in either built-in macro list or in the project 'shortcuts' folder or one of its subfolders
      * @param macroLineContent the current Macro line content
      * @param macroJsonFileContent component registry
      * @return an object that save the macro checking result
      */
    SquashMacroCallControlResult checkMacroLineContent(String macroLineContent, SquashDSLComponentRegistry macroJsonFileContent)  {
        SquashMacroCallControlResult result = new SquashMacroCallControlResult();
        if (macroLineContent != null && !"".equals(macroLineContent.trim())){
            result.setMatchNumber(0);
            result.setCustomMacroTitles(new ArrayList<>());
            result.setLatestMacroLineTemplate(new SquashDSLMacro());
            result.setBestMatchSpecificity(0.0f);

             //first check it with the framework built-in macros.
             List<SquashDSLMacro> builtInMacroList = macroJsonFileContent.getMacros();
             getBuiltInResult(result, macroLineContent, builtInMacroList);

             //then check with all custom defined macros in the current project 'shortcuts' folder
             //charge all macro titres in the project 'shortcuts' folder
             List<SquashMacroMacroTitle> projectMacroTitleList = macroFileLocator.getMacroTitlesInProjectShortcutsFolder();
             getCustomResult(result, macroLineContent, projectMacroTitleList);
         } else {
            result.setEmptyMacroLine(true);
        }
         return result;
     }

    private void getCustomResult(SquashMacroCallControlResult result, String macroLineContent, List<SquashMacroMacroTitle> projectMacroTitleList) {
        //create SquashDSLMacro object for each of custom SquashMacroMacroTitle
        Map<SquashMacroMacroTitle, SquashDSLMacro> customMacroMap = new LinkedHashMap<>();
        int i = 1;
        for (SquashMacroMacroTitle signature: projectMacroTitleList) {
            List<SquashDSLMacroSignature> tempDSLMacroSigList = macroDefinitionOperations.createDSLMacroSignatureListFromPsiElementTitle(signature);
            SquashDSLMacro tempDSLMacro =  new SquashDSLMacro("custom"+i, tempDSLMacroSigList);
            customMacroMap.put(signature, tempDSLMacro);
            ++i;
        }

        Collection<SquashDSLMacro> customMacroDSLCollection= customMacroMap.values();
        List<MacroValidator> macroValidators = new DslTools().getMacroDslToolsFactory().getValidators(customMacroDSLCollection);
        macroValidation(macroLineContent, macroValidators, result, Source.CUSTOM, customMacroMap);
    }

    private void macroValidation(String macroLineContent, List<MacroValidator> macroValidators, SquashMacroCallControlResult result, Source source, Map<SquashMacroMacroTitle, SquashDSLMacro> customMacroMap) {
        int duplicate = result.getMatchNumber();
        float bestMatchSpecificity = result.getBestMatchSpecificity();

        for (MacroValidator validator : macroValidators ) {
            if (validator != null && validator.isValidInstanceOfMe(macroLineContent)){
                float currentSpec = validator.getMatchSpecificity();
                if (currentSpec > bestMatchSpecificity) {
                    duplicate = 1;
                    bestMatchSpecificity = currentSpec;
                    result.setLatestMacroLineTemplate(validator.originalMacroDefinition());
                    result.setBestMatchSpecificity(validator.getMatchSpecificity());
                    //insert the custom PsiElement macro signature for navigation
                    macroDefinitionOperations.addMacroSignaturePsiElementIntoList(result, source, customMacroMap, validator);
                } else if (currentSpec == bestMatchSpecificity && bestMatchSpecificity != 0) {
                    ++duplicate;
                    //insert the custom PsiElement macro signature for navigation
                    macroDefinitionOperations.addMacroSignaturePsiElementIntoList(result, source, customMacroMap, validator);
                }
            }
        }
        result.setMatchNumber(duplicate);
    }

    private void getBuiltInResult(SquashMacroCallControlResult result, String macroLineContent, List<SquashDSLMacro> builtInMacroList) {
        List<MacroValidator> macroValidators = new DslTools().getMacroDslToolsFactory().getValidators(builtInMacroList);
        macroValidation(macroLineContent, macroValidators, result, Source.BUILT_IN, null);

    }
}
