/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.completion;

public enum SquashDSLInstructionTemplate {
    DEFINE (new SquashPropertyListTemplate("DEFINE", "DEFINE $(raw_data)", new SquashPropertyTemplate[]{new SquashPropertyTemplate("AS", "nameInTheContext")})),
    LOAD (new SquashPropertyListTemplate("LOAD", "LOAD path_To_Resource", new SquashPropertyTemplate[]{new SquashPropertyTemplate("FROM", "resourceLibrary"), new SquashPropertyTemplate("AS", "nameInTheContext")})),
    LOAD_FROM (new SquashPropertyListTemplate("LOAD_FROM", "LOAD path_To_Resource", new SquashPropertyTemplate[]{new SquashPropertyTemplate("AS", "nameInTheContext")})),
    CONVERT (new SquashPropertyListTemplate("CONVERT", "CONVERT resourceToConvert", new SquashPropertyTemplate[]{new SquashPropertyTemplate("TO", "outputType(converterName)"), new SquashPropertyTemplate("AS", "convertedResource")})),
    EXECUTE (new SquashPropertyListTemplate("EXECUTE", "EXECUTE command", new SquashPropertyTemplate[]{new SquashPropertyTemplate("WITH", "resource"), new SquashPropertyTemplate("ON", "target"), new SquashPropertyTemplate("AS", "result")})),
    ASSERT(new SquashPropertyListTemplate("ASSERT", "ASSERT resourceToTest", new SquashPropertyTemplate[]{new SquashPropertyTemplate("DOES", "binaryAssertContent"),new SquashPropertyTemplate("THE", "expectedResult")})),
    ASSERT_IS_BINARY (new SquashPropertyListTemplate("ASSERT_IS_BINARY", "ASSERT resourceToTest", new SquashPropertyTemplate[]{new SquashPropertyTemplate("IS", "binaryAssertContent"),new SquashPropertyTemplate("THE", "expectedResult")})),
    ASSERT_IS_UNARY (new SquashPropertyListTemplate("ASSERT_IS_UNARY ", "ASSERT resourceToTest", new SquashPropertyTemplate[]{new SquashPropertyTemplate("IS", "unaryAssertContent")})),
    ASSERT_DOES_UNARY (new SquashPropertyListTemplate("ASSERT_DOES_UNARY", "ASSERT resourceToTest", new SquashPropertyTemplate[]{new SquashPropertyTemplate("DOES", "unaryAssertContent")})),
    ASSERT_HAS_UNARY (new SquashPropertyListTemplate("ASSERT_HAS_UNARY", "ASSERT resourceToTest", new SquashPropertyTemplate[]{new SquashPropertyTemplate("HAS", "unaryAssertContent")})),
    VERIFY(new SquashPropertyListTemplate("VERIFY", "VERIFY resourceToTest", new SquashPropertyTemplate[]{new SquashPropertyTemplate("DOES", "binaryVerifyContent"),new SquashPropertyTemplate("THE", "expectedResult")})),
    VERIFY_IS_BINARY (new SquashPropertyListTemplate("VERIFY_IS_BINARY", "VERIFY resourceToTest", new SquashPropertyTemplate[]{new SquashPropertyTemplate("IS", "binaryVerifyContent"),new SquashPropertyTemplate("THE", "expectedResult")})),
    VERIFY_IS_UNARY (new SquashPropertyListTemplate("VERIFY_IS_UNARY ", "VERIFY resourceToTest", new SquashPropertyTemplate[]{new SquashPropertyTemplate("IS", "unaryVerifyContent")})),
    VERIFY_DOES_UNARY (new SquashPropertyListTemplate("VERIFY_DOES_UNARY", "VERIFY resourceToTest", new SquashPropertyTemplate[]{new SquashPropertyTemplate("DOES", "unaryVerifyContent")})),
    VERIFY_HAS_UNARY (new SquashPropertyListTemplate("VERIFY_HAS_UNARY", "VERIFY resourceToTest", new SquashPropertyTemplate[]{new SquashPropertyTemplate("HAS", "unaryVerifyContent")}));

    private SquashPropertyListTemplate template;

    SquashDSLInstructionTemplate(SquashPropertyListTemplate template) {
        this.template = template;
    }

    @Override
    public String toString() {
        return template.toString();
    }

    public SquashPropertyTemplate[] getProperties() {
        return template.getProperties();
    }

}
