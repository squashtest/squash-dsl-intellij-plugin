/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.projectmodel;

import org.jetbrains.annotations.NotNull;
import org.squashtest.ta.galaxia.dsltools.MacroValidator;
import org.squashtest.ta.galaxia.enginelink.components.*;
import org.squashtest.ta.intellij.plugin.macro.psi.SquashMacroMacroTitle;
import org.squashtest.ta.intellij.plugin.macro.psi.SquashMacroMacroTitleContent;
import org.squashtest.ta.intellij.plugin.macro.psi.SquashMacroMacroTitleProperty;
import org.squashtest.ta.intellij.plugin.tools.CollectionToolkit;
import org.squashtest.ta.intellij.plugin.validation.SquashMacroCallControlResult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Provide operations on macro-related IntelliJ project/code model objects.
 */
public class MacroDefinitionOperations {

    public List<SquashDSLMacroSignature> createDSLMacroSignatureListFromPsiElementTitle(SquashMacroMacroTitle signature) {
        List<SquashDSLMacroSignature> result = new ArrayList<>();
        SquashMacroMacroTitleContent sigContent = signature.getMacroTitleContent();

        if (sigContent != null) {
            String firstParam = sigContent.getTitleFirstParam();
            if (firstParam !=null) {
                String modifiedFirstParam = removeCurlyBracket(firstParam);
                result.add(new SquashDSLMacroParam(new SquashDSLMacroParamDefinition(modifiedFirstParam, null)));
            }
            List<SquashMacroMacroTitleProperty> macroTitlePropertyList= sigContent.getMacroTitlePropertyList();
            for (SquashMacroMacroTitleProperty sigProp : macroTitlePropertyList){
                addDSLComponentToResult(result, sigProp);
            }
        }

        return result;
    }



    private void addDSLComponentToResult(List<SquashDSLMacroSignature> result, SquashMacroMacroTitleProperty sigProp) {
        if (sigProp!= null) {
            String key = sigProp.getTitleKey();
            if (!"".equals(key)) {
                result.add(new SquashDSLMacroFixedPart(key));
            }
            String param = removeCurlyBracket(sigProp.getTitleParam());

            if (param !=null) {
                result.add(new SquashDSLMacroParam(new SquashDSLMacroParamDefinition(param, null)));
            }
        }
    }

    //TODO : untangle this from validation code : we are talking transcoding here, not validation
    public void addMacroSignaturePsiElementIntoList(SquashMacroCallControlResult result, Source source, Map<SquashMacroMacroTitle, SquashDSLMacro> customMacroMap, MacroValidator validator) {
        if (Source.CUSTOM.equals(source)){
            SquashDSLMacro temp = validator.originalMacroDefinition();
            for (Map.Entry<SquashMacroMacroTitle, SquashDSLMacro> entry : customMacroMap.entrySet()) {
                addPsiElementFromMapEntry(result, temp, entry);
            }
        }
    }

    //TODO : untangle this from validation code : we are talking transcoding here, not validation
    private void addPsiElementFromMapEntry(SquashMacroCallControlResult result, SquashDSLMacro temp, Map.Entry<SquashMacroMacroTitle, SquashDSLMacro> entry) {
        if (entry.getValue().equals(temp)) {
            SquashMacroMacroTitle psiEle = entry.getKey();
            if (psiEle != null) {
                result.getCustomMacroTitles().add(psiEle);
            }
        }
    }

    @NotNull
    public List<String> getMacroKeysFromTitleContent(SquashMacroMacroTitleContent macroTitleContent) {
        List <SquashMacroMacroTitleProperty> propertyList = macroTitleContent.getMacroTitlePropertyList();
        List<String> actualKeySet = new ArrayList<>();
        for (SquashMacroMacroTitleProperty property : propertyList) {
            actualKeySet.add(property.getTitleKey());
        }
        return actualKeySet;
    }

    @NotNull
    public Map<Integer, String> getMacroParamsFromTitleContent(SquashMacroMacroTitleContent macroTitleContent) {
        List <SquashMacroMacroTitleProperty> propertyList = macroTitleContent.getMacroTitlePropertyList();
        Map<Integer, String> actualSet = new HashMap<>();
        String firstParam = macroTitleContent.getTitleFirstParam();
        if (firstParam != null) {
		actualSet.put(0, firstParam);
	}
        int k=1;
        for (SquashMacroMacroTitleProperty property : propertyList) {
            String param = property.getTitleParam();
            if (param != null) {
                ++k;
                actualSet.put(k, param);
            }
        }
        return actualSet;
    }

    /**
     * TODO : this code does not "compare sets". Rename it (and most probably split it) after what it acutally does, ie :
     *  1 - check a given macro signature to see if it is what we're looking for
     *  2 - if it is, add it to a list
     *
     *
     * @param result
     * @param expectedKeySet
     * @param expectedParamSet
     * @param title
     * @param keySet
     * @param paramSet
     */
    public void compareSets(List<SquashMacroMacroTitle> result, List<String> expectedKeySet, Map<Integer, String> expectedParamSet, SquashMacroMacroTitle title, List<String> keySet, Map<Integer, String> paramSet) {
        int expectedKeySetLength = expectedKeySet.size();
        if (expectedKeySetLength==keySet.size() && expectedParamSet.size()==paramSet.size()) {
            //control first title param, if any
            boolean actualHasFirstParam = paramSet.containsKey(0);
            boolean expectedHasFirstParam = expectedParamSet.containsKey(0);
            if (((actualHasFirstParam && expectedHasFirstParam)
                    || (!actualHasFirstParam && !expectedHasFirstParam))
                && (CollectionToolkit.compareKeySet(expectedKeySet, keySet)==expectedKeySetLength)){
                    result.add(title);
            }
        }
    }

    /**
     * TODO : could probably be made plainer using regex. Let's give ourselves time to think, though.
     * @param param
     * @return
     */
    private String removeCurlyBracket(String param) {
        String result;
        if(param != null && !param.isEmpty()){
            int openingAccoladeIndex = param.indexOf("{");
            int closingAccoladeIndex = param.indexOf("}");
            if (openingAccoladeIndex > -1 && closingAccoladeIndex > openingAccoladeIndex){
                result = param.substring(openingAccoladeIndex+1, closingAccoladeIndex);
                return result;
            }
        }
        return param;
    }
}
