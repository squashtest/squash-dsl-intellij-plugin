/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.language;

import com.intellij.openapi.fileTypes.LanguageFileType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.squashtest.ta.intellij.plugin.file.general.SquashTestLanguage;

import javax.swing.Icon;


public class SquashTestFileType extends LanguageFileType {

    public static final SquashTestFileType INSTANCE = new SquashTestFileType();

    private SquashTestFileType() {
        super(SquashTestLanguage.TA_FILE_INSTANCE);
    }

    @NotNull
    @Override
    public String getName() {
        return "Squash Test File";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Squash TF test file";
    }

    @NotNull
    @Override
    public String getDefaultExtension() {
        return "ta";
    }

    @Nullable
    @Override
    public Icon getIcon() {
        return SquashTestIcons.FILE;
    }
}
