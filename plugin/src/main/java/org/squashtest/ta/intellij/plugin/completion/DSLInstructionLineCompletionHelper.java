/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.completion;

import com.intellij.codeInsight.completion.CompletionResultSet;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.psi.PsiElement;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLAssertion;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLCommand;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLComponentRegistry;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLConverter;
import org.squashtest.ta.galaxia.metaexecution.enginelink.EngineLinkException;
import org.squashtest.ta.intellij.plugin.fwconnector.IdeaFrameworkConnector;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeoutException;

/**
 * This helper class provides general DSL Instruction line completion logic.
 * @implNote anything previously called "Command line" in fact refers to DSL <strong>instructions</strong> and has been renamed accordingly
 */
public class DSLInstructionLineCompletionHelper extends CompletionHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(DSLInstructionLineCompletionHelper.class);

    private static final String[] DSL_HEADKEY_ARRAY= new String[]{"DEFINE", "LOAD", "ASSERT", "VERIFY", "CONVERT", "EXECUTE"};

    private static final String[] DSL_ALL_KEY_ARRAY= new String[]{"DEFINE", "AS", "LOAD", "FROM", "ASSERT", "VERIFY", "IS", "HAS", "DOES", "THAN", "THE", "CONVERT", "TO", "USING", "EXECUTE", "WITH", "ON"};

    private final IdeaFrameworkConnector connector;

    public DSLInstructionLineCompletionHelper(IdeaFrameworkConnector connector) {
            this.connector=connector;
    }

    /**
     * This method is to propose the autocompletion for all built-in converter values
     * @param result the IntelliJ object control the autocompletion process
     */
    void converterInstructionLineCompletion(CompletionResultSet result) throws EngineLinkException {
        try{
            SquashDSLComponentRegistry macroJsonFileContent = connector.getSquashDSLComponentRegistry();
            List<SquashDSLConverter> builtInConverterList = macroJsonFileContent.getConverters();
            for (SquashDSLConverter converter : builtInConverterList) {
                result.addElement(LookupElementBuilder.create(converter.getOutputResource()+"("+converter.getNature()+") ").withCaseSensitivity(true));
            }
        }catch(TimeoutException e){
            LOGGER.warn(CompletionHelper.TIMEOUT_EXCEPTION_MSG,e);
        }
    }

    /**
     * This method is to propose the autocompletion for all built-in assertion values
     * @param result the IntelliJ object control the autocompletion process
     */
    void cmdLineAssertionCompletion(CompletionResultSet result) throws EngineLinkException {
        try{
            SquashDSLComponentRegistry macroJsonFileContent = connector.getSquashDSLComponentRegistry();
            List<SquashDSLAssertion> builtInAssertionList = macroJsonFileContent.getAssertions();
            for (SquashDSLAssertion assertion : builtInAssertionList) {
                result.addElement(LookupElementBuilder.create(assertion.getNature()+" ").withCaseSensitivity(true));
            }
        }catch(TimeoutException e){
            LOGGER.warn(CompletionHelper.TIMEOUT_EXCEPTION_MSG,e);
        }
    }

    /**
     * This method is to propose the autocompletion for all built-in command values
     * @param result the IntelliJ object control the autocompletion process
     */
    void cmdLineExecuteCommandCompletion(CompletionResultSet result) throws EngineLinkException {
        try{
            SquashDSLComponentRegistry macroJsonFileContent = connector.getSquashDSLComponentRegistry();
            List<SquashDSLCommand> builtInCommandList = macroJsonFileContent.getCommands();
            for (SquashDSLCommand command : builtInCommandList) {
                result.addElement(LookupElementBuilder.create(command.getNature()+" ").withCaseSensitivity(true));
            }
        }catch(TimeoutException e){
            LOGGER.warn(TIMEOUT_EXCEPTION_MSG,e);
        }
    }

    /**
     * This method is to provide Command Line Autocompletion for the TA Property
     * @param result the autocompletion controller
     * @param headKey the Head key whose line is demanded for CMD_KEY autocompletion
     * @param propertyKeys the string list of the current line
     * @param needASpace if we need insert a space before the auto-proposal
     */
    private void commandLineKeyCompletion(CompletionResultSet result, String headKey, List<String> propertyKeys, boolean needASpace) {
        String newHeadKey = headKey.toUpperCase();
        if (Arrays.asList(DSL_HEADKEY_ARRAY).contains(newHeadKey)){
            SquashPropertyTemplate[] autoResults = SquashDSLInstructionTemplate.valueOf(newHeadKey).getProperties();
            for (SquashPropertyTemplate propertyTemplate : autoResults){
                proposeInstructionKeyList(result, propertyKeys, propertyTemplate, needASpace);
            }

            //propose separately USING
            if (!propertyKeys.contains("USING") && !"DEFINE".equals(newHeadKey) && !"LOAD".equals(newHeadKey)){
                if (needASpace){
                    result.addElement(LookupElementBuilder.create(" USING ").withCaseSensitivity(false));
                } else {
                    result.addElement(LookupElementBuilder.create("USING ").withCaseSensitivity(false));
                }
            }
        }
    }

    private void proposeInstructionKeyList(CompletionResultSet result, List<String> propertyKeys, SquashPropertyTemplate propertyTemplate, boolean needASpace) {
        String propertyTemplateKey = propertyTemplate.getKey().toUpperCase();
        if (!propertyKeys.contains(propertyTemplateKey)){
            if (needASpace){
                result.addElement(LookupElementBuilder.create(" "+propertyTemplateKey +" ").withCaseSensitivity(false));
                if ("DOES".equals(propertyTemplateKey)) {
                    result.addElement(LookupElementBuilder.create(" HAS ").withCaseSensitivity(false));
                    result.addElement(LookupElementBuilder.create(" IS ").withCaseSensitivity(false));
                } else if ("THE".equals(propertyTemplateKey)) {
                    result.addElement(LookupElementBuilder.create(" THAN ").withCaseSensitivity(false));
                    result.addElement(LookupElementBuilder.create(" WITH ").withCaseSensitivity(false));
                }
            } else {
                result.addElement(LookupElementBuilder.create(propertyTemplateKey +" ").withCaseSensitivity(false));
                if ("DOES".equals(propertyTemplateKey)) {
                    result.addElement(LookupElementBuilder.create("HAS ").withCaseSensitivity(false));
                    result.addElement(LookupElementBuilder.create("IS ").withCaseSensitivity(false));
                } else if ("THE".equals(propertyTemplateKey)) {
                    result.addElement(LookupElementBuilder.create("THAN ").withCaseSensitivity(false));
                    result.addElement(LookupElementBuilder.create("WITH ").withCaseSensitivity(false));
                }
            }

        }
    }

    /**
     * When we have some keywords in a list, we add others of the same category.
     * TODO : (edegenetais) : see what this is for and check the impact in having multiple instances of the same string in the final list.
     * @param keyList list of DSL keywords
     */
    private void addSupKey(List<String> keyList) {
        if (keyList.contains("IS") || keyList.contains("HAS") || keyList.contains("DOES")) {
            keyList.add("IS");
            keyList.add("HAS");
            keyList.add("DOES");
        }

        if (keyList.contains("WITH") || keyList.contains("THAN") || keyList.contains("THE")) {
            keyList.add("WITH");
            keyList.add("THAN");
            keyList.add("THE");
        }
    }

    /**
     * This method is to propose the keyword list in a command line starting with the given KEYWORD
     * @param result the IntelliJ object control the autocompletion process
     * @param position the AC position
     * @param fileContent the file content text
     */
    void commandLineKeyCompletion(CompletionResultSet result, PsiElement position, String fileContent) {
        List<String> dslAllKeyList = Arrays.asList(DSL_ALL_KEY_ARRAY);
        boolean needASpace = false;

        int autoCompletePos = position.getNode().getStartOffset();
        //get all the text from line start to the auto-completion position
        String beforeACBackToEndLine = getInlineTextBeforeACPosition(fileContent, autoCompletePos);

        if (beforeACBackToEndLine.endsWith(")")){
            needASpace = true;
        }
        //get the instruction headkey
        String headKey = getInstructionHeadKey(beforeACBackToEndLine).toUpperCase();

        //before split the string line by space,
        //firstly we replace all inline-declared values (which can contain spaces) in other KEYWORD with symbol "#"
        String replaceInlineDeclaredValueWithSymbol = replaceInlineDeclaredValueWithSymbol(beforeACBackToEndLine);

        //then we need to replace the USING input list by the "#" symbol
        String replaceUsingInputListWithSymbol = replaceUsingClauseWithSymbol(replaceInlineDeclaredValueWithSymbol);

        List<String> beforeACBackToEndLineNoEmptyElementList = getElementListFromString(replaceUsingInputListWithSymbol);
        List<String> keyList = new ArrayList<>();
        for (int i=0; i < beforeACBackToEndLineNoEmptyElementList.size(); i= i+2){
            String word = beforeACBackToEndLineNoEmptyElementList.get(i).toUpperCase();
            if (dslAllKeyList.contains(word)) {
                keyList.add(word);
            }
        }

        addSupKey(keyList);

        String afterACUpToEndLine = getInlineTextAfterACPosition(fileContent, autoCompletePos);

        //before split the string line by space,
        //firstly we replace all inline-declared values (which can contain spaces) in other KEYWORD with symbol "#"
        replaceInlineDeclaredValueWithSymbol = replaceInlineDeclaredValueWithSymbol(afterACUpToEndLine);

        //then we need to replace the USING input list by the "#" symbol
        replaceUsingInputListWithSymbol = replaceUsingClauseWithSymbolWithHeadKey(headKey, replaceInlineDeclaredValueWithSymbol);

        List<String> afterACUpToEndLineNoEmptyList = getElementListFromString(replaceUsingInputListWithSymbol);
        for (int i=0; i < afterACUpToEndLineNoEmptyList.size(); i= i+2){
            String word = afterACUpToEndLineNoEmptyList.get(i).toUpperCase();
            if (dslAllKeyList.contains(word)) {
                keyList.add(word);
            } else if (!hasAtLeastAKeywordStartingWithThisWord(dslAllKeyList, word)){
                return;
            }
        }

        addSupKey(keyList);

        commandLineKeyCompletion(result, headKey, keyList, needASpace);
    }

    private boolean hasAtLeastAKeywordStartingWithThisWord(List<String> dslAllKeyList, String word) {
        for (String keyword: dslAllKeyList){
            if (keyword.startsWith(word)) {
                return true;
            }
        }
        return false;
    }

    private String replaceUsingClauseWithSymbolWithHeadKey(String headKey, String str) {
        if (str.toUpperCase().contains(" USING ")){
            int usingIndex = str.toUpperCase().indexOf("USING");
            String beforeUsingStr = str.substring(0, usingIndex+5);
            String afterUsingStr = str.substring(usingIndex+5);

            String[] keyArray = getKeyArrayFromHeadKey(headKey);
            String afterUsingStrToUpperCase = afterUsingStr.toUpperCase();
            int firstKeyIndex = getFirstKeyIndex(afterUsingStrToUpperCase, keyArray);
            if (firstKeyIndex != -1) {
                String modifiedAfterUsingStr = replaceIndexedZoneWithSymbol(afterUsingStr, 0, firstKeyIndex);
                return beforeUsingStr+" "+modifiedAfterUsingStr;
            } else {
                return beforeUsingStr+" #";
            }
        }
        return str;
    }

    private String getInstructionHeadKey(String beforeACBackToEndLine) {
        String[] beforeACBackToEndLineArray = beforeACBackToEndLine.trim().split(" ");
        return beforeACBackToEndLineArray[0];
    }

    @NotNull
    private List<String> getElementListFromString(String inputStr) {
        //now we split the string by space " "
        String[] inputStrArray = inputStr.split(" ");
        //take only non-empty elements
        List<String> beforeACBackToEndLineNoEmptyElementList = new ArrayList<>();
        for (String element : inputStrArray) {
            if (!"".equals(element)) {
                beforeACBackToEndLineNoEmptyElementList.add(element);
            }
        }
        return beforeACBackToEndLineNoEmptyElementList;
    }

    /**
     * This method is to replace all inline-declared values (which can contain spaces) with symbol "#"
     * @param inputStr input string
     * @return modified string
     */
    private String replaceInlineDeclaredValueWithSymbol(String inputStr) {
        int i = 0;
        int count = -1;
        boolean startPoint = false;
        int startIndex = -1;

        while (i < inputStr.length()) {
            String currentChar = String.valueOf(inputStr.charAt(i));
            if (!startPoint && "$".equals(currentChar) && i+1 < inputStr.length()){
                String nextChar = String.valueOf(inputStr.charAt(i+1));
                if ("(".equals(nextChar)){
                    startPoint = true;
                    count = 1;
                    startIndex = i;
                    ++i;
                }
            } else if ("(".equals(currentChar)){
                ++count;
            } else if (")".equals(currentChar)){
                --count;
            }
            if (count == 0 && startIndex != -1){
                int endIndex = i+1;
                inputStr = replaceIndexedZoneWithSymbol(inputStr, startIndex, endIndex);
                i = startIndex;
                count = -1;
                startIndex = -1;
            }
            ++i;
        }
        return inputStr;
    }

    private String replaceIndexedZoneWithSymbol(String inputStr, int startIndex, int endIndex) {
        CharSequence target = inputStr.subSequence(startIndex, endIndex);
        CharSequence replacement = "# ";
        return inputStr.replace(target, replacement);
    }

    private String replaceUsingClauseWithSymbol(String str) {
        if (str.toUpperCase().contains(" USING ")){
            int usingIndex = str.toUpperCase().indexOf("USING");
            String beforeUsingStr = str.substring(0, usingIndex+5);
            String afterUsingStr = str.substring(usingIndex+5);
            String[] headStrArray = beforeUsingStr.trim().split(" ");
            String headKey = headStrArray[0];
            String[] keyArray = getKeyArrayFromHeadKey(headKey);
            String afterUsingStrToUpperCase = afterUsingStr.toUpperCase();
            int firstKeyIndex = getFirstKeyIndex(afterUsingStrToUpperCase, keyArray);
            if (firstKeyIndex != -1) {
                String modifiedAfterUsingStr = replaceIndexedZoneWithSymbol(afterUsingStr, 0, firstKeyIndex);
                return beforeUsingStr+" "+modifiedAfterUsingStr;
            } else {
                return beforeUsingStr+" #";
            }
        }
        return str;
    }

    private int getFirstKeyIndex(String inputStr, String[] keyArray) {
        List<String>keyList = Arrays.asList(keyArray);
        String[] tempArray = inputStr.split(" ");
        int i = 0;
        while (i<tempArray.length) {
            String mot = tempArray[i];
            if (!"".equals(mot) && keyList.contains(mot)){
                return inputStr.indexOf(" "+mot);
            }
            ++i;
        }
        return -1;
    }

    private String[] getKeyArrayFromHeadKey(String headKey) {
        switch (headKey.trim().toUpperCase()){
            case "DEFINE":
                return new String[]{"AS"};
            case "LOAD":
                return new String[]{"AS","FROM"};
            case "CONVERT":
                return new String[]{"TO","AS","USING"};
            case "EXECUTE":
                return new String[]{"WITH","ON","AS","USING"};
            case "ASSERT":
            case "VERIFY":
                return new String[]{"IS","HAS","DOES","WITH","THAN","THE","USING"};
            default:
                return new String[0];
        }
    }

    /**
     * This method is to propose an instruction clause value model
     * @param result the IntelliJ object control the autocompletion process
     * @param position the AC position
     * @param fileContent the content of the current file
     */
    void cmdLinePropValueCompletion(CompletionResultSet result, PsiElement position, String fileContent) {
        int autoCompletePos = position.getNode().getStartOffset();
        //get all the text from line start to the auto-completion position
        String beforeACBackToEndLine = getInlineTextBeforeACPosition(fileContent, autoCompletePos);
        //before split the string line by space,
        //firstly we need to replace the USING input list by the "#" symbol
        String replaceUsingInputListWithSymbol = replaceUsingClauseWithSymbol(beforeACBackToEndLine);
        //then we replace all inline-declared values (which can contain spaces) in other KEYWORD with symbol "#"
        String replaceInlineDeclaredValueWithSymbol = replaceInlineDeclaredValueWithSymbol(replaceUsingInputListWithSymbol);


        List<String> beforeACBackToEndLineNoEmptyElementList = getElementListFromString(replaceInlineDeclaredValueWithSymbol);
        String headKey = beforeACBackToEndLineNoEmptyElementList.get(0).toUpperCase();

        String[] keyArray = getKeyArrayFromHeadKey(headKey);

        String currentKey = getLastKeyInLine(replaceInlineDeclaredValueWithSymbol, keyArray);

        if (currentKey != null) {
            //for this version, we propose nothing but just a tooltip
            result.addElement(LookupElementBuilder.create("").appendTailText("Please insert a resource identifier",false));
            //TODO: or we propose the suitable Value variable list : "next versions..."
        }
    }

    private String getLastKeyInLine(String inputStr, String[] keyArray) {
        List<String>keyList = Arrays.asList(keyArray);
        String[] tempArray = inputStr.split(" ");
        int arrayLength = tempArray.length;
        int i = arrayLength-1;
        while (i>=0) {
            String mot = tempArray[i].trim().toUpperCase();
            if (!"".equals(mot) && keyList.contains(mot)){
                return mot;
            }
            --i;
        }
        return null;
    }

    /**
     * This method is to propose a command line without the given KEYWORD
     * @param result the IntelliJ object control the autocompletion process
     * @param position the AC position
     * @param fileContent the file content text
     */
    void cmdLineHeadPropValueCompletion(CompletionResultSet result, PsiElement position, String fileContent) {
        int autoCompletePos = position.getNode().getStartOffset();
        String beforeACBackToEndLine = getInlineTextBeforeACPosition(fileContent, autoCompletePos);
        String headkey = beforeACBackToEndLine.trim().toUpperCase();
        if ("LOAD".equals(headkey)){
            result.addElement(LookupElementBuilder.create("").appendTailText("Please insert a resource path",true));
        } else {
            result.addElement(LookupElementBuilder.create("").appendTailText("Please insert a resource identifier",false));
        }
        //TODO: we propose the suitable Value variable list : "next version"
    }

    void proposeInstructionHeadKeyWithItTemplate(String currentText, CompletionResultSet result) {
        String writingText = currentText.trim().toUpperCase();
        for (String headKey : DSL_HEADKEY_ARRAY) {
            if (headKey.startsWith(writingText) && !writingText.contains(" ")){
                proposeInstructionTemplateOfHeadkey(result, headKey);
            }
        }
    }

    private void proposeInstructionTemplateOfHeadkey(CompletionResultSet result, String headKey) {
        switch (headKey){
            case "DEFINE":
                result.addElement(LookupElementBuilder.create("DEFINE ").withCaseSensitivity(false));
                result.addElement(LookupElementBuilder.create(SquashDSLInstructionTemplate.DEFINE.toString()).withCaseSensitivity(false));
                break;
            case "LOAD":
                result.addElement(LookupElementBuilder.create("LOAD ").withCaseSensitivity(false));
                result.addElement(LookupElementBuilder.create(SquashDSLInstructionTemplate.LOAD.toString()).withCaseSensitivity(false));
                result.addElement(LookupElementBuilder.create(SquashDSLInstructionTemplate.LOAD_FROM.toString()).withCaseSensitivity(false));
                break;
            case "CONVERT":
                result.addElement(LookupElementBuilder.create("CONVERT ").withCaseSensitivity(false));
                result.addElement(LookupElementBuilder.create(SquashDSLInstructionTemplate.CONVERT.toString()).withCaseSensitivity(false));
                break;
            case "EXECUTE":
                result.addElement(LookupElementBuilder.create("EXECUTE ").withCaseSensitivity(false));
                result.addElement(LookupElementBuilder.create(SquashDSLInstructionTemplate.EXECUTE.toString()).withCaseSensitivity(false));
                break;
            case "ASSERT":
                addAssertInstructionLineCompletionProposals(result);
                break;
            case "VERIFY":
                addVerifyDSLInstructionLineCompletionProposals(result);
                break;
            default:
                LOGGER.debug("{} is not a known DSL instruction, so there is no DSL instruction line completion proposal for it.");
        }
    }

    private void addVerifyDSLInstructionLineCompletionProposals(CompletionResultSet result) {
        result.addElement(LookupElementBuilder.create("VERIFY ").withCaseSensitivity(false));
        result.addElement(LookupElementBuilder.create(SquashDSLInstructionTemplate.VERIFY.toString()).withCaseSensitivity(false));
        result.addElement(LookupElementBuilder.create(SquashDSLInstructionTemplate.VERIFY_IS_BINARY.toString()).withCaseSensitivity(false));
        result.addElement(LookupElementBuilder.create(SquashDSLInstructionTemplate.VERIFY_DOES_UNARY.toString()).withCaseSensitivity(false));
        result.addElement(LookupElementBuilder.create(SquashDSLInstructionTemplate.VERIFY_IS_UNARY.toString()).withCaseSensitivity(false));
        result.addElement(LookupElementBuilder.create(SquashDSLInstructionTemplate.VERIFY_HAS_UNARY.toString()).withCaseSensitivity(false));
    }

    private void addAssertInstructionLineCompletionProposals(CompletionResultSet result) {
        result.addElement(LookupElementBuilder.create("ASSERT ").withCaseSensitivity(false));
        result.addElement(LookupElementBuilder.create(SquashDSLInstructionTemplate.ASSERT.toString()).withCaseSensitivity(false));
        result.addElement(LookupElementBuilder.create(SquashDSLInstructionTemplate.ASSERT_IS_BINARY.toString()).withCaseSensitivity(false));
        result.addElement(LookupElementBuilder.create(SquashDSLInstructionTemplate.ASSERT_DOES_UNARY.toString()).withCaseSensitivity(false));
        result.addElement(LookupElementBuilder.create(SquashDSLInstructionTemplate.ASSERT_IS_UNARY.toString()).withCaseSensitivity(false));
        result.addElement(LookupElementBuilder.create(SquashDSLInstructionTemplate.ASSERT_HAS_UNARY.toString()).withCaseSensitivity(false));
    }
}
