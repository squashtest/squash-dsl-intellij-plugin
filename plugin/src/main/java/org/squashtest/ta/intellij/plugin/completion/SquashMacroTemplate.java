/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.completion;

import org.squashtest.ta.intellij.plugin.projectmodel.Source;

public class SquashMacroTemplate {
    private Source source;
    private String name;

    public SquashMacroTemplate(Source source, String name) {
        this.source = source;
        this.name = name;
    }

    public Source getSource() {
        return source;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "SquashMacroTemplate{" +
                "source='" + source + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
