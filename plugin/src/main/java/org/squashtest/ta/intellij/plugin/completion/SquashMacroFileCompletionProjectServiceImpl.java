/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.completion;


import com.intellij.codeInsight.completion.CompletionParameters;
import com.intellij.codeInsight.completion.CompletionResultSet;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.galaxia.metaexecution.enginelink.EngineLinkException;
import org.squashtest.ta.intellij.plugin.tools.LineSearchingResult;
import org.squashtest.ta.intellij.plugin.tools.StringToolkit;

import java.util.HashMap;
import java.util.Map;

public class SquashMacroFileCompletionProjectServiceImpl extends SquashCompletionProjectServiceImpl implements SquashMacroFileCompletionProjectService {
    private static final String MACRO_SYMBOL =  "# ";
    private static final String SEPARATOR =  "=>";

    private static final String MACRO_TITLE_TEMPLATE =  "# KEY1 {param1} KEY 2 {param2} KEY_3";
    private static final String MACRO_TITLE_SEPARATOR_TEMPLATE =  "# KEY1 {param1} KEY 2 {param2} KEY_3\n=>\n";

    private static final String MACRO_LINE_ENDING = "[^\n\r\f]*\n?";
    private static final String MACRO_SEPARATOR_ENDING = "[ \t]*\n?";
    private static final String CMD_LINE_ENDING = "[^\n\r\f]*\n?";
    private static final String CHAR = "[^ \t\n\r\f//\"=>\"]";

    private static final String FIRST_MACRO_LINE = "firstMacroLine";
    private static final String SECOND_MACRO_LINE ="secondMacroLine";
    private static final String SEPARATOR_LINE =  "separator";
    private static final String FIRST_CMD_LINE ="firstCommandLine";
    private static final String SECOND_CMD_LINE ="secondCommandLine";

    private static final Logger LOGGER = LoggerFactory.getLogger(SquashMacroFileCompletionProjectServiceImpl.class);

    private final DSLInstructionLineCompletionHelper dslInstructionLineCompletionHelper;

    private String beforeACBackToEndLine;

    private String afterACUpToEndLine;

    public SquashMacroFileCompletionProjectServiceImpl(MacroCallLineCompletionHelper macroCallLineCompletionHelper, DSLInstructionLineCompletionHelper dslInstructionLineCompletionHelper) {
        super(macroCallLineCompletionHelper);
        this.dslInstructionLineCompletionHelper=dslInstructionLineCompletionHelper;
    }

    @Override
    public void addCompletion(CompletionParameters parameters, CompletionResultSet result) {
        //the current cursor position
        PsiElement position = parameters.getPosition();

        PsiFile taTestFile = parameters.getOriginalFile();
        String filePath = taTestFile.getVirtualFile().getPath();
        //the whole file text content
        String fileContent = parameters.getEditor().getDocument().getText();

        LineSearchingResult firstMacroLine = StringToolkit.getIthIndexes(fileContent, MACRO_SYMBOL, MACRO_LINE_ENDING, 1);
        LineSearchingResult secondMacroLine = StringToolkit.getIthIndexes(fileContent, MACRO_SYMBOL, MACRO_LINE_ENDING,2);

        LineSearchingResult firstMacroSeparator = StringToolkit.getIthIndexes(fileContent, SEPARATOR, MACRO_SEPARATOR_ENDING, 1);
        LineSearchingResult secondMacroSeparator = StringToolkit.getIthIndexes(fileContent, SEPARATOR, MACRO_SEPARATOR_ENDING, 2);

        LineSearchingResult firstCommandLine = StringToolkit.getIthIndexes(fileContent, CHAR, CMD_LINE_ENDING,1);
        LineSearchingResult secondCommandLine = StringToolkit.getIthIndexes(fileContent, CHAR, CMD_LINE_ENDING,2);

        //if the macro file content has (at least) a line starting with "=>"
        //that contains any other non-space char(s) like: "=> abc123   "
        //NO autocompletion will be provided!
        if ((StringToolkit.getIthIndexes(fileContent,SEPARATOR, "([ \t]*[^ \t\n\r\f]+[ \t]*)+\n", 1).notFound())
            //if there are more than ONE macro separator, NO autocompletion will be provided!
            && (secondMacroSeparator.notFound())) {
            Map<String, LineSearchingResult> fileProperties = new HashMap<>();
            fileProperties.put(FIRST_MACRO_LINE, firstMacroLine);
            fileProperties.put(SECOND_MACRO_LINE, secondMacroLine);
            fileProperties.put(SEPARATOR_LINE, firstMacroSeparator);
            fileProperties.put(FIRST_CMD_LINE, firstCommandLine);
            fileProperties.put(SECOND_CMD_LINE, secondCommandLine);

            try {
                blockCompletion(position, fileContent, result, fileProperties);
            } catch (EngineLinkException e) {
                LOGGER.error("AUTO-COMPLETION: "+filePath +": We failed to retrieve the component registry handle: "+e);
            }
        }
    }

    private void blockCompletion(PsiElement position, String fileContent,
                                 CompletionResultSet result, Map<String, LineSearchingResult> fileProperties) throws EngineLinkException {
        int autoCompletePos = position.getNode().getStartOffset();

        beforeACBackToEndLine = dslInstructionLineCompletionHelper.getInlineTextBeforeACPosition(fileContent, autoCompletePos);
        afterACUpToEndLine = dslInstructionLineCompletionHelper.getInlineTextAfterACPosition(fileContent, autoCompletePos);

        LOGGER.debug("Autocompletion starting at position : " + autoCompletePos);
        LOGGER.debug("Content checking in current Squash Macro File.");

        if (fileProperties.get("firstMacroLine").notFound()){
            //there's no Macro symbol in the current file content
            noMacroSymbolAC(result, autoCompletePos, fileProperties);
        } else {
            //there's at least 1 # in the current file
            withMacroSymbolAC(position, fileContent, result, autoCompletePos, fileProperties);
        }
    }

    private void withMacroSymbolAC(PsiElement position, String fileContent, CompletionResultSet result, int autoCompletePos, Map<String, LineSearchingResult> fileProperties) throws EngineLinkException {
        if (fileProperties.get(SEPARATOR_LINE).notFound()) {
            //there's no Separator in the current file content
            withMacroSymbolNoSeparatorAC(result, autoCompletePos, fileProperties);
        } else {
            //there's at least 1 Separator in the current file
            withMacroSymbolWithSeparatorAC(position, fileContent, result, autoCompletePos, fileProperties);
        }
    }

    private void withMacroSymbolWithSeparatorAC(PsiElement position, String fileContent, CompletionResultSet result, int autoCompletePos, Map<String, LineSearchingResult> fileProperties) throws EngineLinkException {
        LineSearchingResult firstMacroSymbol = fileProperties.get(FIRST_MACRO_LINE);
        LineSearchingResult macroSeparator = fileProperties.get(SEPARATOR_LINE);
        LineSearchingResult firstCommandLine = fileProperties.get(FIRST_CMD_LINE);
        LineSearchingResult secondCommandLine = fileProperties.get(SECOND_CMD_LINE);

        if(firstCommandLine.wasFound() && firstMacroSymbol.before(firstCommandLine.getStartIndex()) && secondCommandLine.notFound()){
            //there's no other character in the current file content (comments excluded)
            withoutOtherChar(position, fileContent, result, autoCompletePos, fileProperties);
        } else if (firstCommandLine.wasFound() && firstMacroSymbol.startsBefore(firstCommandLine.getStartIndex())){
            //there's at least 1 other character AFTER the first macro symbol in the current file content (comments excluded)
            withOtherChars(position, fileContent, result, autoCompletePos, fileProperties);
        } else if (macroSeparator.wasFound() && firstCommandLine.wasFound() && macroSeparator.getStartIndex() < firstCommandLine.getStartIndex()
                    && autoCompletePos < firstCommandLine.getStartIndex()){
            //there's at least 1 other character BEFORE the first macro symbol in the current file content (comments excluded)
            lookupBuilderWithLog(result, MACRO_TITLE_TEMPLATE);
        }
    }

    private void withOtherChars(PsiElement position, String fileContent, CompletionResultSet result, int autoCompletePos, Map<String, LineSearchingResult> fileProperties) throws EngineLinkException {
        LineSearchingResult firstMacroSymbol = fileProperties.get(FIRST_MACRO_LINE);
        LineSearchingResult macroSeparator = fileProperties.get(SEPARATOR_LINE);
        LineSearchingResult secondCommandLine = fileProperties.get(SECOND_CMD_LINE);

        //we firstly check if, in the current macro file, there is at least a "=>" symbol
        if (macroSeparator.wasFound()){
            proposeForFileWithSignatureSeparator(firstMacroSymbol, macroSeparator, autoCompletePos, result, fileContent, position, secondCommandLine);
        } else {
            macroOrCommandLineCompletion(fileContent, position, result);
        }
    }

    private void proposeForFileWithSignatureSeparator(LineSearchingResult firstMacroSymbol, LineSearchingResult macroSeparator, int autoCompletePos, CompletionResultSet result, String fileContent, PsiElement position, LineSearchingResult secondCommandLine) throws EngineLinkException {
        //if there is also a "#" symbol
        if (firstMacroSymbol.wasFound()) {
            //then if the first "#" is written after the "=>" symbol ie. the file doesn't have a signature
            if (macroSeparator.before(firstMacroSymbol.getStartIndex())
                    //lastly, if the AC position is before the "=>"
                    && macroSeparator.after(autoCompletePos)){
                //so we propose a MACRO signature template
                lookupBuilderWithLog(result, MACRO_TITLE_TEMPLATE);
            }
            //if the first "#" is written before the "=>" symbol ie. the file has already a signature
            else if (macroSeparator.after(firstMacroSymbol.getEndIndex()) &&
                //lastly, if the AC position is before the "=>"
                 macroSeparator.before(autoCompletePos)){
                    macroOrCommandLineCompletion(fileContent, position, result);
            }
        }
        //so we have a "=>" but no "#" symbol, let's check if the "=>" is in the right place
        else if (beforeAnElementIfAny(macroSeparator.getEndIndex(), secondCommandLine) &
                //then check if the AC position is after the "=>"
                macroSeparator.before(autoCompletePos)){
            //so we propose macro and instruction templates
            macroOrCommandLineCompletion(fileContent, position, result);
        }
    }

    private void withoutOtherChar(PsiElement position, String fileContent, CompletionResultSet result, int autoCompletePos, Map<String, LineSearchingResult> fileProperties) throws EngineLinkException {
        LineSearchingResult firstMacroSymbol = fileProperties.get(FIRST_MACRO_LINE);
        LineSearchingResult macroSeparator = fileProperties.get(SEPARATOR_LINE);

        if (macroSeparator.wasFound() && firstMacroSymbol.wasFound()
                &&  macroSeparator.getStartIndex() < firstMacroSymbol.getStartIndex()
                && autoCompletePos < macroSeparator.getStartIndex()){
            lookupBuilderWithLog(result, MACRO_TITLE_TEMPLATE);
        } else if (macroSeparator.before(autoCompletePos)) {
            macroOrCommandLineCompletion(fileContent, position, result);
        }
    }

    private void withMacroSymbolNoSeparatorAC(CompletionResultSet result, int autoCompletePos, Map<String, LineSearchingResult> fileProperties) {
        LineSearchingResult firstMacroSymbol = fileProperties.get("firstMacroLine");
        LineSearchingResult secondMacroSymbol = fileProperties.get("secondMacroLine");
        LineSearchingResult firstCommandLine = fileProperties.get("firstCommandLine");
        LineSearchingResult secondCommandLine = fileProperties.get("secondCommandLine");

        if (firstMacroSymbol.wasFound() && firstCommandLine.after(firstMacroSymbol.getStartIndex())) {
            //there's no other character before the first Macro symbol in the current file content (comments excluded)
            if (autoCompletePos < firstMacroSymbol.getStartIndex()){
                lookupBuilderWithLog(result, MACRO_TITLE_SEPARATOR_TEMPLATE);
            }
            if (firstMacroSymbol.before(autoCompletePos)
                    && beforeAnElementIfAny(autoCompletePos, secondCommandLine)
                    && beforeAnElementIfAny(autoCompletePos, secondMacroSymbol)) {
                lookupBuilderWithLog(result, SEPARATOR+"\n");
            }
        } else if (firstCommandLine.wasFound() &&  autoCompletePos < firstCommandLine.getStartIndex()){
            //there's at least 1 other character before the first Macro symbol in the current file content (comments excluded)
            lookupBuilderWithLog(result, MACRO_TITLE_SEPARATOR_TEMPLATE);
        }
    }

    private boolean beforeAnElementIfAny(int currentPosition, LineSearchingResult element) {
        return element.notFound() || element.after(currentPosition);
    }

    private void noMacroSymbolAC(CompletionResultSet result, int autoCompletePos, Map<String, LineSearchingResult> fileProperties) {
        LineSearchingResult macroSeparator = fileProperties.get("separator");
        LineSearchingResult firstCommandLine = fileProperties.get("firstCommandLine");

        if (macroSeparator.notFound()) {
            //there's no Separator in the current file content
            noMacroSymbolNoSeparatorAC(result, autoCompletePos, firstCommandLine);
        } else {
            //there's at least 1 Separator in the current file
            noMacroSymbolWithSeparatorAC(result, autoCompletePos, macroSeparator, firstCommandLine);
        }
    }

    private void noMacroSymbolWithSeparatorAC(CompletionResultSet result, int autoCompletePos, LineSearchingResult macroSeparatorSearch, LineSearchingResult firstCommandLineSearch) {
        if (firstCommandLineSearch.notFound()) {
            //there's no other character in the current file content (comments excluded)
            if (macroSeparatorSearch.wasFound() && autoCompletePos < macroSeparatorSearch.getStartIndex()) {
                lookupBuilderWithLog(result, MACRO_TITLE_TEMPLATE);
            }
        } else {
            //there's at least 1 other character in the current file content (comments excluded)
            if (macroSeparatorSearch.wasFound() && firstCommandLineSearch.after(macroSeparatorSearch.getEndIndex())
                    && autoCompletePos < macroSeparatorSearch.getStartIndex()) {
                lookupBuilderWithLog(result, MACRO_TITLE_TEMPLATE);
            }
        }
    }

    private void noMacroSymbolNoSeparatorAC(CompletionResultSet result, int autoCompletePos, LineSearchingResult firstCommandlineSearch) {
        if (firstCommandlineSearch.notFound() || autoCompletePos < firstCommandlineSearch.getStartIndex()) {
            //there's no other character in the current file content (comments excluded)
            lookupBuilderWithLog(result, MACRO_TITLE_SEPARATOR_TEMPLATE);
        }
    }

    private void lookupBuilderWithLog(CompletionResultSet result, String str) {
        LOGGER.debug(str+" autocompletion proposed.");
        result.addElement(LookupElementBuilder.create(str).withCaseSensitivity(false));
    }

    private void macroOrCommandLineCompletion(String fileContent, PsiElement position, CompletionResultSet result) throws EngineLinkException {
        ASTNode currPos = position.getNode();
        String currPosType = currPos.getElementType().toString();
        PsiElement currPosParent = currPos.getPsi().getParent();
        String currPosParentType = currPosParent.getNode().getElementType().toString();

        LOGGER.debug("Accepting position parent type : {}",currPosParentType);
        switch (currPosType) {
            case "SquashMacroTokenType.AC_POSITION":
            case "SquashMacroTokenType.CMD_KEY" :
                acPositionAC(fileContent, position, result, currPosParentType);
                break;
            case "SquashMacroTokenType.CRLF":
            case "WHITE_SPACE":
                dslInstructionLineCompletionHelper.commandLineKeyCompletion(result, position, fileContent);
                break;
            case "SquashMacroTokenType.VALUE":
                valueAC(fileContent, position, result, currPosParentType);
                break;
            case "SquashMacroTokenType.CONVERTER":
                converterAC(result);
                break;
            case "SquashMacroTokenType.EXECUTE_CMD":
                executeAC(result);
                break;
            case "SquashMacroTokenType.ASSERTION_VALIDATOR":
                assertionAC(result);
                break;
            case "SquashMacroTokenType.MACRO_LINE_CONTENT":
                macroLineContentAC(result, currPosParent);
                break;
            default:
                break;
        }
    }

    private void assertionAC(CompletionResultSet result) throws EngineLinkException {
        LOGGER.debug("Starting Squash Command line Command autocompletion");
        dslInstructionLineCompletionHelper.cmdLineAssertionCompletion(result);
    }

    private void executeAC(CompletionResultSet result) throws EngineLinkException {
        LOGGER.debug("Starting Squash Command line Command autocompletion");
        dslInstructionLineCompletionHelper.cmdLineExecuteCommandCompletion(result);
    }

    private void converterAC(CompletionResultSet result) throws EngineLinkException {
        LOGGER.debug("Starting Squash Command line Converter autocompletion");
        dslInstructionLineCompletionHelper.converterInstructionLineCompletion(result);
    }

    private void valueAC(String fileContent, PsiElement position, CompletionResultSet result, String currPosParentType) {
        if ("CMD_HEAD_PROPERTY_VALUE".equals(currPosParentType)) {
            LOGGER.debug("Squash Command line Head Property Value autocompletion");
            dslInstructionLineCompletionHelper.cmdLineHeadPropValueCompletion(result, position, fileContent);
        }
        if ("CMD_PROPERTY_VALUE".equals(currPosParentType) || "VALUE_ELEMENT".equals(currPosParentType)) {
            LOGGER.debug("Squash Command line ONE template autocompletion");
            dslInstructionLineCompletionHelper.cmdLinePropValueCompletion(result, position, fileContent);
        }
    }

    private void acPositionAC(String fileContent, PsiElement position, CompletionResultSet result, String currPosParentType) {
        if ("CMD_HEAD_PROPERTY_KEY".equals(currPosParentType)) {
            String lineContentBeforeAC = beforeACBackToEndLine.trim();
            String lineContentAfterAC = afterACUpToEndLine.trim();
            if ("#".equals(lineContentAfterAC)) {
                LOGGER.debug("Insert a space after the # symbol");
                result.addElement(LookupElementBuilder.create(" "));
            } else if (lineContentAfterAC.isEmpty()) {
                if (lineContentBeforeAC.isEmpty()){
                    LOGGER.debug("Squash instruction line ALL templates autocompletion");
                    dslInstructionLineCompletionHelper.allCommandLineTemplatesCompletion(result);
                }
            } else {
                LOGGER.debug("Squash instruction line ONE template autocompletion");
                dslInstructionLineCompletionHelper.proposeInstructionHeadKeyWithItTemplate(lineContentAfterAC, result);
            }
        } else if ("CMD_PROPERTY_KEY".equals(currPosParentType)) {
            LOGGER.debug("Squash instruction line key word autocompletion");
            dslInstructionLineCompletionHelper.commandLineKeyCompletion(result, position, fileContent);
        }
    }
}
