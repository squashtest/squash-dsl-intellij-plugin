/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.validation;

import com.intellij.codeInsight.daemon.RelatedItemLineMarkerInfo;
import com.intellij.codeInsight.navigation.NavigationGutterIconBuilder;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.util.PsiTreeUtil;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLComponentRegistry;
import org.squashtest.ta.galaxia.metaexecution.enginelink.EngineLinkException;
import org.squashtest.ta.intellij.plugin.fwconnector.IdeaFrameworkConnector;
import org.squashtest.ta.intellij.plugin.highlight.MacroHighlightsHelper;
import org.squashtest.ta.intellij.plugin.language.SquashMacroIcons;
import org.squashtest.ta.intellij.plugin.macro.psi.SquashMacroMacroLine;
import org.squashtest.ta.intellij.plugin.macro.psi.SquashMacroMacroTitle;
import org.squashtest.ta.intellij.plugin.macro.psi.SquashMacroMacroTitleContent;

import org.squashtest.ta.intellij.plugin.projectmodel.SquashMacroFileLocationProjectService;
import org.squashtest.ta.intellij.plugin.projectmodel.SquashMacroTitleControlResult;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeoutException;

public class SquashMacroLineMarkerProjectServiceImpl implements SquashMacroLineMarkerProjectService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SquashMacroLineMarkerProjectServiceImpl.class);

    private final MacroDefinitionValidationHelper macroDefinitionValidationHelper;
    private final SquashMacroFileLocationProjectService macroFileLocator;
    private final MacroCallValidationHelper macroCallValidationHelper;
    private final MacroHighlightsHelper macroHighlightsHelper;
    private final IdeaFrameworkConnector connector;


    public SquashMacroLineMarkerProjectServiceImpl(SquashMacroFileLocationProjectService macroFileLocator, MacroCallValidationHelper macroCallValidationHelper, MacroDefinitionValidationHelper macroDefinitionValidationHelper, MacroHighlightsHelper macroHighlightsHelper, IdeaFrameworkConnector connector){

        this.macroFileLocator=macroFileLocator;
        this.macroCallValidationHelper = macroCallValidationHelper;
        this.macroDefinitionValidationHelper = macroDefinitionValidationHelper;
        this.macroHighlightsHelper = macroHighlightsHelper;
        this.connector=connector;
    }

    @Override
    public void collectNavigationMarkers(@NotNull PsiElement element,
                                         @NotNull Collection<? super RelatedItemLineMarkerInfo> result) {

        PsiFile taMacroFile = element.getContainingFile();

        //file path checking
        if (macroFileLocator.isInShortCutsFolder(taMacroFile.getVirtualFile().getPath())){
            String elementType = element.getNode().getElementType().toString();
            try {
                //get all built-in content
                SquashDSLComponentRegistry macroJsonFileContent = connector.getSquashDSLComponentRegistry();
                //Macro Title Marker
                if ("MACRO_TITLE_CONTENT".equals(elementType)){
                    macroTitleMarkerProviding(element, result, macroJsonFileContent);
                }
                //Macro Line Marker
                else if ("MACRO_LINE".equals(elementType)){
                    macroLineMarkerProviding(element, result, macroJsonFileContent);
                }
            } catch (EngineLinkException e) {
                LOGGER.warn("LINE PROVIDER for {} : We failed to retrieve the component registry handle.", taMacroFile.getVirtualFile().getPath(),e);
            } catch(TimeoutException e){
                LOGGER.warn("Retrieving of Squash component registry is in progress. Please wait until it is completely loaded!",e);
            }
        }
    }

    private void macroLineMarkerProviding(@NotNull PsiElement element, @NotNull Collection<? super RelatedItemLineMarkerInfo> result, SquashDSLComponentRegistry macroJsonFileContent) {
        PsiElement macroSymbol = element.getFirstChild();
        SquashMacroMacroLine macroLine = PsiTreeUtil.getParentOfType(macroSymbol, SquashMacroMacroLine.class);
        if (macroLine != null) {
            String macroLineContent = macroLine.getMacroLineContent();
            SquashMacroCallControlResult macroControlResult = macroCallValidationHelper.checkMacroLineContent(macroLineContent, macroJsonFileContent);
            if (macroControlResult != null) {
                //custom Macro navigating
                customMacroNavigating(result, macroSymbol, macroControlResult);
                //built-in macro announcement
                builtInMacroMsg(result, macroSymbol, macroControlResult);
            }
        }
    }

    private void builtInMacroMsg(@NotNull Collection<? super RelatedItemLineMarkerInfo> result, PsiElement macroSymbol, SquashMacroCallControlResult macroControlResult) {
        int matchNumber = macroControlResult.getMatchNumber();
        List<SquashMacroMacroTitle> temp = macroControlResult.getCustomMacroTitles();
        if (temp != null){
            int matchCustom = (temp.isEmpty())? 0 : temp.size();
            int matchBuiltIn = matchNumber - matchCustom;

            if (matchBuiltIn>0) {
                NavigationGutterIconBuilder<PsiElement> builder = NavigationGutterIconBuilder.create(
                        SquashMacroIcons.FILE).setTarget(null).setTooltipText("This is a Built-in Macro.");
                result.add(builder.createLineMarkerInfo(macroSymbol));
            }
        }
    }

    private void customMacroNavigating(@NotNull Collection<? super RelatedItemLineMarkerInfo> result, PsiElement macroSymbol, SquashMacroCallControlResult macroControlResult) {
        List<SquashMacroMacroTitle> macroTitles = macroControlResult.getCustomMacroTitles();
        if(macroTitles != null && !macroTitles.isEmpty()) {
            NavigationGutterIconBuilder<PsiElement> builder = NavigationGutterIconBuilder.create(
                    SquashMacroIcons.FILE).setTargets(macroTitles).setTooltipText("Navigate to the project Squash Macro File");
            result.add(builder.createLineMarkerInfo(macroSymbol));
        }
    }

    private void macroTitleMarkerProviding(@NotNull PsiElement element, @NotNull Collection<? super RelatedItemLineMarkerInfo> result, SquashDSLComponentRegistry macroJsonFileContent) {
        PsiElement macroSymbol = element.getPrevSibling();
        PsiElement macroTitlePsiElement = element.getParent();
        SquashMacroMacroTitleContent macroTitleContent = PsiTreeUtil.getChildOfType(macroTitlePsiElement, SquashMacroMacroTitleContent.class);
        if (macroTitleContent != null) {
            SquashMacroTitleControlResult macroTitleCheckResult = macroDefinitionValidationHelper.isMacroTitleExisted(macroTitleContent, macroJsonFileContent);
            if (macroTitleCheckResult.isInCustom()){
                //custom Macro navigating
                customMacroNavigatingForTitle(result, macroSymbol, macroTitleContent);
            }
            if (macroTitleCheckResult.isInBuiltIn()) {
                //built-in macro announcement
                builtInMacroMsgForTitle(result, macroSymbol);
            }
        }
    }

    private void builtInMacroMsgForTitle(@NotNull Collection<? super RelatedItemLineMarkerInfo> result, PsiElement macroSymbol) {
        NavigationGutterIconBuilder<PsiElement> builder = NavigationGutterIconBuilder.create(
                SquashMacroIcons.FILE).setTarget(null).setTooltipText("A built-in Macro with the same signature already exists in the Squash framework");
        result.add(builder.createLineMarkerInfo(macroSymbol));
    }

    private void customMacroNavigatingForTitle(@NotNull Collection<? super RelatedItemLineMarkerInfo> result, PsiElement macroSymbol, SquashMacroMacroTitleContent macroTitleContent) {

        final List<SquashMacroMacroTitle> customMacroTitles = macroHighlightsHelper.getCustomMacroTitlesByTitleContent(macroTitleContent);
        if(!customMacroTitles.isEmpty()) {
            NavigationGutterIconBuilder<PsiElement> builder = NavigationGutterIconBuilder.create(
                    SquashMacroIcons.FILE).setTargets(customMacroTitles).setTooltipText("A Macro with the same signature already exists in the project");
            result.add(builder.createLineMarkerInfo(macroSymbol));
        }
    }
}
