/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.lexer;

import com.intellij.lexer.FlexLexer;
import com.intellij.psi.tree.IElementType;
import com.intellij.psi.TokenType;

import org.squashtest.ta.intellij.plugin.file.psi.SquashTestTypes;

%%

%{
    private String inlineText;
    private int count = 0;
    private boolean inUsingValueList;
    private boolean inlineInUsing;

    void append(char c){
        inlineText += c;
    }

    private void startInlineState() {
        ++count;
        inlineText += "$(";
    }

    private void getInParenthesis() {
        ++count;
        inlineText += "(";
    }

    private void getOutParenthesis() {
        if (count > 0){
            --count;
            inlineText += ')';
        }
    }
%}

%class SquashTestLexer
%implements FlexLexer
%unicode
%function advance
%type IElementType
%ignorecase
%eof{  return;
%eof}

WHITE_SPACE=[\ \t\x0B]
END_LINE=[\r\n]
COMMENT="//"[^\r\n]*

METADATA="METADATA :"
METADATA_KEY = [^\r\n":""//"][^\r\n":"]+
METADATA_SEPARATOR = ":"
METADATA_VALUE = [^\r\n":"]+

SETUP="SETUP :"
TEST="TEST :"
TEARDOWN="TEARDOWN :"
SYMBOL="#"

MACRO_LINE_CONTENT=[^\r\n\f]+

ATOMIC_VALUE=[\w"-"".""/"\\]+
BUILT_IN_VALUE="{"{ATOMIC_VALUE}?"}"
TEMP_SCOPE_VALUE="{{"{ATOMIC_VALUE}?"}}"
CONVERTER_NAME="("[^"("")"]+")"

OPEN_INLINE="$("
OPEN_PARANTHESE="("
CLOSE_PARANTHESE=")"
INLINE_VALUE_CHAR=[^"("")"\r\n\f]

CMD_HEAD_KEY = "LOAD" | "CONVERT" | "ASSERT" | "VERIFY"
EXECUTE = "EXECUTE"
DEFINE = "DEFINE"

CMD_KEY = "AS" | "WITH" | "ON" | "THAN" | "THE" | "FROM"
USING = "USING"
VALUE_SEPARATOR = ","
TO = "TO"
VALIDATOR = "IS" | "HAS" | "DOES"

AC_POSITION = ([^"$"\ \t\x0B\r\n\f][^\ \t\x0B\r\n\f]*) | ("$"[^"("\ \t\x0B\\r\n\f]*)
AC_POSITION_IN_USING = ([^"$"\ \t\x0B\r\n\f","][^\ \t\x0B\r\n\f","]*) | ("$"[^"("\ \t\x0B\\r\n\f","]*)

%state DEFINE_VALUE, METADATA, METADATA_VALUE, MACRO_LINE, CMD_KEY, CONVERSION, CMD_VALUE, EXECUTE_CMD, ASSERTION
%xstate INLINE, USING_VALUE_LIST
%%

<YYINITIAL, MACRO_LINE, CMD_KEY, CONVERSION, CMD_VALUE, DEFINE_VALUE, EXECUTE_CMD, ASSERTION, INLINE, USING_VALUE_LIST>
                                   ({WHITE_SPACE}*{END_LINE})+                  { yybegin(YYINITIAL); return SquashTestTypes.CRLF; }

<YYINITIAL>^{WHITE_SPACE}*{COMMENT}                                             { yybegin(YYINITIAL); return SquashTestTypes.COMMENT; }

<YYINITIAL>^{WHITE_SPACE}*{METADATA}{WHITE_SPACE}*                              { yybegin(METADATA); return SquashTestTypes.METADATA; }

<METADATA>^{WHITE_SPACE}*{METADATA_KEY}{WHITE_SPACE}*                           { yybegin(METADATA); return SquashTestTypes.METADATA_KEY; }
<METADATA_VALUE, METADATA>{WHITE_SPACE}*{METADATA_SEPARATOR}{WHITE_SPACE}*      { yybegin(METADATA_VALUE); return SquashTestTypes.METADATA_SEPARATOR; }
<METADATA_VALUE>{WHITE_SPACE}*{METADATA_VALUE}{WHITE_SPACE}*                    { yybegin(METADATA); return SquashTestTypes.METADATA_VALUE; }
<METADATA, METADATA_VALUE>({WHITE_SPACE}*{END_LINE})                            { yybegin(METADATA); return SquashTestTypes.CRLF; }
<METADATA>^{WHITE_SPACE}*{COMMENT}                                              { yybegin(METADATA); return SquashTestTypes.COMMENT; }

<YYINITIAL, METADATA>^{WHITE_SPACE}*{SETUP}{WHITE_SPACE}*$                      { yybegin(YYINITIAL); return SquashTestTypes.SETUP; }
<YYINITIAL, METADATA>^{WHITE_SPACE}*{TEST}{WHITE_SPACE}*$                       { yybegin(YYINITIAL); return SquashTestTypes.TEST; }
<YYINITIAL>^{WHITE_SPACE}*{TEARDOWN}{WHITE_SPACE}*$                             { yybegin(YYINITIAL); return SquashTestTypes.TEARDOWN; }

<YYINITIAL, METADATA>^{WHITE_SPACE}*{SYMBOL}{WHITE_SPACE}+                      { yybegin(MACRO_LINE); return SquashTestTypes.SYMBOL; }

<MACRO_LINE>{MACRO_LINE_CONTENT}                                                { return SquashTestTypes.MACRO_LINE_CONTENT; }

//a whitespace after any CMD_KEY words to identify them from a non-defining word before the auto-completion

<YYINITIAL, METADATA>^{WHITE_SPACE}*{CMD_HEAD_KEY}{WHITE_SPACE}                 { yybegin(CMD_VALUE); return SquashTestTypes.CMD_HEAD_KEY; }
<YYINITIAL, METADATA>^{WHITE_SPACE}*{DEFINE}{WHITE_SPACE}                       { yybegin(DEFINE_VALUE); return SquashTestTypes.CMD_HEAD_KEY; }
<YYINITIAL, METADATA>^{WHITE_SPACE}*{EXECUTE}{WHITE_SPACE}                      { yybegin(EXECUTE_CMD); return SquashTestTypes.CMD_HEAD_KEY; }

<CMD_KEY, USING_VALUE_LIST>{WHITE_SPACE}+{CMD_KEY}{WHITE_SPACE}+                { yybegin(CMD_VALUE); return SquashTestTypes.CMD_KEY; }
<CMD_KEY, USING_VALUE_LIST>{WHITE_SPACE}+{TO}{WHITE_SPACE}+                     { yybegin(CONVERSION); return SquashTestTypes.CMD_KEY; }
<CMD_KEY, USING_VALUE_LIST>{WHITE_SPACE}+{VALIDATOR}{WHITE_SPACE}+              { yybegin(ASSERTION); return SquashTestTypes.CMD_KEY; }
<CMD_KEY>{WHITE_SPACE}+{USING}{WHITE_SPACE}+                                    { yybegin(USING_VALUE_LIST); inUsingValueList = true; return SquashTestTypes.USING; }

<CONVERSION>{ATOMIC_VALUE}({WHITE_SPACE}*{CONVERTER_NAME})?                     { yybegin(CMD_KEY); return SquashTestTypes.CONVERTER; }

<CMD_VALUE, DEFINE_VALUE, EXECUTE_CMD, ASSERTION>{OPEN_INLINE}                  { yybegin(INLINE); inlineText=""; count=0; inlineInUsing=false; startInlineState();
                                                                                               return  SquashTestTypes.INLINE_VALUE_CHAR;}
<USING_VALUE_LIST>{
    {OPEN_INLINE}                                                               { if (inUsingValueList) {
                                                                                    yybegin(INLINE); inlineText=""; count=0; inlineInUsing=true; startInlineState();
                                                                                    return  SquashTestTypes.INLINE_VALUE_CHAR;}
                                                                                }
    {WHITE_SPACE}*{VALUE_SEPARATOR}{WHITE_SPACE}*                               { inUsingValueList = true; return  SquashTestTypes.VALUE_SEPARATOR;}
    {ATOMIC_VALUE} | {BUILT_IN_VALUE} | {TEMP_SCOPE_VALUE}                      { if (inUsingValueList) {
                                                                                    inUsingValueList = false;
                                                                                    return SquashTestTypes.VALUE; }
                                                                                }
    {AC_POSITION_IN_USING}                                                      { if (inUsingValueList) {
                                                                                        inUsingValueList = false;
                                                                                        return SquashTestTypes.AC_POSITION; }
                                                                                }
    {WHITE_SPACE}+                                                              { return TokenType.WHITE_SPACE; }
    [^]                                                                         { return TokenType.BAD_CHARACTER; }
}

<INLINE>{
    {CLOSE_PARANTHESE}                                                          { getOutParenthesis(); if (count == 0) {
                                                                                    if (inlineInUsing) {
                                                                                        yybegin(USING_VALUE_LIST);
                                                                                        inUsingValueList = false;
                                                                                        inlineInUsing = false;
                                                                                    } else {
                                                                                        yybegin(CMD_KEY);
                                                                                    }
                                                                                    return SquashTestTypes.INLINE_VALUE_CHAR;
                                                                                    }
                                                                                return SquashTestTypes.INLINE_VALUE_CHAR;}
    {INLINE_VALUE_CHAR}                                                         { return SquashTestTypes.INLINE_VALUE_CHAR;}
    {OPEN_PARANTHESE} | {OPEN_INLINE}                                           { getInParenthesis(); return SquashTestTypes.INLINE_VALUE_CHAR;}

}

<CMD_VALUE>{ATOMIC_VALUE} | {BUILT_IN_VALUE} | {TEMP_SCOPE_VALUE}               { yybegin(CMD_KEY); return SquashTestTypes.VALUE; }
<EXECUTE_CMD>{ATOMIC_VALUE} | {BUILT_IN_VALUE} | {TEMP_SCOPE_VALUE}             { yybegin(CMD_KEY); return SquashTestTypes.EXECUTE_CMD; }
<ASSERTION>{ATOMIC_VALUE} | {BUILT_IN_VALUE} | {TEMP_SCOPE_VALUE}               { yybegin(CMD_KEY); return SquashTestTypes.ASSERTION_VALIDATOR; }

{WHITE_SPACE}+                                                                  { return TokenType.WHITE_SPACE; }

<YYINITIAL, METADATA, CMD_KEY>{AC_POSITION}                                     { yybegin(CMD_VALUE); return SquashTestTypes.AC_POSITION; }
<CMD_VALUE, DEFINE_VALUE, EXECUTE_CMD, CONVERSION, ASSERTION>{AC_POSITION}      { yybegin(CMD_KEY); return SquashTestTypes.AC_POSITION; }


//important to handle unidentified element, don't remove it even there's an IDE warning!!!
[^]                                                                             { return TokenType.BAD_CHARACTER; }