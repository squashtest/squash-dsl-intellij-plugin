/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.highlight;

import com.intellij.lexer.Lexer;
import com.intellij.openapi.editor.DefaultLanguageHighlighterColors;
import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighterBase;
import com.intellij.psi.tree.IElementType;
import org.jetbrains.annotations.NotNull;
import org.squashtest.ta.intellij.plugin.lexer.SquashMacroLexerAdapter;
import org.squashtest.ta.intellij.plugin.macro.psi.SquashMacroTypes;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static com.intellij.openapi.editor.colors.TextAttributesKey.createTextAttributesKey;

public class SquashMacroSyntaxHighlighter extends SyntaxHighlighterBase {
    static final TextAttributesKey BUILT_IN =
            createTextAttributesKey("MACRO_BUILT_IN", DefaultLanguageHighlighterColors.CLASS_NAME);

    public static final TextAttributesKey CMD_KEY =
            createTextAttributesKey("MACRO_CMD_KEY", DefaultLanguageHighlighterColors.KEYWORD);

    public static final TextAttributesKey VALUE =
            createTextAttributesKey("MACRO_VALUE", DefaultLanguageHighlighterColors.STRING);

    static final TextAttributesKey INLINE_VALUE =
            createTextAttributesKey("TEST_INLINE_VALUE", DefaultLanguageHighlighterColors.CONSTANT);

    public static final TextAttributesKey COMMENT =
            createTextAttributesKey("MACRO_COMMENT_LINE", DefaultLanguageHighlighterColors.LINE_COMMENT);

    private static final TextAttributesKey SEPARATOR =
            createTextAttributesKey("MACRO_SEPARATOR", DefaultLanguageHighlighterColors.FUNCTION_CALL);

    static final TextAttributesKey MACRO_SYMBOL =
            createTextAttributesKey("MACRO_SYMBOL", DefaultLanguageHighlighterColors.FUNCTION_DECLARATION);

    static final TextAttributesKey MACRO_SIGNATURE_KEY =
            createTextAttributesKey("MACRO_SIGNATURE_KEY", DefaultLanguageHighlighterColors.FUNCTION_DECLARATION);

    static final TextAttributesKey MACRO_SIGNATURE_PARAM =
            createTextAttributesKey("MACRO_SIGNATURE_PARAM", DefaultLanguageHighlighterColors.STRING);

    static final TextAttributesKey MACRO_MACRO_LINE =
            createTextAttributesKey("MACRO_MACRO_LINE", DefaultLanguageHighlighterColors.STRING);

    static final TextAttributesKey MACRO_KEY =
            createTextAttributesKey("MACRO_MACRO_KEY", DefaultLanguageHighlighterColors.FUNCTION_DECLARATION);

    private static final TextAttributesKey[] BUILT_IN_KEYS = new TextAttributesKey[]{BUILT_IN};
    private static final TextAttributesKey[] KEY_KEYS = new TextAttributesKey[]{CMD_KEY};
    private static final TextAttributesKey[] VALUE_KEYS = new TextAttributesKey[]{VALUE};
    private static final TextAttributesKey[] INLINE_VALUE_KEYS = new TextAttributesKey[]{INLINE_VALUE};
    private static final TextAttributesKey[] COMMENT_KEYS = new TextAttributesKey[]{COMMENT};
    private static final TextAttributesKey[] SEPARATOR_KEYS = new TextAttributesKey[]{SEPARATOR};
    private static final TextAttributesKey[] SYMBOL_KEYS = new TextAttributesKey[]{MACRO_SYMBOL};
    private static final TextAttributesKey[] MACRO_TITLE_KEY_KEYS = new TextAttributesKey[]{MACRO_SIGNATURE_KEY};
    private static final TextAttributesKey[] MACRO_TITLE_PARAM_KEYS = new TextAttributesKey[]{MACRO_SIGNATURE_PARAM};
    private static final TextAttributesKey[] MACRO_LINE = new TextAttributesKey[]{MACRO_MACRO_LINE};
    private static final TextAttributesKey[] EMPTY_KEYS = new TextAttributesKey[0];

    private static final Map<IElementType, TextAttributesKey[]> TOKEN_KEYS_DICTIONARY;

    static {

        Map<IElementType, TextAttributesKey[]> newMap = new HashMap<>();
        newMap.put(SquashMacroTypes.SYMBOL, SYMBOL_KEYS);
        newMap.put(SquashMacroTypes.TITLE_KEY, MACRO_TITLE_KEY_KEYS);
        newMap.put(SquashMacroTypes.TITLE_PARAM, MACRO_TITLE_PARAM_KEYS);
        newMap.put(SquashMacroTypes.TITLE_FIRST_PARAM, MACRO_TITLE_PARAM_KEYS);

        newMap.put(SquashMacroTypes.MACRO_LINE_CONTENT, MACRO_LINE);

        newMap.put(SquashMacroTypes.SEPARATOR, SEPARATOR_KEYS);

        newMap.put(SquashMacroTypes.COMMENT,COMMENT_KEYS);

        newMap.put(SquashMacroTypes.CMD_HEAD_KEY, KEY_KEYS);
        newMap.put(SquashMacroTypes.CMD_KEY, KEY_KEYS);
        newMap.put(SquashMacroTypes.USING, KEY_KEYS);


        newMap.put(SquashMacroTypes.VALUE, VALUE_KEYS);
        newMap.put(SquashMacroTypes.INLINE_VALUE_CHAR, INLINE_VALUE_KEYS);

        newMap.put(SquashMacroTypes.CONVERTER, BUILT_IN_KEYS);
        newMap.put(SquashMacroTypes.EXECUTE_CMD, BUILT_IN_KEYS);
        newMap.put(SquashMacroTypes.ASSERTION_VALIDATOR, BUILT_IN_KEYS);

        TOKEN_KEYS_DICTIONARY = Collections.unmodifiableMap(newMap);
    }

    @NotNull
    @Override
    public Lexer getHighlightingLexer() {
        return new SquashMacroLexerAdapter();
    }

    @NotNull
    @Override
    public TextAttributesKey[] getTokenHighlights(IElementType tokenType) {
        TextAttributesKey[] keys = TOKEN_KEYS_DICTIONARY.get(tokenType);
        return (keys!=null)? keys : EMPTY_KEYS;
    }
}
