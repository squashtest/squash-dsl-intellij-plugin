/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.completion;

import com.intellij.codeInsight.completion.CompletionResultSet;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import org.jetbrains.annotations.NotNull;

class CompletionHelper {
    //TODO : this should probably live in the framework connector
    static final String TIMEOUT_EXCEPTION_MSG = "Retrieving of Squash component registry is in progress. Please wait until it is completely charged!";

    /**
     * This method is to propose the autocompletion of all Squash Command line templates
     * @param result the IntelliJ object control the autocompletion process
     */
    void allCommandLineTemplatesCompletion(CompletionResultSet result) {
        result.addElement(LookupElementBuilder.create(SquashDSLInstructionTemplate.DEFINE.toString()).withCaseSensitivity(false));
        result.addElement(LookupElementBuilder.create("DEFINE ").withCaseSensitivity(false));
        result.addElement(LookupElementBuilder.create(SquashDSLInstructionTemplate.LOAD.toString()).withCaseSensitivity(false));
        result.addElement(LookupElementBuilder.create(SquashDSLInstructionTemplate.LOAD_FROM.toString()).withCaseSensitivity(false));
        result.addElement(LookupElementBuilder.create("LOAD ").withCaseSensitivity(false));
        result.addElement(LookupElementBuilder.create(SquashDSLInstructionTemplate.CONVERT.toString()).withCaseSensitivity(false));
        result.addElement(LookupElementBuilder.create("CONVERT ").withCaseSensitivity(false));
        result.addElement(LookupElementBuilder.create(SquashDSLInstructionTemplate.EXECUTE.toString()).withCaseSensitivity(false));
        result.addElement(LookupElementBuilder.create("EXECUTE ").withCaseSensitivity(false));
        result.addElement(LookupElementBuilder.create(SquashDSLInstructionTemplate.ASSERT.toString()).withCaseSensitivity(false));
        result.addElement(LookupElementBuilder.create(SquashDSLInstructionTemplate.ASSERT_IS_BINARY.toString()).withCaseSensitivity(false));
        result.addElement(LookupElementBuilder.create(SquashDSLInstructionTemplate.ASSERT_DOES_UNARY.toString()).withCaseSensitivity(false));
        result.addElement(LookupElementBuilder.create(SquashDSLInstructionTemplate.ASSERT_IS_UNARY.toString()).withCaseSensitivity(false));
        result.addElement(LookupElementBuilder.create(SquashDSLInstructionTemplate.ASSERT_HAS_UNARY.toString()).withCaseSensitivity(false));
        result.addElement(LookupElementBuilder.create("ASSERT ").withCaseSensitivity(false));
        result.addElement(LookupElementBuilder.create(SquashDSLInstructionTemplate.VERIFY.toString()).withCaseSensitivity(false));
        result.addElement(LookupElementBuilder.create(SquashDSLInstructionTemplate.VERIFY_IS_BINARY.toString()).withCaseSensitivity(false));
        result.addElement(LookupElementBuilder.create(SquashDSLInstructionTemplate.VERIFY_DOES_UNARY.toString()).withCaseSensitivity(false));
        result.addElement(LookupElementBuilder.create(SquashDSLInstructionTemplate.VERIFY_IS_UNARY.toString()).withCaseSensitivity(false));
        result.addElement(LookupElementBuilder.create(SquashDSLInstructionTemplate.VERIFY_HAS_UNARY.toString()).withCaseSensitivity(false));
        result.addElement(LookupElementBuilder.create("VERIFY ").withCaseSensitivity(false));
        result.addElement(LookupElementBuilder.create("# "));
    }

    /**
     * This methods returns the start of the current line before the position of the carret when autocompletion was resquested.
     * @param fileContent content of the current file
     * @param autoCompletePos the position where the auto-completion is asked
     * @return the whole text in the same line but before the AC position
     */
    @NotNull
    String getInlineTextBeforeACPosition(String fileContent, int autoCompletePos) {
        String beforeAC = fileContent.substring(0, autoCompletePos);
        int lastCRLF = beforeAC.lastIndexOf("\n");
        return (lastCRLF != -1)? beforeAC.substring(lastCRLF+1):beforeAC;
    }

    /**
     * This methods returns the string from the Auto-completion position to the end of the current line
     * @param docContent content of the current file
     * @param autoCompletePos the position where the auto-completion is asked
     * @return the whole text in the same line but after the AC position
     */
    String getInlineTextAfterACPosition(String docContent, int autoCompletePos) {
        String afterAC = docContent.substring(autoCompletePos);
        int firstCRLF = afterAC.indexOf("\n");
        return (firstCRLF != -1)? afterAC.substring(0, firstCRLF):afterAC;
    }
}
