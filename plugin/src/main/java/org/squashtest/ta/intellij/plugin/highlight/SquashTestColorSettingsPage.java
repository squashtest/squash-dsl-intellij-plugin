/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.highlight;

import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighter;
import com.intellij.openapi.options.colors.AttributesDescriptor;
import com.intellij.openapi.options.colors.ColorDescriptor;
import com.intellij.openapi.options.colors.ColorSettingsPage;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.squashtest.ta.intellij.plugin.language.SquashTestIcons;

import javax.swing.Icon;
import java.util.Map;

public class SquashTestColorSettingsPage implements ColorSettingsPage {
    private static final AttributesDescriptor[] DESCRIPTORS = new AttributesDescriptor[]{
            new AttributesDescriptor("Metadata Key", SquashTestSyntaxHighlighter.METADATA_KEY),
            new AttributesDescriptor("Metadata Separator", SquashTestSyntaxHighlighter.METADATA_SEP),
            new AttributesDescriptor("Metadata Value", SquashTestSyntaxHighlighter.METADATA_VALUE),
            new AttributesDescriptor("Macro Symbol", SquashTestSyntaxHighlighter.MACRO_SYMBOL),
            new AttributesDescriptor("Phase", SquashTestSyntaxHighlighter.PHASE),
            new AttributesDescriptor("Instruction Key", SquashTestSyntaxHighlighter.CMD_KEY),
            new AttributesDescriptor("Instruction Built-in Value", SquashTestSyntaxHighlighter.BUILT_IN),
            new AttributesDescriptor("Instruction Value", SquashTestSyntaxHighlighter.VALUE),
            new AttributesDescriptor("Instruction Inline Declared Value", SquashTestSyntaxHighlighter.INLINE_VALUE),
            new AttributesDescriptor("Macro Line", SquashTestSyntaxHighlighter.TEST_MACRO_LINE),
            new AttributesDescriptor("Macro Key", SquashTestSyntaxHighlighter.MACRO_KEY),
            new AttributesDescriptor("Comment Line", SquashTestSyntaxHighlighter.COMMENT)
    };

    @Nullable
    @Override
    public Icon getIcon() {
        return SquashTestIcons.FILE;
    }

    @NotNull
    @Override
    public SyntaxHighlighter getHighlighter() {
        return new SquashTestSyntaxHighlighter();
    }

    @NotNull
    @Override
    public String getDemoText() {
        return "//This is the first comment line.\n" +
                "METADATA :\n" +
                "Key_only\n" +
                "Key : value\n" +
                "Multi-value-key : value1\n" +
                "//This is a comment line inside multi-line metadata block.\n" +
                "                : value2\n" +
                "                : value3\n" +
                "\n" +
                "SETUP :\n" +
                "DEFINE $(raw_data) AS nameInTheContext\n" +
                "\n" +
                "# ASSERT XML {result} IS $(VALID) USING SCHEMA {{xsd_path}}\n" +
                "\n" +
                "TEST :\n" +
                "LOAD path_To_Resource FROM resourceLibrary AS nameInTheContext\n" +
                "\n" +
                "CONVERT resourceToConvert TO xml(xslt) USING config AS convertedResource\n" +
                "\n" +
                "ASSERT resourceToTest DOES contain THE expectedResult USING config\n" +
                "\n" +
                "VERIFY resourceToTest IS equal WITH expectedResult USING config\n" +
                "\n" +
                "TEARDOWN :\n"+
                "EXECUTE pause WITH Res ON Tar USING config AS result\n";
    }

    @Nullable
    @Override
    public Map<String, TextAttributesKey> getAdditionalHighlightingTagToDescriptorMap() {
        return null;
    }

    @NotNull
    @Override
    public AttributesDescriptor[] getAttributeDescriptors() {
        return DESCRIPTORS;
    }

    @NotNull
    @Override
    public ColorDescriptor[] getColorDescriptors() {
        return ColorDescriptor.EMPTY_ARRAY;
    }

    @NotNull
    @Override
    public String getDisplayName() {
        return "Squash Test File";
    }
}
