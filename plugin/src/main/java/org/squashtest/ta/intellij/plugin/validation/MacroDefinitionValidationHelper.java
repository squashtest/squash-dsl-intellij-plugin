/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.validation;

import com.intellij.psi.PsiElement;
import org.squashtest.ta.galaxia.enginelink.components.*;
import org.squashtest.ta.intellij.plugin.macro.psi.*;
import org.squashtest.ta.intellij.plugin.projectmodel.MacroDefinitionOperations;
import org.squashtest.ta.intellij.plugin.projectmodel.SquashMacroFileLocationProjectService;
import org.squashtest.ta.intellij.plugin.projectmodel.SquashMacroTitleControlResult;
import org.squashtest.ta.intellij.plugin.tools.CollectionToolkit;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MacroDefinitionValidationHelper {
    private final MacroDefinitionOperations macroDefinitionOperations = new MacroDefinitionOperations();

    private SquashMacroFileLocationProjectService macroFileLocator;

    public MacroDefinitionValidationHelper(SquashMacroFileLocationProjectService macroFileLocator){
        this.macroFileLocator =macroFileLocator;
    }

    /**
     * This is a generic method to check if the current macro title is already used or not
     * @param macroJsonFileContent the framework built-in instructions
     * @return true if the current Squash Macro file's macro title is defined, false if NOT
     */
    SquashMacroTitleControlResult isMacroTitleExisted(SquashMacroMacroTitleContent macroTitleContent, SquashDSLComponentRegistry macroJsonFileContent) {
        //charge all macro titles in the project 'shortcuts' folder or one of its subfolders
        List<SquashMacroMacroTitle> projectMacroTitleList = macroFileLocator.getMacroTitlesInProjectShortcutsFolder();

        //get all built-in macros
        List<SquashDSLMacro> builtInMacroList = macroJsonFileContent.getMacros();

        return isMacroTitleExisted(builtInMacroList, projectMacroTitleList, macroTitleContent);
    }

    private SquashMacroTitleControlResult isMacroTitleExisted(List<SquashDSLMacro> builtInMacroList, List<SquashMacroMacroTitle> projectMacroTitleList, SquashMacroMacroTitleContent macroTitleContent) {
        SquashMacroTitleControlResult result = new SquashMacroTitleControlResult(false, false);

        PsiElement currentFile = macroTitleContent.getContainingFile();
        List<String> actualKeySet = macroDefinitionOperations.getMacroKeysFromTitleContent(macroTitleContent);
        Map<Integer, String> actualParamSet = macroDefinitionOperations.getMacroParamsFromTitleContent(macroTitleContent);

        //searching the current macro in built_in macro list
        findMacroInBuiltIn(builtInMacroList, result, actualKeySet, actualParamSet);

        //next let's find it in all project 'shortcuts' .macro files
        return findMacroInProjectCustom(projectMacroTitleList, result, currentFile, actualKeySet, actualParamSet);
    }

    private SquashMacroTitleControlResult findMacroInProjectCustom(List<SquashMacroMacroTitle> projectMacroTitleList, SquashMacroTitleControlResult result, PsiElement currentFile, List<String> actualKeySet, Map<Integer, String> actualParamSet) {
        int k =0;
        while (k<projectMacroTitleList.size() && !result.isInCustom()) {
            SquashMacroMacroTitle macroTitle = projectMacroTitleList.get(k);
            PsiElement comparedFile = macroTitle.getContainingFile();
            if (!comparedFile.equals(currentFile)){
                isInCustomMacroList(result, actualKeySet, actualParamSet, macroTitle);
            }
            ++k;
        }
        return result;
    }

    private void isInCustomMacroList(SquashMacroTitleControlResult result, List<String> actualKeySet, Map<Integer, String> actualParamSet, SquashMacroMacroTitle macroTitle) {
        int keySetLength = actualKeySet.size();
        int paramSetLength = actualParamSet.size();

        SquashMacroMacroTitleContent titleContent = macroTitle.getMacroTitleContent();

        List<String> expectedKeySet = macroDefinitionOperations.getMacroKeysFromTitleContent(titleContent);
        Map<Integer, String> expectedParamSet = macroDefinitionOperations.getMacroParamsFromTitleContent(titleContent);
        if (expectedKeySet.size()==keySetLength && expectedParamSet.size()==paramSetLength){
            //control first title param, if any
            boolean actualHasFirstParam = actualParamSet.containsKey(0);
            boolean expectedHasFirstParam = expectedParamSet.containsKey(0);
            if (((actualHasFirstParam && expectedHasFirstParam) ||
                    (!actualHasFirstParam && !expectedHasFirstParam) ) && CollectionToolkit.compareKeySet(actualKeySet, expectedKeySet)==keySetLength)    {
                result.setInCustom(true);
            }
        }
    }

    private void findMacroInBuiltIn(List<SquashDSLMacro> builtInMacroList, SquashMacroTitleControlResult result, List<String> actualKeySet, Map<Integer, String> actualParamSet) {
        int j = 0;
        while ( j < builtInMacroList.size() && !result.isInBuiltIn()) {
            List<SquashDSLMacroSignature> currentSignatures = builtInMacroList.get(j).getSignatures();
            List<String> expectedKeySet = new ArrayList<>();
            List<String> expectedParamSet = new ArrayList<>();
            initiateKeyAndParamSets(currentSignatures, expectedKeySet, expectedParamSet);

            builtInUpdatedResult(result, actualKeySet, actualParamSet, currentSignatures, expectedKeySet, expectedParamSet);
            ++j;
        }
    }

    private void builtInUpdatedResult(SquashMacroTitleControlResult result, List<String> actualKeySet, Map<Integer, String> actualParamSet, List<SquashDSLMacroSignature> currentSignatures, List<String> expectedKeySet, List<String> expectedParamSet) {
        int keySetLength = actualKeySet.size();
        int paramSetLength = actualParamSet.size();

        if (expectedKeySet.size() == keySetLength &&
                expectedParamSet.size()== paramSetLength){
            //control first title param, if any
            boolean actualHasFirstParam = actualParamSet.containsKey(0);
            boolean expectedHasFirstParam = currentSignatures.get(0) instanceof SquashDSLMacroParam;
            if (((actualHasFirstParam && expectedHasFirstParam) ||
                    (!actualHasFirstParam && !expectedHasFirstParam) ) && CollectionToolkit.compareKeySet(actualKeySet, expectedKeySet)==keySetLength )  {
                result.setInBuiltIn(true);
            }
        }
    }

    private void initiateKeyAndParamSets(List<SquashDSLMacroSignature> currentSignatures, List<String> expectedKeySet, List<String> expectedParamSet) {
        for (SquashDSLMacroSignature signature: currentSignatures) {
            if (signature instanceof SquashDSLMacroFixedPart) expectedKeySet.add(((SquashDSLMacroFixedPart) signature).getContent());
            if (signature instanceof SquashDSLMacroParam) expectedParamSet.add(((SquashDSLMacroParam) signature).getDefinition().getName());
        }
    }

}
