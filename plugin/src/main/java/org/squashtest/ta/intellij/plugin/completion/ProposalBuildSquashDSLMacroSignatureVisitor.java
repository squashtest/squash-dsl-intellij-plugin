/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.completion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.squashtest.ta.galaxia.enginelink.components.SquashDSLMacroSignatureVisitor;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLMacroFixedPart;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLMacroParam;
import org.squashtest.ta.intellij.plugin.tools.LineSearchingResult;
import org.squashtest.ta.intellij.plugin.tools.StringToolkit;

import java.util.HashSet;
import java.util.Set;

class ProposalBuildSquashDSLMacroSignatureVisitor implements SquashDSLMacroSignatureVisitor<Void> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProposalBuildSquashDSLMacroSignatureVisitor.class);


        private final String primer;
        LineSearchingResult lastFixedMatch;
        StringBuilder proposalBuilder;
        ProposalBuildSquashDSLMacroSignatureVisitor nextHypothesis;

        public ProposalBuildSquashDSLMacroSignatureVisitor(String primer) {
            this.primer = primer;
            proposalBuilder = new StringBuilder();
        }

        protected ProposalBuildSquashDSLMacroSignatureVisitor(String primer, LineSearchingResult lastFixedMatch){
            this(primer);
            if(LOGGER.isDebugEnabled()) {
                LOGGER.debug("Creating alternative hypothesis with match {}", lastFixedMatch == null ? "null" : "'"+lastFixedMatch.getContent() + "'");
            }
            this.lastFixedMatch=lastFixedMatch;
        }

        @Override
        public Void visit(SquashDSLMacroFixedPart squashDSLMacroFixedPart) {
            LOGGER.debug("Treating fixed part '{}'",squashDSLMacroFixedPart.getContent());
            if(nextHypothesis!=null){
                LOGGER.debug("Updating other hypotheses, too.");
                nextHypothesis.visit(squashDSLMacroFixedPart);
            }
            if(lastFixedMatch==null) {
                //No fixed match yet : search the primer from position 0
                lookupFixedToken(squashDSLMacroFixedPart, 0);
            }else if(lastFixedMatch.getEndIndex()<primer.length()){
                //The primer is not wholly matched yet => search from the last fixed match
                lookupFixedToken(squashDSLMacroFixedPart,lastFixedMatch.getEndIndex());
            }else{
                proposalBuilder.append(squashDSLMacroFixedPart.getContent());
            }
            LOGGER.debug("We have now matched '{}' up to {} proposal comes to '{}'",lastFixedMatch.getContent(),lastFixedMatch.getEndIndex(),proposalBuilder);
            return null;
        }

        /**
         * Apply rules for the first fixed token match.
         *
         * @param squashDSLMacroFixedPart the fixed token descriptor.
         * @param lastMatchIndex the last match index
         */
        protected void lookupFixedToken(SquashDSLMacroFixedPart squashDSLMacroFixedPart, int lastMatchIndex) {
            LOGGER.debug("First match, lookup from primer start.");
            LineSearchingResult paramSearchResult=StringToolkit.findCompleteOccurrenceOrPartialEndingOccurrence(primer,squashDSLMacroFixedPart.getContent(), lastMatchIndex);
            if(paramSearchResult.wasFound()){
                /*
                 * We have found the fixed part token => let's record the match.
                 */
                lastFixedMatch=paramSearchResult;
                if(paramSearchResult.getContent().length()<squashDSLMacroFixedPart.getContent().length()){
                    /*
                     * This was a partial match at the end of the primer => let's begin our completion proposal with the missing end of the token
                     */
                    proposalBuilder.append(squashDSLMacroFixedPart.getContent().substring(paramSearchResult.getContent().length()));
                }
            }else{
                /*
                * The primer is supposed to match the macro, so if we don't find that fixed part, the previous signature element must have been a parameter,
                * and the wholme primer is its value. Our fixed part value is the first part of the completion proposal.
                */
                lastFixedMatch=new LineSearchingResult.LineSearchHit(0,primer.length(),primer);
                proposalBuilder.append(squashDSLMacroFixedPart.getContent());
            }
        }

        @Override
        public Void visit(SquashDSLMacroParam squashDSLMacroParam) {
            LOGGER.debug("Treating param '{}'",squashDSLMacroParam.definitionPart());
            if(nextHypothesis!=null){
                LOGGER.debug("Updating ohter hypotheses, too.");
                nextHypothesis.visit(squashDSLMacroParam);
            }
            if(lastFixedMatch==null || lastFixedMatch.getEndIndex()<primer.length()){
                LOGGER.debug("Consuming primer end.");
                nextHypothesis=new ProposalBuildSquashDSLMacroSignatureVisitor(this.primer,this.lastFixedMatch);
                lastFixedMatch=new LineSearchingResult.LineSearchHit(0,primer.length(),primer);
            }else {//amorce entièrement consommée => on ajoute le paramètre à la suggestion
                LOGGER.debug("Primer end is consumed, adding parameter to proposal.");
                proposalBuilder.append(squashDSLMacroParam.definitionPart());
            }
            return null;
        }

        public Set<String> getProposals(){
            LOGGER.debug("Proposal set required.");
            Set<String> proposals=new HashSet<>();
            String proposal = proposalBuilder.toString();
            if(!proposal.trim().isEmpty()) {
                LOGGER.debug("Provide non-empty proposal '{}'", proposal);
                proposals.add(proposal);
            }
            if(nextHypothesis!=null){
                LOGGER.debug("Getting alternative proposals from other match hypotheses.");
                proposals.addAll(nextHypothesis.getProposals());
            }
            return proposals;
        }
}
