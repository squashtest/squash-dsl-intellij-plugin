/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.validation;

import java.util.Arrays;
import java.util.List;

public abstract class DSLInstructionParser {
    boolean matches(List<String> instructionKeyList){
        List<String> instructionAllTokens = Arrays.asList(getInstructionTokens());
        for (String key : instructionKeyList) {
            //if there is a KEY out of the instruction template key list --> false
            if (!instructionAllTokens.contains(key)){
                return false;
            }
        }
        String[] instructionMandatoryTokens = getInstructionMandatoryTokens();
        for (String mandatoryKey : instructionMandatoryTokens) {
            //if there is a missing mandatory key --> false
            if (!instructionKeyList.contains(mandatoryKey)){
                if ("IS".equals(mandatoryKey)) {
                    //the keyword "IS" can be replaced by "HAS" or "DOES"
                    return instructionKeyList.contains("HAS") || instructionKeyList.contains("DOES");
                } else {
                    return false;
                }
            }
        }
        return true;
    }

    public abstract String getInstructionParserName();

    public abstract String[] getInstructionTokens();

    public abstract String[] getInstructionMandatoryTokens();

}
