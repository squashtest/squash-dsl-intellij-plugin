/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.validation;

import com.intellij.codeInspection.ProblemHighlightType;
import com.intellij.lang.ASTNode;
import com.intellij.lang.annotation.Annotation;
import com.intellij.lang.annotation.AnnotationHolder;
import org.jetbrains.annotations.NotNull;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLAssertion;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLCommand;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLComponentRegistry;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLConverter;
import org.squashtest.ta.intellij.plugin.file.psi.SquashTestCmdHeadProperty;
import org.squashtest.ta.intellij.plugin.file.psi.SquashTestCmdHeadPropertyKey;
import org.squashtest.ta.intellij.plugin.file.psi.SquashTestCmdProperty;
import org.squashtest.ta.intellij.plugin.file.psi.SquashTestCommandLine;
import org.squashtest.ta.intellij.plugin.file.psi.SquashTestUsingClause;
import org.squashtest.ta.intellij.plugin.file.psi.SquashTestValueList;
import org.squashtest.ta.intellij.plugin.macro.psi.SquashMacroCmdHeadProperty;
import org.squashtest.ta.intellij.plugin.macro.psi.SquashMacroCmdHeadPropertyKey;
import org.squashtest.ta.intellij.plugin.macro.psi.SquashMacroCmdProperty;
import org.squashtest.ta.intellij.plugin.macro.psi.SquashMacroCommandLine;
import org.squashtest.ta.intellij.plugin.macro.psi.SquashMacroUsingClause;
import org.squashtest.ta.intellij.plugin.macro.psi.SquashMacroValueList;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

class DSLInstructionLineValidationHelper {
    private static final DSLInstructionParser[] PARSERS = { new LoadInstructionParser(),
            new DefineInstructionParser(), new ConvertInstructionParser(),
            new ExecuteInstructionParser(), new AssertInstructionParser(),
            new VerifyInstructionParser()};

    private static final String INSTRUCTIONS = "Invalid template for Squash DSL instruction line";

    private static final String UNDEFINED_KEYWORD = "UNDEFINED";

    private static final String MISSING_VALUE_KEYWORD = "MISSING_LAST_VALUE";

    private static final String MISSING_VALUE_USING = "MISSING_USING_VALUE";

    boolean hasTemplateError(List<String> instructionKeyList) {
        //check if the current Cmd line matches 1 of 6 Squash command templates
        return instructionKeyList.contains(UNDEFINED_KEYWORD) ||
                instructionKeyList.contains(MISSING_VALUE_KEYWORD) ||
                instructionKeyList.contains(MISSING_VALUE_USING) ||
                unmatchedOneOfSixDSLInstructionTemplates(instructionKeyList);
    }

    private boolean hasBuiltInValueError(SquashDSLComponentRegistry macroJsonFileContent, ASTNode valueNode) {
        ASTNode valueChildNode = valueNode.getFirstChildNode();
        String nodeType = valueChildNode.getElementType().toString();
        String nodeText = valueChildNode.getText().trim();
        switch (nodeType) {
            case "SquashTestTokenType.ASSERTION_VALIDATOR":
            case "SquashMacroTokenType.ASSERTION_VALIDATOR":
                List<SquashDSLAssertion> builtInAssertionList = macroJsonFileContent.getAssertions();
                return !checkAssertion(builtInAssertionList, nodeText);
            case "SquashTestTokenType.CONVERTER":
            case "SquashMacroTokenType.CONVERTER":
                List<SquashDSLConverter> builtInConverterList = macroJsonFileContent.getConverters();
                return !checkConverter(builtInConverterList, nodeText);
            case "SquashTestTokenType.EXECUTE_CMD":
            case "SquashMacroTokenType.EXECUTE_CMD":
                List<SquashDSLCommand> builtInCommandList = macroJsonFileContent.getCommands();
                return !checkExecuteCommand(builtInCommandList, nodeText);
            default:
                break;
        }
        return false;
    }

    private boolean checkExecuteCommand(List<SquashDSLCommand> builtInCommandList, String nodeText) {
        for (SquashDSLCommand refCommand : builtInCommandList) {
            if (refCommand.getNature().equals(nodeText)) {
                return true;
            }
        }
        return false;
    }

    private boolean checkConverter(List<SquashDSLConverter> builtInConverterList, String nodeText) {
        for (SquashDSLConverter refConverter : builtInConverterList) {
            String refOutput = refConverter.getOutputResource();
            if (refOutput.equals(nodeText)) {
                return true;
            } else if (nodeText.contains("(") && nodeText.contains(")")){
                String refFullConverter = refOutput+"("+refConverter.getNature()+")";
                if (refFullConverter.equals(nodeText)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean unmatchedOneOfSixDSLInstructionTemplates(List<String> instructionKeyList){
        if (containDuplicateKeywords(instructionKeyList) || instructionKeyList.isEmpty()){
            return true;
        }
        String headProperty = instructionKeyList.get(0);
        for (DSLInstructionParser p : PARSERS) {
            if (p.getInstructionParserName().equals(headProperty)) {
                return !p.matches(instructionKeyList);
            }
        }
        return true;
    }

    private boolean containDuplicateKeywords(List<String> instructionKeyList) {
        Set<String> instructionKeySet = new HashSet<>(instructionKeyList);
        if (instructionKeySet.size() != instructionKeyList.size()){
            return true;
        } else {
            return treatSamePositionKeywords(instructionKeySet);
        }
    }

    private boolean treatSamePositionKeywords(Set<String> instructionKeySet) {
        int isHasDoesCount = 0;
        int withThanTheCount = 0;
        for (String key : instructionKeySet) {
            if ("IS".equalsIgnoreCase(key) || "HAS".equalsIgnoreCase(key) || "DOES".equalsIgnoreCase(key)){
                ++isHasDoesCount;
                if (isHasDoesCount > 1) {
                    return true;
                }
            }
            if ("WITH".equalsIgnoreCase(key) || "THAN".equalsIgnoreCase(key) || "THE".equalsIgnoreCase(key)){
                ++withThanTheCount;
                if (withThanTheCount > 1) {
                    return true;
                }
            }
        }
        return false;
    }

    List<String> getKeyListFromInstructionTextInTestFile(SquashTestCommandLine commandLine) {
        List<String> result = new ArrayList<>();
        SquashTestCmdHeadProperty headProperty = commandLine.getCmdHeadProperty();
        SquashTestCmdHeadPropertyKey headPropertyKey = headProperty.getCmdHeadPropertyKey();
        String headKey = headPropertyKey.getKey();
        if (headKey != null){
            result.add(headKey.trim().toUpperCase());
            List<SquashTestCmdProperty> taFileCmdProperties = commandLine.getCmdPropertyList();

            //verify if the last keyword is followed by a value
            if (!taFileCmdProperties.isEmpty()){
                SquashTestCmdProperty lastProperty = taFileCmdProperties.get(taFileCmdProperties.size()-1);
                String lastPropertyText = lastProperty.getText().trim();
                updateKeywordList(result, !lastPropertyText.contains(" "), MISSING_VALUE_KEYWORD);

                for (SquashTestCmdProperty property : taFileCmdProperties) {
                    String propertyKey = property.getCmdPropertyKey().getKey();
                    checkInstructionPropertyKeyword(result, propertyKey);

                }
            }

            List<SquashTestUsingClause> squashTestUsingClauses = commandLine.getUsingClauseList();
            for (SquashTestUsingClause usingClause : squashTestUsingClauses) {
                String usingKey = usingClause.getUsingKey().getUsing();
                if (usingKey != null) {
                    addUsingKeyword(result, usingKey);
                    SquashTestValueList squashTestValueList = usingClause.getValueList();
                    updateKeywordList(result, squashTestValueList == null, MISSING_VALUE_USING);
                }
            }
        }

        return result;
    }

    private void checkInstructionPropertyKeyword(List<String> result, String propertyKey) {
        //verify if there are undefined keyword(s) in the current instruction line
        if (propertyKey == null) {
            result.add(UNDEFINED_KEYWORD);
        } else {
            addInstructionKeywordIntoList(result, propertyKey);
        }
    }

    private void updateKeywordList(List<String> result, boolean condition, String keyword) {
        if (condition) {
            result.add(keyword);
        }
    }

    private void addUsingKeyword(List<String> result, String usingKey) {
        updateKeywordList(result, usingKey != null, "USING");
    }

    /**
     * This method is to check if the current assertion value (not empty) is defined in Squash framework
     * @param builtInAssertionList the Squash framework built-in assertion value list
     * @param assertion the current assertion string value
     * @return true if the assertion nature is found, false otherwise.
     */
    private boolean checkAssertion(List<SquashDSLAssertion> builtInAssertionList, String assertion) {
        for (SquashDSLAssertion refAssertion : builtInAssertionList) {
            if (refAssertion.getNature().equals(assertion)) {
                return true;
            }
        }
        return false;
    }

    List<String> getKeyListFromInstructionTextInMacroFile(SquashMacroCommandLine commandLine) {
        List<String> result = new ArrayList<>();
        SquashMacroCmdHeadProperty headProperty = commandLine.getCmdHeadProperty();
        SquashMacroCmdHeadPropertyKey headPropertyKey = headProperty.getCmdHeadPropertyKey();
        String headKey = headPropertyKey.getCMDHeadKey();
        if (headKey != null) {
            result.add(headKey.trim().toUpperCase());

            List<SquashMacroCmdProperty> macroFileCmdProperties = commandLine.getCmdPropertyList();
            //verify if the last keyword is followed by a value
            if (!macroFileCmdProperties.isEmpty()){
                SquashMacroCmdProperty lastProperty = macroFileCmdProperties.get(macroFileCmdProperties.size()-1);
                String lastPropertyText = lastProperty.getText().trim();
                updateKeywordList(result, !lastPropertyText.contains(" "), MISSING_VALUE_KEYWORD);

                for (SquashMacroCmdProperty property : macroFileCmdProperties) {
                    String propertyKey = property.getCmdPropertyKey().getCMDKey();

                    //verify if there are undefined keyword(s) in the current instruction line
                    checkInstructionPropertyKeyword(result, propertyKey);
                }
            }

            List<SquashMacroUsingClause> squashTestUsingClauses = commandLine.getUsingClauseList();
            for (SquashMacroUsingClause usingClause : squashTestUsingClauses) {
                String usingKey = usingClause.getUsingKey().getUsing();
                if (usingKey != null) {
                    addUsingKeyword(result, usingKey);
                    SquashMacroValueList squashmacroValueList = usingClause.getValueList();
                    updateKeywordList(result, squashmacroValueList == null, MISSING_VALUE_USING);
                }
            }
        }
        return result;
    }

    private void addInstructionKeywordIntoList(List<String> result, String propertyKey) {
        if (!propertyKey.isEmpty()){
            result.add(propertyKey.trim().toUpperCase());
        }
    }

    void validateValueOfOneKey(AnnotationHolder holder, ASTNode valueNode, String key, String undefinedBuiltInValue, SquashDSLComponentRegistry macroJsonFileContent) {
        String newKey = key.trim().toUpperCase();
        switch (newKey) {
            case "IS":
            case "HAS":
            case "DOES":
            case "EXECUTE":
            case "TO":
                invokeUndefinedBuiltInValue(holder, valueNode, undefinedBuiltInValue, macroJsonFileContent);
                break;
            default:
                invokeSyntaxErrorValue(holder, valueNode, undefinedBuiltInValue);
                break;
        }
    }

    private void invokeSyntaxErrorValue(AnnotationHolder holder, ASTNode valueNode, String undefinedBuiltInValue) {
        if (undefinedBuiltInValue != null) {
            holder.createErrorAnnotation(valueNode, "Value must contain ONLY '.', '-', '_', '/', \\ or digital characters.");
        }
    }

    private void invokeUndefinedBuiltInValue(AnnotationHolder holder, ASTNode valueNode, String undefinedBuiltInValue, SquashDSLComponentRegistry macroJsonFileContent) {
        if (undefinedBuiltInValue != null || hasBuiltInValueError(macroJsonFileContent, valueNode)) {
            holder.createErrorAnnotation(valueNode, "Not an existing engine component identifier of the Squash framework");
        }
    }

    void displayInstructionTemplateListInQuickFix(@NotNull AnnotationHolder holder, ASTNode node) {
        Annotation anno = holder.createErrorAnnotation(node, INSTRUCTIONS);
        anno.setHighlightType(ProblemHighlightType.ERROR);

        String defineCmd = "DEFINE $(raw data) AS {nameInTheContext}";
        String loadCmd = "LOAD {path_To_Resource} [ FROM {resourceLibrary} ] [ AS {nameInTheContext} ]";
        String convertCmd = "CONVERT {resourceToConvert} TO <Category>( <Conv> ) [ USING {config} ] AS {convertedResource}";
        String executeCmd = "EXECUTE <Cmd> WITH {resource} ON {target} [ USING {config} ] AS {result}";
        String assertCmd = "ASSERT {resourceToTest} ( IS | HAS | DOES ) <Asr> [ ( WITH | THAN | THE ) {expectedResult} ] [ USING {config} ]";
        String verifyCmd = "VERIFY {resourceToTest} ( IS | HAS | DOES ) <Asr> [ ( WITH | THAN | THE ) {expectedResult} ] [ USING {config} ]";

        String[] cmdArray = new String[]{defineCmd, loadCmd, convertCmd, executeCmd, assertCmd, verifyCmd};

        for (String str : cmdArray) {
            MyIntentionAction quickFix = new MyIntentionAction(str);
            anno.registerFix(quickFix);
        }

    }

    void controlEmptyValue(AnnotationHolder holder, ASTNode valueListNode) {
        String valueListText = valueListNode.getText().trim();
        String[] valueListTextArray = valueListText.split(",");

        for (String valueEle : valueListTextArray) {
            if ("".equals(valueEle.trim())){
                holder.createWarningAnnotation(valueListNode, "Value in list must NOT be empty.");
            }
        }
    }

    void controlUsingValues(ASTNode eleNode, AnnotationHolder holder) {
        String eleNodeType = eleNode.getElementType().toString();
        if ("SquashTestTokenType.AC_POSITION".equals(eleNodeType)) {
            holder.createErrorAnnotation(eleNode, "Value in list must contain ONLY '.', '-', '_', '/', \\ or digital characters.");
        }
    }
}
