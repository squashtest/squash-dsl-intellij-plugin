/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.fwconnector;

import com.intellij.notification.NotificationType;
import com.intellij.openapi.project.Project;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLComponentRegistry;
import org.squashtest.ta.galaxia.metaexecution.enginelink.EngineLinkException;
import org.squashtest.ta.galaxia.metaexecution.enginelink.probe.BackgroundFrameworkConnectorFactory;
import org.squashtest.ta.galaxia.metaexecution.enginelink.probe.BackgroundFrameworkConnector;
import org.squashtest.ta.intellij.plugin.notification.NotificationProjectService;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.concurrent.TimeoutException;

public class IdeaFrameworkConnectorImpl implements BackgroundFrameworkConnector.ErrorReporter, BackgroundFrameworkConnector.SuccessListener, IdeaFrameworkConnector {
    public static final String JAR_PROTOCOL = "jar";
    public static final Logger LOGGER = LoggerFactory.getLogger(IdeaFrameworkConnectorImpl.class);

    private final BackgroundFrameworkConnectorFactory CONNECTOR_FACTORY;
    private final NotificationProjectService notifyer;
    private BackgroundFrameworkConnector connector;

    public IdeaFrameworkConnectorImpl(Project project, NotificationProjectService notifyer) throws EngineLinkException{
        this(project,new BackgroundFrameworkConnectorFactory(),notifyer);
    }

    public IdeaFrameworkConnectorImpl(Project project, BackgroundFrameworkConnectorFactory factory, NotificationProjectService notifyer) throws EngineLinkException{
        this.notifyer=notifyer;
        CONNECTOR_FACTORY=factory;
        File projectBase=new File(project.getBasePath());
        File pomFile=new File(projectBase,"pom.xml");

        if(pomFile.exists()) {
            String classRscName = "/"+ IdeaFrameworkConnectorImpl.class.getName().replace('.', '/') + ".class";
            URL thisClassURL=getClass().getResource(classRscName);
            switch(thisClassURL.getProtocol()){
                case JAR_PROTOCOL:
                    doJarProtocol(pomFile, thisClassURL);
                    break;
                case "file":
                default:
                    throw new UnsupportedOperationException("Cannot find plugin resource location through unsupported url protocol "+thisClassURL.getProtocol());
            }
        }else{
            throw new EngineLinkException("This is not a valid KeywordFramework project: no pom.");
        }
    }

    private void doJarProtocol(File pomFile, URL thisClassURL) throws EngineLinkException{
        String[] parts = thisClassURL.getFile().split("!");
        final String jarURLString = parts[0];
        try {
            File pluginDir = new File(new URL(jarURLString).toURI()).getParentFile();
            File mavenBinDir = new File(pluginDir, "maven/bin");
            LOGGER.debug("Looking up maven bin dir in {}", mavenBinDir.getAbsolutePath());
            if (mavenBinDir.exists()) {
                connector = CONNECTOR_FACTORY.getFrameworkConnector(this, this, pomFile, mavenBinDir.getAbsolutePath());
            } else {
                LOGGER.warn("Anomaly : maven was not found at expected location {} ! No fallback if maven is not on the PATH!", mavenBinDir.getAbsolutePath());
                connector = CONNECTOR_FACTORY.getFrameworkConnector(this, this, pomFile);
            }
        }catch(URISyntaxException | MalformedURLException e){
            LOGGER.warn("Failed to parse unexpected jarURL {}. No fallback if maven is not on the path.",jarURLString,e);
            connector = CONNECTOR_FACTORY.getFrameworkConnector(this, this, pomFile);
        }
    }

    @Override
    public SquashDSLComponentRegistry getSquashDSLComponentRegistry() throws EngineLinkException, TimeoutException {
        return connector.getSquashDSLComponentRegistry();
    }

    @Override
    public void report(Throwable t) {
        notifyer.notifyProject(NotificationType.ERROR,"Failed to extract component registry data","",t.getMessage());
    }

    @Override
    public void notifySuccess(SquashDSLComponentRegistry registry){
                if (registry != null){
                        notifyer.notifyProject(NotificationType.INFORMATION,"Component registry data extracted","","The Squash TF plugin is now ready. You may close and reopen already annotated files to get accurate validation and autocompletion support.");
                    }
            }
}
