/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.completion;

import org.squashtest.ta.intellij.plugin.tools.LineSearchingResult;

/**
 * TODO : This helper is not a Control as in validation, but as in 'I control how my dude {@link SquashTestFileCompletionContributor}' behaves for phase marker completion.
 * Therefore, it should be renamed "SquashTestFilePhaseCompletionStrategy", and reimplemented as a genuine Strategy class, and not a mere data producer with yet
 * more data treatment to do at the calling side. This would  make {@link SquashTestFileCompletionContributor} simpler, and also clearer.
 *
 */
class SquashTestFilePhaseControl {
    private static final String METADATA = "_METADATA";
    private static final String SETUP = "_SETUP";
    private static final String TEST = "_TEST";
    private static final String TEARDOWN = "_TEARDOWN";

    private LineSearchingResult metadataSearchResult;
    private LineSearchingResult setupSearchResult;
    private LineSearchingResult testSearchResult;
    private LineSearchingResult teardownSearchResult;

    SquashTestFilePhaseControl(LineSearchingResult metadataSearchResult, LineSearchingResult setupSearchResult, LineSearchingResult testSearchResult, LineSearchingResult teardownSearchResult) {
        this.metadataSearchResult = metadataSearchResult;
        this.setupSearchResult = setupSearchResult;
        this.testSearchResult = testSearchResult;
        this.teardownSearchResult = teardownSearchResult;
    }

    String findPhases () {
        StringBuilder result = new StringBuilder("EMPTY");
        if(metadataSearchResult.wasFound()){
            result.append(METADATA);
        }

        if (setupSearchResult.wasFound()){
            result.append(SETUP);
        }
        if (testSearchResult.wasFound()) {
            result.append(TEST);
        }
        if (teardownSearchResult.wasFound()) {
            result.append(TEARDOWN);
        }
        return result.toString();
    }
}
