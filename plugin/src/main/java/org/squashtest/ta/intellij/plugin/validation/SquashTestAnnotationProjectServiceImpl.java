/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.validation;

import com.intellij.codeInspection.ProblemHighlightType;
import com.intellij.lang.ASTNode;
import com.intellij.lang.annotation.AnnotationHolder;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.util.PsiTreeUtil;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.ta.galaxia.enginelink.components.SquashDSLComponentRegistry;
import org.squashtest.ta.galaxia.metaexecution.enginelink.EngineLinkException;
import org.squashtest.ta.intellij.plugin.file.psi.SquashTestCmdHeadProperty;
import org.squashtest.ta.intellij.plugin.file.psi.SquashTestCmdHeadPropertyValue;
import org.squashtest.ta.intellij.plugin.file.psi.SquashTestCmdProperty;
import org.squashtest.ta.intellij.plugin.file.psi.SquashTestCmdPropertyKey;
import org.squashtest.ta.intellij.plugin.file.psi.SquashTestCmdPropertyValue;
import org.squashtest.ta.intellij.plugin.file.psi.SquashTestCommandLine;
import org.squashtest.ta.intellij.plugin.file.psi.SquashTestMetadataSection;
import org.squashtest.ta.intellij.plugin.file.psi.SquashTestTestPhase;
import org.squashtest.ta.intellij.plugin.file.psi.SquashTestUsingClause;
import org.squashtest.ta.intellij.plugin.file.psi.SquashTestValueElement;
import org.squashtest.ta.intellij.plugin.file.psi.SquashTestValueList;
import org.squashtest.ta.intellij.plugin.fwconnector.IdeaFrameworkConnector;
import org.squashtest.ta.intellij.plugin.highlight.MacroHighlightsHelper;
import org.squashtest.ta.intellij.plugin.notification.NotificationProjectService;
import org.squashtest.ta.intellij.plugin.projectmodel.SquashTestFileLocationService;

import java.util.List;
import java.util.concurrent.TimeoutException;
import java.util.regex.Pattern;

public class SquashTestAnnotationProjectServiceImpl extends BaseAnnotationProjectServiceImpl implements SquashTestAnnotationProjectService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SquashTestAnnotator.class);
    private static final String TEST_FILE ="The Squash Test File {} ";

    private static final String METADATA_KEY_REGEX = "[\\w-.]+";
    private static final String METADATA_VALUE_REGEX = "[\\w-./]+";

    private static final Pattern METADATA_KEY_REGEX_PATTERN = Pattern.compile(METADATA_KEY_REGEX);
    private static final Pattern METADATA_VALUE_REGEX_PATTERN = Pattern.compile(METADATA_VALUE_REGEX);

    private final SquashTestFileLocationService fileLocationService;

    private final MacroCallValidationHelper macroCallValidationHelper;
    private final MacroHighlightsHelper macroHighlightsHelper;

    private final DSLInstructionLineValidationHelper dslInstructionLineValidationHelper = new DSLInstructionLineValidationHelper();

    public SquashTestAnnotationProjectServiceImpl(NotificationProjectService notifier, SquashTestFileLocationService fileLocationService, MacroCallValidationHelper macroCallValidationHelper, MacroHighlightsHelper macroHighlightsHelper, IdeaFrameworkConnector connector) {
        super(notifier,connector);

        this.macroCallValidationHelper = macroCallValidationHelper;
        this.macroHighlightsHelper = macroHighlightsHelper;
        this.fileLocationService = fileLocationService;
    }

    @Override
    protected void performAnnotation(@NotNull PsiElement element, @NotNull AnnotationHolder holder, SquashDSLComponentRegistry macroJsonFileContent) {
        PsiFile taTestFile = element.getContainingFile();
        String elementType = element.getNode().getElementType().toString();

        switch (elementType){
            case "SquashTestTokenType.METADATA_KEY":
                metadataKeySyntaxControl(element, holder, taTestFile);
                break;
            case "SquashTestTokenType.METADATA_VALUE":
                metadataValueSyntaxControl(element, holder, taTestFile);
                break;
            case "SquashTestTokenType.METADATA_SEPARATOR":
                metadataSeparatorSyntaxControl(element, holder, taTestFile);
                break;
            case "SquashTestTokenType.MACRO_LINE_CONTENT":
                //macro line control
                macroLineControl(holder, element, macroJsonFileContent);
                break;
            case "COMMAND_LINE":
                //command line control
                commandLineControl(holder, element, macroJsonFileContent);
                break;
            case "FILE":
                //check if this current file is located in 'tests' folder
                fileControl(element, holder, taTestFile);
                break;
            default:
                break;
        }
    }

    private void metadataSeparatorSyntaxControl(PsiElement element, AnnotationHolder holder, PsiFile taTestFile) {
        String filePath=taTestFile.getVirtualFile().getPath();
        PsiElement nextValue = element.getNextSibling();
        if (nextValue == null ||
                !"SquashTestTokenType.METADATA_VALUE".equals(nextValue.getNode().getElementType().toString())){
            LOGGER.warn("["+ filePath + "] Initiated Metadata VALUE must NOT be empty.");
            holder.createErrorAnnotation(element.getParent(), " Initiated Metadata VALUE must NOT be empty.");
        }
    }

    private void metadataValueSyntaxControl(PsiElement element, AnnotationHolder holder, PsiFile taTestFile) {
        String filePath=taTestFile.getVirtualFile().getPath();
        String valueContent = element.getText().trim();
        if (notMatchWithPattern(METADATA_VALUE_REGEX_PATTERN, valueContent)){
            LOGGER.warn("["+ filePath + "] Metadata VALUE syntax convention(s) violated: " + valueContent);
            holder.createErrorAnnotation(element, "Metadata VALUE syntax convention(s) violated!");
        }
    }

    private void metadataKeySyntaxControl(@NotNull PsiElement element, @NotNull AnnotationHolder holder, PsiFile taTestFile) {
        String filePath = taTestFile.getVirtualFile().getPath();
        String metadataKey = element.getText();
        if (metadataKey.startsWith(" ")){
            LOGGER.warn("["+ filePath + "] Metadata KEY must NOT start with space/tabulations: " + metadataKey);
            holder.createErrorAnnotation(element, "Metadata KEY must NOT start with space/tabulations.");
        }
        String keyContent = metadataKey.trim();
        if (notMatchWithPattern(METADATA_KEY_REGEX_PATTERN, keyContent)){
            LOGGER.warn("["+ filePath + "] Metadata KEY syntax convention(s) violated: " + keyContent);
            holder.createErrorAnnotation(element, "Metadata KEY syntax convention(s) violated!");
        }
    }

    private boolean notMatchWithPattern(Pattern pattern, String keyContent) {
        return !pattern.matcher(keyContent).matches();
    }

    private void fileControl(@NotNull PsiElement element, @NotNull AnnotationHolder holder, PsiFile taTestFile) {
        String filePath=taTestFile.getVirtualFile().getPath();
        if (!fileLocationService.isInTestsFolder(filePath)) {
            LOGGER.warn(TEST_FILE + filePath + ") is NOT located in the 'project/src/squashTA/tests' folder!");
            holder.createErrorAnnotation(taTestFile,
                    "This Squash Test Script should be placed in the 'project/src/squashTA/tests' folder.");
        } else if (!filePath.endsWith("setup.ta") && !filePath.endsWith("teardown.ta")
                && !filePath.endsWith("setup.txt") && !filePath.endsWith("teardown.txt")) {
            //check if the current Test file contains ONE TEST phase
            isTestPhaseExisted(element, holder, filePath);
        } else {
            //verify if there is a METADATA section in an Ecosystem file
            isMetadataSectionExisted(element, holder, filePath);
        }

    }

    private void isMetadataSectionExisted(PsiElement element, AnnotationHolder holder, String filePath) {
        SquashTestMetadataSection[] metadataSections = PsiTreeUtil.getChildrenOfType(element, SquashTestMetadataSection.class);
        if (metadataSections != null && metadataSections.length > 0) {
            LOGGER.error(TEST_FILE + filePath + ") is an Ecosystem file. It is hence forbidden to have a METADATA section.");
            holder.createErrorAnnotation(element.getContainingFile(),
                    "This Squash Test Ecosystem Script MUST NOT have any METADATA section.");
        }
    }


    private void isTestPhaseExisted(@NotNull PsiElement element, @NotNull AnnotationHolder holder, String filePath) {
        SquashTestTestPhase[] testPhases = PsiTreeUtil.getChildrenOfType(element, SquashTestTestPhase.class);
        if (testPhases == null || testPhases.length!=1) {
            LOGGER.warn(TEST_FILE + filePath + ") MUST have ONE and ONLY ONE TEST phase.");
            holder.createErrorAnnotation(element.getContainingFile(),
                 "This Squash Test Script MUST have ONE (and ONLY ONE) 'TEST' phase.");
        }
    }

    private void commandLineControl(@NotNull AnnotationHolder holder, PsiElement cmdLine, SquashDSLComponentRegistry macroJsonFileContent) {
        SquashTestCommandLine commandLine = (SquashTestCommandLine) cmdLine;
        if (commandLine != null) {
            //check instruction template syntax
            validateInstructionTemplate(holder, commandLine);

            //check values
            validateValueOfAllKeys(holder, commandLine, macroJsonFileContent);
            
            //check USING value list
            validateUsingValueList(holder, commandLine);
        }
    }

    private void validateUsingValueList(AnnotationHolder holder, SquashTestCommandLine commandLine) {
        List<SquashTestUsingClause> squashTestUsingClauses = commandLine.getUsingClauseList();
        for (SquashTestUsingClause usingClause : squashTestUsingClauses) {
            String usingKey = usingClause.getUsingKey().getUsing();
            if (usingKey != null) {
                SquashTestValueList squashTestValueList = usingClause.getValueList();
                if (squashTestValueList != null) {
                    ASTNode valueListNode = squashTestValueList.getNode();
                    dslInstructionLineValidationHelper.controlEmptyValue(holder, valueListNode);

                    List<SquashTestValueElement> squashTestUsingValueElements = squashTestValueList.getValueElementList();
                    controlUsingValues(holder, squashTestUsingValueElements);
                }
            }
        }
    }

    private void controlUsingValues(AnnotationHolder holder, List<SquashTestValueElement> squashTestUsingValueElements) {
        for (SquashTestValueElement squashTestUsingValueElement : squashTestUsingValueElements) {
            ASTNode eleNode = squashTestUsingValueElement.getFirstChild().getNode();
            dslInstructionLineValidationHelper.controlUsingValues(eleNode, holder);
        }
    }

    private void validateValueOfAllKeys(AnnotationHolder holder, SquashTestCommandLine commandLine, SquashDSLComponentRegistry macroJsonFileContent) {
        //check in head clause
        SquashTestCmdHeadProperty cmdHeadProperty = commandLine.getCmdHeadProperty();
        String headKey = cmdHeadProperty.getCmdHeadPropertyKey().getKey();
        if (headKey != null) {
            SquashTestCmdHeadPropertyValue headValue = cmdHeadProperty.getCmdHeadPropertyValue();
            if (headValue != null) {
                ASTNode headValueNode = headValue.getNode();
                //check in this instruction if there are values of type empty "{}" or "{{}}"
                checkEmptyCurlyBrackets(holder, headValueNode, headValueNode.getText());
                String undefinedBuiltInValue = headValue.getUndefinedElement();
                dslInstructionLineValidationHelper.validateValueOfOneKey(holder, headValueNode, headKey, undefinedBuiltInValue, macroJsonFileContent);
            }
        }

        //check in other clauses
        List<SquashTestCmdProperty> testCmdProperties = commandLine.getCmdPropertyList();
        for (SquashTestCmdProperty property : testCmdProperties) {
            SquashTestCmdPropertyKey cmdPropertyKey = property.getCmdPropertyKey();
            String key = cmdPropertyKey.getKey();
            if (key != null){
                SquashTestCmdPropertyValue cmdPropertyValue = property.getCmdPropertyValue();
                if (cmdPropertyValue != null) {
                    ASTNode cmdPropertyValueNode = cmdPropertyValue.getNode();
                    //check in this instruction if there are values of type empty "{}" or "{{}}"
                    checkEmptyCurlyBrackets(holder, cmdPropertyValueNode, cmdPropertyValue.getText());
                    String undefinedBuiltInValue = cmdPropertyValue.getUndefinedElement();
                    dslInstructionLineValidationHelper.validateValueOfOneKey(holder, cmdPropertyValueNode, key, undefinedBuiltInValue, macroJsonFileContent);
                }
            }
        }
    }

    private void checkEmptyCurlyBrackets(AnnotationHolder holder, ASTNode cmdPropertyValueNode, String text) {
        String newValue = text.trim();
        if ("{}".equals(newValue) || "{{}}".equals(newValue)) {
            holder.createWeakWarningAnnotation(cmdPropertyValueNode, "Value inside curly brackets must NOT be empty.");
        }
    }

    private void validateInstructionTemplate(AnnotationHolder holder, SquashTestCommandLine commandLine) {
        List<String> instructionKeyList = dslInstructionLineValidationHelper.getKeyListFromInstructionTextInTestFile(commandLine);
        if (dslInstructionLineValidationHelper.hasTemplateError(instructionKeyList)){
            ASTNode instructionLineNode = commandLine.getNode();
            dslInstructionLineValidationHelper.displayInstructionTemplateListInQuickFix(holder, instructionLineNode);
        }
    }

    private void macroLineControl(@NotNull AnnotationHolder holder, PsiElement macroLineContent, SquashDSLComponentRegistry macroJsonFileContent) {
        String macroLineContentText = macroLineContent.getText();
        SquashMacroCallControlResult macroControlResult = macroCallValidationHelper.checkMacroLineContent(macroLineContentText, macroJsonFileContent);
        String message = (macroControlResult.isEmptyMacroLine())? "" : macroCallValidationHelper.getMacroLineMsg(macroControlResult);

        //macro color annotation
        switch (message) {
            case "Macro line definition not found":
                holder.createErrorAnnotation(macroLineContent, "No match with existing Macro found").setHighlightType(ProblemHighlightType.ERROR);
                break;
            case "Macro line multi-definition conflict":
                holder.createWarningAnnotation(macroLineContent, "Conflict : Multiple Macros match").setHighlightType(ProblemHighlightType.WEAK_WARNING);
                break;
            case "":
                break;
            default:
                macroHighlightsHelper.makeColorToNewValue(macroLineContent, message, holder, "ta");
                break;
        }
    }
}
