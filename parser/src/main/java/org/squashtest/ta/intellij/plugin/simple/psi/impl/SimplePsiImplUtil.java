/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.simple.psi.impl;

import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import org.squashtest.ta.intellij.plugin.simple.psi.SimpleProperty;
import org.squashtest.ta.intellij.plugin.simple.psi.SimpleTypes;

public class SimplePsiImplUtil {
    private SimplePsiImplUtil() {
        throw new IllegalStateException("Utility class");
    }

    public static String getKey(SimpleProperty element) {
        ASTNode macroNode = element.getNode().findChildByType(SimpleTypes.KEY);
        return getValueFromNote(macroNode);
    }

    public static String getValue(SimpleProperty  element) {
        ASTNode macroNode = element.getNode().findChildByType(SimpleTypes.VALUE);
        return getValueFromNote(macroNode);
    }

    //shared methods
    private static String getValueFromNote(ASTNode node) {
        if (node != null) {
            // IMPORTANT: Convert embedded escaped spaces to simple spaces
            return node.getText().replaceAll("\\\\ ", " ");
        } else {
            return null;
        }
    }

    public static String getName(SimpleProperty element) {
        return getKey(element);
    }

	public static PsiElement getNameIdentifier(SimpleProperty element) {
        ASTNode keyNode = element.getNode().findChildByType(SimpleTypes.KEY);
        if (keyNode != null) {
            return keyNode.getPsi();
        } else {
            return null;
        }
    }
}
