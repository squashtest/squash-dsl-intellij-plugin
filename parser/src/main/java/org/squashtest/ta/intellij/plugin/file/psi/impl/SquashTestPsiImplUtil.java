/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.file.psi.impl;


import com.intellij.lang.ASTNode;
import org.squashtest.ta.intellij.plugin.file.psi.SquashTestCmdHeadPropertyKey;
import org.squashtest.ta.intellij.plugin.file.psi.SquashTestCmdHeadPropertyValue;
import org.squashtest.ta.intellij.plugin.file.psi.SquashTestCmdPropertyKey;
import org.squashtest.ta.intellij.plugin.file.psi.SquashTestCmdPropertyValue;
import org.squashtest.ta.intellij.plugin.file.psi.SquashTestMacroLine;
import org.squashtest.ta.intellij.plugin.file.psi.SquashTestTypes;
import org.squashtest.ta.intellij.plugin.file.psi.SquashTestUsingKey;
import org.squashtest.ta.intellij.plugin.file.psi.SquashTestValueElement;

public class SquashTestPsiImplUtil {
    private SquashTestPsiImplUtil() {
        throw new IllegalStateException("Utility class");
    }

    public static String getKey(SquashTestCmdHeadPropertyKey element) {
        ASTNode macroNode = element.getNode().findChildByType(SquashTestTypes.CMD_HEAD_KEY);
        return getValueFromNote(macroNode);
    }

    public static String getValue(SquashTestCmdHeadPropertyValue element) {
        ASTNode macroNode = element.getNode().findChildByType(SquashTestTypes.VALUE);
        return getValueFromNote(macroNode);
    }

    public static String getExecuteCommand(SquashTestCmdHeadPropertyValue element) {
        ASTNode macroNode = element.getNode().findChildByType(SquashTestTypes.EXECUTE_CMD);
        return getValueFromNote(macroNode);
    }

    public static String getUndefinedElement(SquashTestCmdHeadPropertyValue element) {
        ASTNode macroNode = element.getNode().findChildByType(SquashTestTypes.AC_POSITION);
        return getValueFromNote(macroNode);
    }

    public static String getKey(SquashTestCmdPropertyKey element) {
        ASTNode macroNode = element.getNode().findChildByType(SquashTestTypes.CMD_KEY);
        return getValueFromNote(macroNode);
    }

    public static String getUsing(SquashTestUsingKey element) {
        ASTNode macroNode = element.getNode().findChildByType(SquashTestTypes.USING);
        return getValueFromNote(macroNode);
    }

    public static String getValue(SquashTestCmdPropertyValue element) {
        ASTNode macroNode = element.getNode().findChildByType(SquashTestTypes.VALUE);
        return getValueFromNote(macroNode);
    }

    public static String getAssertionValidator(SquashTestCmdPropertyValue element) {
        ASTNode macroNode = element.getNode().findChildByType(SquashTestTypes.ASSERTION_VALIDATOR);
        return getValueFromNote(macroNode);
    }

    public static String getConverter(SquashTestCmdPropertyValue element) {
        ASTNode macroNode = element.getNode().findChildByType(SquashTestTypes.CONVERTER);
        return getValueFromNote(macroNode);
    }

    public static String getUndefinedElement(SquashTestCmdPropertyValue element) {
        ASTNode macroNode = element.getNode().findChildByType(SquashTestTypes.AC_POSITION);
        return getValueFromNote(macroNode);
    }

    public static String getMacroLineContent(SquashTestMacroLine element) {
        ASTNode macroNode = element.getNode().findChildByType(SquashTestTypes.MACRO_LINE_CONTENT);
        return getValueFromNote(macroNode);
    }

    public static String getValue(SquashTestValueElement element) {
        ASTNode macroNode = element.getNode().findChildByType(SquashTestTypes.VALUE);
        return getValueFromNote(macroNode);
    }

    //shared methods
    private static String getValueFromNote(ASTNode node) {
        if (node != null) {
            // IMPORTANT: Convert embedded escaped spaces to simple spaces
            return node.getText().replaceAll("\\\\ ", " ");
        } else {
            return null;
        }
    }
}
