/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.simple.psi.impl;

import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import org.jetbrains.annotations.NotNull;
import org.squashtest.ta.intellij.plugin.simple.psi.SimpleProperty;
import org.squashtest.ta.intellij.plugin.simple.psi.SimpleTypes;
import org.squashtest.ta.intellij.plugin.simple.psi.SimpleElementFactory;
import org.squashtest.ta.intellij.plugin.simple.psi.SimpleNamedElement;


public abstract class SimpleNamedElementImpl extends ASTWrapperPsiElement implements SimpleNamedElement {
    public SimpleNamedElementImpl(@NotNull ASTNode node) {
        super(node);
    }
	
	public PsiElement setName(String newName) {
        ASTNode keyNode = getNode().findChildByType(SimpleTypes.KEY);
        if (keyNode != null) {
            SimpleProperty property = SimpleElementFactory.createProperty(getProject(), newName);
            ASTNode newKeyNode = property.getFirstChild().getNode();
            getNode().replaceChild(keyNode, newKeyNode);
        }
        return this;
    }
	
	public PsiElement getNameIdentifier(){throw new UnsupportedOperationException("This should have been overriden !");}
}
