/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2018 - 2020 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.ta.intellij.plugin.macro.psi.impl;

import com.intellij.lang.ASTNode;
import org.squashtest.ta.intellij.plugin.macro.psi.SquashMacroCmdHeadPropertyKey;
import org.squashtest.ta.intellij.plugin.macro.psi.SquashMacroCmdHeadPropertyValue;
import org.squashtest.ta.intellij.plugin.macro.psi.SquashMacroCmdPropertyKey;
import org.squashtest.ta.intellij.plugin.macro.psi.SquashMacroCmdPropertyValue;
import org.squashtest.ta.intellij.plugin.macro.psi.SquashMacroMacroLine;
import org.squashtest.ta.intellij.plugin.macro.psi.SquashMacroMacroTitleContent;
import org.squashtest.ta.intellij.plugin.macro.psi.SquashMacroMacroTitleProperty;
import org.squashtest.ta.intellij.plugin.macro.psi.SquashMacroTypes;
import org.squashtest.ta.intellij.plugin.macro.psi.SquashMacroUsingKey;
import org.squashtest.ta.intellij.plugin.macro.psi.SquashMacroValueElement;


public class SquashMacroPsiImplUtil {
    private SquashMacroPsiImplUtil() {
        throw new IllegalStateException("Utility class");
    }

    public static String getTitleKey(SquashMacroMacroTitleProperty element) {
        ASTNode macroNode = element.getNode().findChildByType(SquashMacroTypes.TITLE_KEY);
        return getValueFromNote(macroNode);
    }

    public static String getTitleFirstParam(SquashMacroMacroTitleContent element) {
        ASTNode macroNode = element.getNode().findChildByType(SquashMacroTypes.TITLE_FIRST_PARAM);
        return getValueFromNote(macroNode);
    }

    public static String getTitleParam(SquashMacroMacroTitleProperty element) {
        ASTNode macroNode = element.getNode().findChildByType(SquashMacroTypes.TITLE_PARAM);
        return getValueFromNote(macroNode);
    }

    public static String getCMDHeadKey(SquashMacroCmdHeadPropertyKey element) {
        ASTNode macroNode = element.getNode().findChildByType(SquashMacroTypes.CMD_HEAD_KEY);
        return getValueFromNote(macroNode);
    }

    public static String getCMDHeadValue(SquashMacroCmdHeadPropertyValue element) {
        ASTNode macroNode = element.getNode().findChildByType(SquashMacroTypes.VALUE);
        return getValueFromNote(macroNode);
    }

    public static String getExecuteCommand(SquashMacroCmdHeadPropertyValue element) {
        ASTNode macroNode = element.getNode().findChildByType(SquashMacroTypes.EXECUTE_CMD);
        return getValueFromNote(macroNode);
    }

    public static String getUndefinedElement(SquashMacroCmdHeadPropertyValue element) {
        ASTNode macroNode = element.getNode().findChildByType(SquashMacroTypes.AC_POSITION);
        return getValueFromNote(macroNode);
    }

    public static String getCMDKey(SquashMacroCmdPropertyKey element) {
        ASTNode macroNode = element.getNode().findChildByType(SquashMacroTypes.CMD_KEY);
        return getValueFromNote(macroNode);
    }

    public static String getUsing(SquashMacroUsingKey element) {
        ASTNode macroNode = element.getNode().findChildByType(SquashMacroTypes.USING);
        return getValueFromNote(macroNode);
    }

    public static String getCMDValue(SquashMacroCmdPropertyValue element) {
        ASTNode macroNode = element.getNode().findChildByType(SquashMacroTypes.VALUE);
        return getValueFromNote(macroNode);
    }

    public static String getAssertionValidator(SquashMacroCmdPropertyValue element) {
        ASTNode macroNode = element.getNode().findChildByType(SquashMacroTypes.ASSERTION_VALIDATOR);
        return getValueFromNote(macroNode);
    }

    public static String getUndefinedElement(SquashMacroCmdPropertyValue element) {
        ASTNode macroNode = element.getNode().findChildByType(SquashMacroTypes.AC_POSITION);
        return getValueFromNote(macroNode);
    }

    public static String getConverter(SquashMacroCmdPropertyValue element) {
        ASTNode macroNode = element.getNode().findChildByType(SquashMacroTypes.CONVERTER);
        return getValueFromNote(macroNode);
    }

    public static String getMacroLineContent(SquashMacroMacroLine element) {
        ASTNode macroNode = element.getNode().findChildByType(SquashMacroTypes.MACRO_LINE_CONTENT);
        return getValueFromNote(macroNode);
    }

    public static String getCMDValue(SquashMacroValueElement element) {
        ASTNode macroNode = element.getNode().findChildByType(SquashMacroTypes.VALUE);
        return getValueFromNote(macroNode);
    }

    //shared methods
    private static String getValueFromNote(ASTNode node) {
        if (node != null) {
            // IMPORTANT: Convert embedded escaped spaces to simple spaces
            return node.getText().replaceAll("\\\\ ", " ");
        } else {
            return null;
        }
    }
}
