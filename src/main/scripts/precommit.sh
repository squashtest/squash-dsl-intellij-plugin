#!/bin/sh
#
#     This file is part of the Squashtest platform.
#     Copyright (C) 2011 - 2019 Henix
#
#     See the NOTICE file distributed with this work for additional
#     information regarding copyright ownership.
#
#     This is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Lesser General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     this software is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Lesser General Public License for more details.
#
#     You should have received a copy of the GNU Lesser General Public License
#     along with this software.  If not, see <http://www.gnu.org/licenses />.
#

echo pre-commit checks - license headers

echo ...license check
log=$(mktemp)
mvn -o -DskipTests license:check license:check@Modified-by license:check@OriginalCopyright>$log
status=$?

if [ $status -ne 0 ]; then
	echo "*** Failed : some license headers are missing or non-conformant ***"
	grep Missing $log| cut -d"]" -f 2
	if [ $(grep Missing $log | wc -l) -eq 0 ]; then
		echo "*** No missing header found, see log in target/precommit.log"
		mkdir -p target
		mv $log target/precommit.log
	else
		echo "To draft license editing, please run the following :"
		echo mvn license:format license:format@Modified-by license:format@OriginalCopyright
		rm $log
	fi
	exit 1
fi

echo "All checked files have conformant headers"
rm $log
